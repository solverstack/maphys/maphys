#
# Python wrapper for maphys
# 24/03/17
# Tested on maphys version 5.0.2
# Author: Gilles Marait
#

from ctypes import *
from ctypes.util import find_library
import os
import re
import numpy as np
import scipy.sparse as spp
import sys

if sys.version_info.major == 3:
    from pymaphys.maphys_struct import *
else:
    from maphys_struct import *

class Maphys():

    def __init__(self, precision='d'):
        """
        Initialize the maphys wrapper with a given scalar type and precision
        precision: one of 'c','d','s','z' (ignore case)
        """

        # Check precision / scalar type
        av_precisions = ['c', 'C', 'd', 'D', 's', 'S', 'z', 'Z']
        if precision not in av_precisions:
            raise ValueError("Maphys precision must be 'c','d','s' or 'z'")

        self.precision = precision.lower()
        self.is_complex = (self.precision == 'c' or self.precision == 'z')

        self.libmaphys = self.__load_maphys()

        struct = Maphys_struct(self.precision)

        # Create the maphys structure
        self.__struct_type = struct.get_struct_type()
        self.__scal_type = struct.get_scal_type()
        self.__real_type = struct.get_real_type()
        self.__MPI_comm_type = struct.get_MPI_comm_type()
        self.nptype = struct.get_numpy_type()

        self.mphs = self.__struct_type()
        self.__mphs_ptr = pointer(self.mphs)

        # Set intern values
        self.__is_init = False

        # Get some permanent pointers for the data
        self.__rows = None
        self.__cols = None
        self.__data = None
        self.__rhs = None
        self.__sol = None
        self.__myinterface = None
        self.__myindexvi = None
        self.__myptrindexvi = None
        self.__myindexintrf = None
        self.__myinterface_ptr = None
        self.__myindexvi_ptr = None
        self.__myptrindexvi_ptr = None
        self.__myindexintrf_ptr = None

        # CNTLS and INFOS with fortran numbering
        self.ICNTL = {}
        self.RCNTL = {}
        self.IINFO = {}
        self.RINFO = {}
        self.IINFOMIN = {}
        self.RINFOMIN = {}
        self.IINFOMAX = {}
        self.RINFOMAX = {}
        self.IINFOAVG = {}
        self.RINFOAVG = {}
        self.IINFOG = {}
        self.RINFOG = {}

        # Drive function depending on the precision
        if self.precision == 'c':
            self.__driver = self.libmaphys.cmph_maphys_driver_c
            self.__distributed_interface = self.libmaphys.CMPH_distributed_interface_c
            size_checker = self.libmaphys.cmph_get_maphys_struct_size
        elif self.precision == 'd':
            self.__driver = self.libmaphys.dmph_maphys_driver_c
            self.__distributed_interface = self.libmaphys.DMPH_distributed_interface_c
            size_checker = self.libmaphys.dmph_get_maphys_struct_size
        elif self.precision == 's':
            self.__driver = self.libmaphys.smph_maphys_driver_c
            self.__distributed_interface = self.libmaphys.SMPH_distributed_interface_c
            size_checker = self.libmaphys.smph_get_maphys_struct_size
        elif self.precision == 'z':
            self.__driver = self.libmaphys.zmph_maphys_driver_c
            self.__distributed_interface = self.libmaphys.ZMPH_distributed_interface_c
            size_checker = self.libmaphys.zmph_get_maphys_struct_size

        self.__driver.argtypes = [POINTER(self.__struct_type)]
        self.__distributed_interface.argtypes = [POINTER(self.__struct_type), c_int, c_int, POINTER(c_int), c_int, POINTER(c_int), POINTER(c_int), POINTER(c_int)]
        size_checker.restype = c_uint

        # Check size of the structure is consistent
        struct_size_c = int(size_checker())

        if struct_size_c != sizeof(self.__struct_type):
            raise EnvironmentError("Size of structure in not consistent between C and python")

    def __update_cntls(self):
        """
        Update CNTLS (assumes fortran numbering in the dictionaries)
        """
        for i, v in self.ICNTL.items():
            self.mphs.icntl[i-1] = c_int(v)
        for i, v in self.RCNTL.items():
            self.mphs.rcntl[i-1] = c_double(v)

    def __update_infos(self):
        """
        Update INFOS (assumes fortran numbering in the dictionaries)
        """
        for i in range(Maphys_struct.MAPHYS_IINFO_SIZE):
            self.IINFO[i+1] = self.mphs.iinfo[i]
            self.IINFOMIN[i+1] = self.mphs.iinfomin[i]
            self.IINFOMAX[i+1] = self.mphs.iinfomax[i]
            self.IINFOAVG[i+1] = self.mphs.iinfoavg[i]

        for i in range(Maphys_struct.MAPHYS_RINFO_SIZE):
            self.RINFO[i+1] = self.mphs.rinfo[i]
            self.RINFOMIN[i+1] = self.mphs.rinfomin[i]
            self.RINFOMAX[i+1] = self.mphs.rinfomax[i]
            self.RINFOAVG[i+1] = self.mphs.rinfoavg[i]

        for i in range(Maphys_struct.MAPHYS_IINFOG_SIZE):
            self.IINFOG[i+1] = self.mphs.iinfog[i]

        for i in range(Maphys_struct.MAPHYS_RINFOG_SIZE):
            self.RINFOG[i+1] = self.mphs.rinfog[i]

    def __load_maphys(self):
        """
        Load the maphys library
        """

        libmaphys_name = find_library('maphys')
        if libmaphys_name == None:
            raise EnvironmentError("Could not find shared library: maphys."
                                   "The path to libmaphys.so should be in "
                                   "$LIBRARY_PATH")
        try:
            libmaphys = cdll.LoadLibrary(libmaphys_name)
        except:
            raise EnvironmentError("Could not load shared library: maphys."
                                   "The path to libmaphys.so should be in "
                                   "$LD_LIBRARY_PATH or $DYLD_LIBRARY_PATH on MacOS");

        return libmaphys

    def initialize(self):
        """
        Initialize maphys (job = -1)
        NB: 'comm', 'sym' and 'par' must have been set in the structure
        """
        self.drive(-1)
        self.__is_init = True

    def finalize(self):
        """
        Finalize maphys (job = -2)
        """
        if not self.__is_init:
            raise RuntimeError('Try to finalize maphys when maphys is not initialized')
        self.drive(-2)
        self.__is_init = False

    def drive(self, id_job=None):
        """
        Check data type and call maphys driver
        The job id must have been set if job_id is not given
        """
        self.__update_cntls()

        if id_job:
            self.mphs.job = id_job

        self.__driver(self.__mphs_ptr)

        if id_job > 0:
            self.__update_infos()

    def set_comm(self, mpi4py_comm):
        """
        Set the C communicator from the mpi4py one
        """
        MPI_Comm = self.__MPI_comm_type
        comm_ptr = MPI._addressof(mpi4py_comm)
        comm_val = MPI_Comm.from_address(comm_ptr)
        self.mphs.comm = comm_val

    def set_A(self, A):
        """
        Assume A can be cast into a scipy.sparse.coo_matrix
        """
        A = spp.coo_matrix(A, dtype=self.nptype)
        self.mphs.n = c_int(A.shape[0])
        nz = A.getnnz()
        self.mphs.nnz = nz

        # Fortran numbering
        self.__rows = np.array(A.row[:] + 1, dtype='i')
        self.__cols = np.array(A.col[:] + 1, dtype='i')

        self.mphs.rows = self.__rows.ctypes.data_as(POINTER(c_int))
        self.mphs.cols = self.__cols.ctypes.data_as(POINTER(c_int))

        if self.is_complex:
            self.__data = (self.__scal_type * nz)()
            for idx in range(0, nz):
                self.__data[idx].r = A.data.real[idx]
                self.__data[idx].i = A.data.imag[idx]
            self.mphs.values = cast(self.__data, POINTER(self.__scal_type))
        else:
            self.__data = A.data
            self.mphs.values = self.__data.ctypes.data_as(POINTER(self.__scal_type))


    def set_RHS(self, rhs, init_guess=None):
        """
        Assume rhs can be cast into a numpy.matrix
        Also sets initial guess when provided
        rhs shape is n x n_rhs
        """

        rhs = np.matrix(rhs, dtype=self.nptype)
        if init_guess:
            self.ICNTL[23] = 1
            init_guess = np.matrix(init_guess, dtype=self.nptype)

        n = rhs.shape[0]
        n_rhs = rhs.shape[1]
        # If the rhs was an array, assume it was just a vector
        if n == 1 and n_rhs > 1:
            rhs = rhs.getT()
            n = rhs.shape[0]
            n_rhs = rhs.shape[1]
            if init_guess:
                init_guess = init_guess.getT()

        self.mphs.nrhs = c_int(n_rhs)

        if self.is_complex:
            self.__rhs = (self.__scal_type * (n * n_rhs))()
            self.__sol = (self.__scal_type * (n * n_rhs))()
            for i in range(0, n):
                for j in range(0, n_rhs):
                    idx = j * n + i
                    self.__rhs[idx].r = rhs.real[i,j]
                    self.__rhs[idx].i = rhs.imag[i,j]
            if init_guess:
                for i in range(0, n):
                    for j in range(0, n_rhs):
                        idx = j * n + i
                        self.__sol[idx].r = init_guess.real[i,j]
                        self.__sol[idx].i = init_guess.imag[i,j]
            else:
                for i in range(0, n*n_rhs):
                    self.__sol[i].r = 0.0
                    self.__sol[i].i = 0.0

            self.mphs.rhs = cast(self.__rhs, POINTER(self.__scal_type))
            self.mphs.sol = cast(self.__sol, POINTER(self.__scal_type))

        else:
            # Send column major array (fortran style)
            self.__rhs = np.array(rhs.flatten('F'), dtype=self.nptype)
            if init_guess:
                self.__sol = np.array(init_guess.flatten('F'), dtype=self.nptype)
            else:
                self.__sol = np.zeros(shape=self.__rhs.shape, dtype=self.nptype)
            self.mphs.rhs = self.__rhs.ctypes.data_as(POINTER(self.__scal_type))
            self.mphs.sol = self.__sol.ctypes.data_as(POINTER(self.__scal_type))

    def distributed_interface(self, myinterface, myindexvi, myptrindexvi, myindexintrf):

        self.ICNTL[43] = 2 # Activate distributed interface
        self.__update_cntls()
        myndof = int(self.mphs.n)

        # Copy and cast to numpy array
        self.__myinterface  = np.array( myinterface  , dtype='i', copy=True) + 1 # Fortran numbering
        self.__myindexvi    = np.array( myindexvi    , dtype='i', copy=True)     # MPI numbering
        self.__myptrindexvi = np.array( myptrindexvi , dtype='i', copy=True) + 1 # Fortran numbering
        self.__myindexintrf = np.array( myindexintrf , dtype='i', copy=True) + 1 # Fortran numbering

        mynbvi = len(self.__myindexvi)
        mysizeintrf = len(self.__myinterface)
        if len(self.__myptrindexvi) != (mynbvi + 1):
            raise ValueError('myptrindexvi must be an array of size mynbvi + 1')
        if len(self.__myindexintrf) != self.__myptrindexvi[mynbvi] - 1:
            raise ValueError('Wrong size for myindexintrf: ' + str(len(self.__myindexintrf)) + ' VS ' + str(self.__myptrindexvi[mynbvi]-1))

        # Cast to c_int
        myndof = c_int(myndof)
        mysizeintrf = c_int(mysizeintrf)
        mynbvi = c_int(mynbvi)

        # Cast to c pointers
        self.__myinterface_ptr  =  self.__myinterface.ctypes.data_as(POINTER(c_int))
        self.__myindexvi_ptr    =    self.__myindexvi.ctypes.data_as(POINTER(c_int))
        self.__myptrindexvi_ptr = self.__myptrindexvi.ctypes.data_as(POINTER(c_int))
        self.__myindexintrf_ptr = self.__myindexintrf.ctypes.data_as(POINTER(c_int))

        self.__distributed_interface(self.__mphs_ptr, myndof, mysizeintrf, self.__myinterface_ptr, mynbvi, self.__myindexvi_ptr, self.__myptrindexvi_ptr, self.__myindexintrf_ptr)

    def get_solution(self):
        """
        Get solution as a numpy array
        """
        n = int(self.mphs.n)
        n_rhs = int(self.mphs.nrhs)

        if self.is_complex:
            sol = np.zeros((n,n_rhs,), dtype=self.nptype)
            for i in range(0, n):
                for j in range(0, n_rhs):
                    idx = j * n + i
                    sol.real[i,j] = self.mphs.sol[idx].r
                    sol.imag[i,j] = self.mphs.sol[idx].i
            return np.array(sol)

        else:
            if self.mphs.nrhs == 1:
                return np.array(self.mphs.sol[0:n], dtype=self.nptype)
            else:
                return np.array(self.mphs.sol[0:n*self.mphs.nrhs], dtype=self.nptype).reshape((n_rhs,n)).T
