def compute_box_shape(nDomains):
    """
    This function computes the triplet where each
    element is the number of domain in the corresponding dimension
    with optimal balancing.
    E.G. if nDomains = 24, this function returns [4,3,2]
    nDomains: number of domains in the cube
    """

    # Auxilary functions
    def largest_prime_factor(n):
        i = 2
        while i * i <= n:
            if n % i:
                i += 1
            else:
                n //= i
        return n

    def prime_factors(n):
        i = 2
        factors = []
        while i * i <= n:
            if n % i:
                i += 1
            else:
                n //= i
                factors.append(i)
        if n > 1:
            factors.append(n)
        return factors

    primes = prime_factors(nDomains)

    # In case we have less than 3 prime numbers, we add ones
    while len(primes) < 3:
        primes.append(1)

    # Sort ascending
    primes.sort()

    triple = [1,1,1]

    while primes:
        last = primes.pop()
        triple[0] *= last
        triple.sort()

    triple.sort(reverse=True)
    return triple
