from mpi4py import MPI
from pymaphys import Maphys
import scipy.sparse as spp
import numpy as np

# Initialize maphys wrapper
driver_maphys = Maphys('d')
# Get corresponding numpy type
nptype = driver_maphys.nptype

# MPI stuff
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
nprocs = comm.Get_size()

# Set fortran communicator
mphs = driver_maphys.mphs
driver_maphys.set_comm(comm)

# Init maphys
driver_maphys.initialize()

# Set matrix A
n = 9
row = np.array([0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 6, 7, 7, 7, 7, 8, 8, 8])
col = np.array([0, 1, 3, 0, 1, 2, 4, 1, 2, 5, 0, 3, 4, 6, 1, 3, 4, 5, 7, 2, 4, 5, 8, 3, 6, 7, 4, 6, 7, 8, 5, 7, 8])
data = np.array([4.0, -1.0, -1.0, -1.0, 4.0, -1.0, -1.0, -1.0, 4.0, -1.0, -1.0, 4.0, -1.0, -1.0, -1.0, -1.0, 4.0,
                 -1.0, -1.0, -1.0, -1.0, 4.0, -1.0, -1.0, 4.0, -1.0, -1.0, -1.0, 4.0, -1.0, -1.0, -1.0, 4.0], dtype=nptype)

NRHS = 2
if driver_maphys.is_complex:
    data.imag += 1
A = spp.coo_matrix((data, (row, col)), shape=(n, n))
mphs.sym = 0

th_sol = np.zeros((n, NRHS,), dtype=nptype)
for i in range(NRHS):
    k = i * NRHS
    if driver_maphys.is_complex:
        th_sol[:,i].real = np.arange(k+1.0, k+n+1.0)
        th_sol[:,i].imag = np.arange(-n-k, -k)
    else:
        th_sol[:,i] = np.arange(k+1.0, k+n+1.0, dtype=nptype)

# Set rhs
rhs = np.matmul(A.todense(), th_sol)

if rank == 0:
    driver_maphys.set_A(A)
    driver_maphys.set_RHS(rhs)

# Set icntl
driver_maphys.ICNTL[4] = 5
driver_maphys.ICNTL[5] = 1
driver_maphys.ICNTL[6] = 1
driver_maphys.ICNTL[7] = 4
driver_maphys.ICNTL[13] = 1
driver_maphys.ICNTL[20] = 4
driver_maphys.ICNTL[21] = 1
driver_maphys.ICNTL[24] = 500
driver_maphys.ICNTL[26] = 500
driver_maphys.ICNTL[28] = 0
driver_maphys.ICNTL[43] = 1

# Call maphys
driver_maphys.drive(6)

be = driver_maphys.RINFOG[2]

if rank == 0:
    sol = driver_maphys.get_solution()
    print("Found:")
    print(sol)
    print("Th. solution:")
    print(th_sol)
    print("Backward error:")
    print(be)

# Finalize maphys
driver_maphys.finalize()

if rank == 0:
    print("done")
