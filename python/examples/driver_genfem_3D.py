# -*- coding:utf-8 -*-

import time

t_0 = time.time() ##################

import numpy as np
import genfem as fem
from mpi4py import MPI
from pymaphys import Maphys
import input
import box

t_loadpy = time.time() ##################

# MPI stuff
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
nprocs = comm.Get_size()

t_mpi4py = time.time() ##################

# Generate local matrix, rhs, domain with genfem
cube_base, K_, nLineK = input.get_genfem_param()

# Get box dimensions from prime decomposition
nDom = box.compute_box_shape(nprocs)
shape = [cube_base*n+1 for n in nDom]

limits, neighbors, interfaces = fem.subdomain(shape, nDom, rank)
loc2glob, glob2loc, interfaces, interface = fem.local_ordering(shape, limits, neighbors, interfaces)
A, rhs = fem.stratified_heat(shape, K1=1, K2=K_, nLineK=nLineK, rhs_sparse=False, symmetry=True, lim=limits, loc2glob=loc2glob, glob2loc=glob2loc)

t_genmat = time.time() ##################

# Call MaPHyS

# Initialize maphys wrapper
driver_maphys = Maphys('d')
nptype = driver_maphys.nptype
mphs = driver_maphys.mphs
driver_maphys.set_comm(comm)
# Init maphys
driver_maphys.initialize()

# Set parameters
input.get_input(driver_maphys)

driver_maphys.set_A(A)
driver_maphys.set_RHS(rhs)

# Set distributed interface

myinterface = interface
myindexvi = neighbors
myptrindexvi = [0]
k = 0
for idx in interfaces:
    k = k + len(idx)
    myptrindexvi.append(k)
myindexintrf = np.concatenate(interfaces)

t_setup = time.time() ##################

# Run MaPHyS
driver_maphys.distributed_interface(myinterface, myindexvi, myptrindexvi, myindexintrf)
driver_maphys.drive(6)
driver_maphys.finalize()

comm.Barrier()
t_maphys = time.time() ##################

# Get a solution
sol = driver_maphys.get_solution()
if rank == 0:
    print(sol[:min(20, len(sol))])

if rank == 0:

    print("tLoadingPython: " + str(t_loadpy -      t_0)[0:5])
    print("tGenMat: "        + str(t_genmat - t_loadpy)[0:5])
    print("tSetup: "         + str(t_setup  - t_genmat)[0:5])
    print("tMaPHyS: "        + str(t_maphys -  t_setup)[0:5])

    print("cube_per_dim: "   + str(nDom))
    print("dom_shape: "      + str(shape))
