#!/usr/bin/env python

import os, re

dir_path = os.path.dirname(os.path.realpath(__file__))

mph_def_file = os.path.join(dir_path, '..', 'include', 'mph_defs_f.h')
c_intrf_file = os.path.join(dir_path, '..', 'include', 'xmph_maphys_type_c.h')
py_intrf_infile = os.path.join(dir_path, 'pymaphys', 'maphys_struct.py.in')
py_intrf_outfile = os.path.join(dir_path, 'pymaphys', 'maphys_struct.py')

capture = [
    "MAPHYS_STRL",
    "MAPHYS_ICNTL_SIZE",
    "MAPHYS_RCNTL_SIZE",
    "MAPHYS_IINFO_SIZE",
    "MAPHYS_RINFO_SIZE",
    "MAPHYS_IINFOG_SIZE",
    "MAPHYS_RINFOG_SIZE",
    "MAPHYS_IKEEP_SIZE",
    "MAPHYS_RKEEP_SIZE"
]

capt_dict = {}

# Capture constant values
with open(mph_def_file, 'r') as f_intrf:
    for line in f_intrf:

        for cap in capture:
            reg = "#define\s" + cap + "\s+(\d+)"
            match = re.search(reg, line)

            if match:
                capt_dict[cap] = match.group(1)
                break

# Capture maphys C structure

# C to python type dictionnary
types = {
    'MPI_Comm' : 'MPI_Comm',
    'int' : 'c_int',
    'XMPH_CFLOAT' : 'xmph_cfloat',
    'double' : 'c_double',
    'char' : 'c_char',
}

python_structure = ""

with open(c_intrf_file, 'r') as fin:
    line = fin.readline()
    while line and 'MPI_Comm' not in line:
        line = fin.readline()

    while line and 'XMPH_maphys_t_c' not in line:

        type_out = ""

        # Pass comments
        if '/*' in line:
            while line and '*/'  not in line:
                line = fin.readline()

        if '//' not in line:

            for c_t in types.keys():

                if c_t in line:

                    # Check pointer types
                    regex = c_t + '\s+\*\s*(\w+)\s*;'
                    match = re.search(regex, line)

                    if match:
                        type_out = '("' + match.group(1) + '", POINTER(' + types[c_t] +')),'
                        break

                    # Check table types
                    regex = c_t + '\s+(\w+)\[((\w|\+|-|\*|\s)+)\]\s*;'
                    match = re.search(regex, line)

                    if match:
                        type_out = '("' + match.group(1) + '", ' + types[c_t] +' * (self.' + match.group(2) + ')),'
                        break

                    # Check regular types
                    regex = c_t + '\s+(\w+)\s*;'
                    match = re.search(regex, line)

                    if match:
                        type_out = '("' + match.group(1) + '", ' + types[c_t] + '),'

        if type_out:
            python_structure += '            ' + type_out + '\n'

        line = fin.readline()

# Replace in python file

with open(py_intrf_infile, 'r') as fin:
    with open(py_intrf_outfile, 'w') as fout:

        for l_in in fin:
            l_out = l_in
            for cap in capture:
                if cap in l_in:
                    pattern = '%' + cap + '%'
                    repl = capt_dict[cap]
                    l_out = re.sub(pattern, repl, l_in)
                    break

            if '%FIELDS%' in l_in:
                l_out = python_structure

            fout.write(l_out)
