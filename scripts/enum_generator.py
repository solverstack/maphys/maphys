#
# This python script is used to generate from a yaml file
# in ../hybrid/enum_maphys.yaml :
# - The fortran enumeration with the constant name (../hybrid/mph_maphys_enum.F90)
# - The documentation for control parameters and info output
# (../doc/userguide/control_parameters.tex and ../doc/userguide/info_parameters.tex)
# - The refcard documentation (../doc/userguide/refcard.tex)
# - The array sizes for ICNTL, RCNTLS, IINFO, IKEEP... (../include/mph_defs_f.h)
#
# The point of this generation is to have consistent information
# in the code and the documentation, and be able to modify
# the parameters more quickly just by modifying the yaml file.
#
# Author: Gilles Marait
# Date: 06/2017
#

import yaml
import os
import re

# Get current directory path
dir_path = os.path.dirname(os.path.realpath(__file__))

# Set file names
VERSION_FILE   = os.path.join(dir_path, '..', 'VERSION')
INPUT_FNAME    = os.path.join(dir_path, '..', 'hybrid', 'enum_maphys.yaml')
DOC_CNTL_FNAME = os.path.join(dir_path, "control_parameters.tex")
DOC_INFO_FNAME = os.path.join(dir_path, "info_parameters.tex")
ENUM_FNAME     = os.path.join(dir_path, "mph_maphys_enum.F90")
REFCARD_FNAME  = os.path.join(dir_path, "refcard.tex")
HEADER_FNAME   = os.path.join(dir_path, '..', 'include', 'mph_defs_f.h')
DEFAULT_FNAME  = os.path.join(dir_path, '..', 'hybrid', 'xmph_maphys_aux_mod.F90')

# Put automatic references to CNTL/INFO
# ICNTL(40) -> \ref{itm:icntl40}
reg_str = "((RCNTL|ICNTL|IINFOG|RINFOG|IINFO|RINFO)\((\d+)\))"
expr = re.compile(reg_str)
def put_ref(description):
    def replace(match):
        listname = match.group(2)
        num = match.group(3)
        return r"\ref{itm:" + listname.lower() + num + "}"
    return re.sub(expr, replace, description)

# Closure avoid putting multiple nested devversion condition
# Use: tex_dev(file) ... dev code ... end_tex_dev(file)
current_param = ""
current_hide_level = [0]
def tex_dev(tex_file):
    if current_hide_level[0] == 0:
        tex_file.write("\iftoggle{devversion}{\n\\textcolor{red}{\n")
    current_hide_level[0] += 1
def end_tex_dev(tex_file):
    if  current_hide_level[0] == 1:
        tex_file.write("}\n}{}\n")
    current_hide_level[0] -= 1

# Get version
with open(VERSION_FILE, "r") as vf:
    VERSION = vf.readline().strip()

# Create KEEP from CNTL constant name
def cntl_to_keep(var_name):
    return re.sub("CNTL", "KEEP", var_name)

#
# Parse the list of ICNTL/RCNTL/IINFO/RINFO/IINFOG/RINFOG
#
def parse_list(listname, list_dict, doc_tex, refcard, fortran_enum):
    """
    Parse a list and generate doc and code.
    listname: one of (ICNTL,RCNTL,IINFO,RINFO,IINFOG,RINFOG)
    list_dict: dictionary parsed from yaml
    doc_tex: doc tex file
    refcard: refcard tex file
    fortran_enum: fortran constant file
    """

    is_info = "INFO" in listname

    # Function to parse DESCRIPTION, NOTE and WARNING keys
    def write_tex_description(file, val, data, label=None):

        strlabel = ""
        if label:
            strlabel = "\label{" + label + "}"
        if "DESCRIPTION" in data.keys():
            desc = put_ref(str(data["DESCRIPTION"]))
            file.write("\item [" + str(val) + strlabel + "]: " + desc + "\n")
            if "WARNING" in data.keys():
                file.write(r"\\ \textbf{Warning: " + str(put_ref(data["WARNING"])))
                file.write("}\n")
            if "NOTE" in data.keys():
                file.write(r"\\ \textit{Note: " + str(put_ref(data["NOTE"])))
                file.write("}\n")

            if "NAME" in data.keys():
                texname = re.sub("_", "\_", str(data["NAME"]))
                tex_dev(file)
                file.write(r"\\ \*\emph{Enumeration: " + texname + "}")
                end_tex_dev(file)

    for enum_val, d in list_dict.items():

        current_param = listname + " " + str(enum_val)

        try:

            #DEBUG
            #print(listname + str(enum_val))

            # Copy description into brief when needed
            if "BRIEF" not in d.keys():
                d["BRIEF"] = d["DESCRIPTION"]
            if "VALUES" in d.keys():
                for dval in d["VALUES"].values():
                    if "BRIEF" not in dval.keys():
                        dval["BRIEF"] = dval["DESCRIPTION"]

            # Documentation
            if "IS_HIDDEN" in d.keys():
                tex_dev(doc_tex)
            labelname = listname.lower() + str(enum_val)
            write_tex_description(doc_tex, listname + "(" + str(enum_val) + ")", d, label="itm:" + labelname)

            if "VALUES" in d.keys() or "DEFAULT" in d.keys():
                doc_tex.write(r"\begin{description}" + "\n")

            if "VALUES" in d.keys():
                for val, dval in d["VALUES"].items():
                    write_tex_description(doc_tex, str(val), dval)

            if "HIDDEN_VALUES" in d.keys():
                tex_dev(doc_tex)
                for val, dval in d["HIDDEN_VALUES"].items():
                    write_tex_description(doc_tex, str(val), dval)
                end_tex_dev(doc_tex)

            if "DEFAULT" in d.keys():
                doc_tex.write("\item[Default]: " + str(d["DEFAULT"]) + "\n")

            if "VALUES" in d.keys() or "DEFAULT" in d.keys():
                doc_tex.write("\end{description}\n")

            if "IS_HIDDEN" in d.keys():
                end_tex_dev(doc_tex)

            # Refcard
            if "IS_HIDDEN" not in d.keys():

                if is_info:
                    refcard.write(listname +"(" + str(enum_val) + ") & " + str(d["BRIEF"]) + "\\\\\n")
                else:
                    refcard.write(listname +"(" + str(enum_val) + ") & " + str(d["BRIEF"]) + " & " + str(d["DEFAULT"]) + "\\\\\n")

                if "VALUES" in d.keys():
                    for val, dval in d["VALUES"].items():
                        if is_info:
                            refcard.write(" & " + str(val) + ": " + str(dval["BRIEF"]) + r" \\" + "\n")
                        else:
                            refcard.write(" & " + str(val) + ": " + str(dval["BRIEF"]) + r" & \\" + "\n")

                refcard.write("\hline\n")

            fortran_enum.write("  ! " + str(d["BRIEF"]) + "\n")
            fortran_enum.write("  Integer, Parameter :: " + str(d["NAME"]) + " = " + str(enum_val) + "\n")
            if "CNTL" in listname:
                fortran_enum.write("  Integer, Parameter :: " + cntl_to_keep(str(d["NAME"])) + " = " + str(enum_val) + "\n")

            if "VALUES" in d.keys():
                for val, dval in d["VALUES"].items():
                    if "NAME" in dval.keys():
                        fortran_enum.write("  Integer, Parameter :: " + str(dval["NAME"]) + " = " + str(val) + "\n")

            hidden_written = False
            if "HIDDEN_VALUES" in d.keys():
                for val, dval in d["HIDDEN_VALUES"].items():
                    if "NAME" in dval.keys():
                        # Avoid writing the comment several times
                        if not hidden_written:
                            fortran_enum.write("  ! Hidden values\n")
                            hidden_written = True
                        fortran_enum.write("  Integer, Parameter :: " + str(dval["NAME"]) + " = " + str(val) + "\n")

            fortran_enum.write("\n")

        except KeyError as e:
            print("Error in: " + current_param)
            print("KeyError: could not find key")
            print(e)

#
# Main phase: open the yaml file and parse it to generate code and doc
#
with open(INPUT_FNAME, 'r') as mph_enum:

    # Get the data from the yaml
    try:
        data = yaml.load(mph_enum)
    except yaml.YAMLError as exc:
        if hasattr(exc, 'problem_mark'):
            mark = exc.problem_mark
            print("YAML ERROR:")
            print("Error position: (%s:%s)" % (mark.line+1, mark.column+1))

    ctrl_tex = open(DOC_CNTL_FNAME, 'w')
    refcard = open(REFCARD_FNAME, 'w')
    fortran_enum = open(ENUM_FNAME, 'w')

    # Headers
    ctrl_tex.write(r"\section{List of control parameters}\label{sec:controlpar}" + "\n")
    ctrl_tex.write(r"The execution of \maphys{} can be controlled by the user through the 2 arrays \textbf{mphs\%ICNTL} and \textbf{mphs\%RCNTL}. When an execution control should be expressed as an integer (or an equivalent), this control is a entry of \textbf{mphs\%ICNTL}; and when an execution control should be expressed as a float (or an equivalent), this control is a entry of \textbf{mphs\%RCNTL}." + "\n")

    refcard.write(r"\documentclass[10pt,twoside]{article}" + "\n")
    refcard.write(r"\usepackage[left=1cm,right=1cm,top=2cm,bottom=2cm]{geometry} \usepackage{xspace} \usepackage{longtable}" + "\n")
    refcard.write(r"""\newcommand{\mkl}{\textsc{MKL}\xspace}
    \newcommand{\lapack}{\textsc{LAPACK}\xspace}
    \newcommand{\blas}{\textsc{BLAS}\xspace}
    \newcommand{\scalapack}{\textsc{ScaLAPACK}\xspace}
    \newcommand{\mumps}{\textsc{MUMPS}\xspace}
    \newcommand{\hwloc}{\textsc{HWLOC}\xspace}
    \newcommand{\scotch}{\textsc{SCOTCH}\xspace}
    \newcommand{\pastix}{\textsc{PaStiX }\xspace}
    \newcommand{\fabulous}{\textsc{FABuLOuS}\xspace}
    \newcommand{\maphys}{\textsc{MaPHyS}\xspace}
    \newcommand{\mpi}{\textsc{MPI}\xspace} """ + "\n")
    refcard.write(r"\begin{document}" + "\n")
    refcard.write(r"\section*{\maphys version " + VERSION + " }\n")

    fortran_enum.write(r"! This file was generated, DO NOT MODIFY THIS FILE DIRECTLY" + "\n")
    fortran_enum.write(r"! Modifications must be made to enum_maphys.yaml" + "\n")
    fortran_enum.write(r"! Use the python script enum_generator.py in the scripts directory" + "\n")
    fortran_enum.write(r"! List of aliases used in MaPHyS" + "\n\n")
    fortran_enum.write("Module MPH_maphys_enum\n  Implicit none\n\n")

    # ICNTLS and RCNTLS

    for CNTL in ["ICNTL", "RCNTL"]:

        ctrl_tex.write(r"\subsection{" + CNTL + "}\n")
        ctrl_tex.write(r"\textbf{mphs\%" + CNTL + r"} is an integer array of size \textbf{MAPHYS\_" + CNTL + "\_SIZE}.\n")
        ctrl_tex.write(r"\begin{description}" + "\n")

        refcard.write(r"\subsection*{" + CNTL + "}\n")
        refcard.write(r"\begin{longtable}[H]{| p{.1\textwidth} | p{.7\textwidth} | p{.1\textwidth} |} \hline  " + "\n")
        refcard.write(r"Parameter & Description and values & Default value\\ \hline " + "\n")

        fortran_enum.write(r"  ! Control parameters " + CNTL + "\n\n")

        parse_list(CNTL, data[CNTL], ctrl_tex, refcard, fortran_enum)

        ctrl_tex.write(r"\end{description}" + "\n")
        refcard.write(r"\end{longtable}" + "\n")

    # IINFOS, RINFOS, IINFOG and RINFOG
    ctrl_tex.close()
    info_tex = open(DOC_INFO_FNAME, 'w')

    info_tex.write(r"\section{List of information parameters}" + "\n")
    info_tex.write(r"\subsection{Information local to each process}" + "\n")

    for INFO in ["IINFO", "RINFO", "IINFOG", "RINFOG"]:

        if INFO == "IINFOG":
            info_tex.write(r"\subsection{Information available on all processes}" + "\n")

        info_tex.write(r"\subsubsection{" + INFO + "}\n")
        info_tex.write(r"\textbf{mphs\%" + INFO + r"} is an integer array of size \textbf{MAPHYS\_" + INFO + "\_SIZE}.\n")

        if INFO == "IINFO" or INFO == "RINFO":
            info_tex.write("By default, a value of -1 indicates that the data is not available.\n")

        info_tex.write(r"\begin{description}" + "\n")

        refcard.write(r"\subsection*{" + INFO + "}\n")
        refcard.write(r"\begin{longtable}[H]{| p{.15\textwidth} | p{.75\textwidth} | } \hline  " + "\n")
        refcard.write(r" Output & Description and values \\ \hline " + "\n")

        fortran_enum.write(r"  ! Info parameters " + INFO + "\n\n")

        parse_list(INFO, data[INFO], info_tex, refcard, fortran_enum)

        info_tex.write(r"\end{description}" + "\n")
        refcard.write(r"\end{longtable}" + "\n")

    # Compute the size of the arrays needed to contain the parameters
    all_parameters = ['ICNTL', 'RCNTL', 'IINFO', 'RINFO', 'IINFOG', 'RINFOG', 'IKEEP', 'RKEEP']
    max_val = {}
    for param in all_parameters:
        if param in data.keys():
            max_param = max([val for val in data[param].keys()])
            max_val[param] = max_param
    max_val["IKEEP"] = 0
    max_val["RKEEP"] = 0

    # Write extra KEEP parameters needed in the fortran file
    # Start counting keep after the last number of cntl
    for KEEP, start in zip(["IKEEP", "RKEEP"], [max_val["ICNTL"]+1, max_val["RCNTL"]+1]):
        value = start
        if KEEP in data.keys():
            d = data[KEEP]
            fortran_enum.write("  ! Extra definitions for " + KEEP + "\n\n")
            for definition in d["AUTOMATIC"]:
                fortran_enum.write("  Integer, Parameter :: " + str(definition) + " = " + str(value) + "\n")
                value += 1
            for val, definition in d["VALUED"].items():
                fortran_enum.write("  Integer, Parameter :: " + str(definition) + " = " + str(val) + "\n")
            fortran_enum.write("\n")
        max_val[KEEP] = value - 1

    #print(max_val)

    # FOOTER
    refcard.write(r"\end{document}")

    fortran_enum.write(r"End Module MPH_maphys_enum")

    info_tex.close()
    refcard.close()
    fortran_enum.close()

    # Replace array sizes in maphys header
    TMP_HEADER = os.path.join(dir_path, "tmp.h")
    with open(HEADER_FNAME, 'r') as h_in:
        h_out = open(TMP_HEADER, 'w')
        for l in h_in:
            l_out = l

            # Change version
            l_out = re.sub("MAPHYS_VERSION\s+'.+'" , "MAPHYS_VERSION '" + str(VERSION) +"'" , l_out)

            # Check all the substitutions
            for param in all_parameters:
                l_out = re.sub("MAPHYS_" + param + "_SIZE\s+\d+" , "MAPHYS_" + param + "_SIZE  " + str(max_val[param]) , l_out)
            h_out.write(l_out)
        h_out.close()

    # Set default values in maphys_aux_mod file
    TMP_MAPHYS_AUX = os.path.join(dir_path, "tmp2.F90")
    with open(DEFAULT_FNAME, 'r') as f_in:
        f_out = open(TMP_MAPHYS_AUX, 'w')
        l_in = f_in.readline()
        while l_in:

            f_out.write(l_in)

            # Copy default icntl values after this line
            if '>>>GENERATED ICNTL VALUES' in l_in:
                for d in data['ICNTL'].values():
                    def_val = str(d['DEFAULT']).split()[0]
                    l_out = '    ICNTL(' + str(d['NAME']) + ') = ' + str(def_val) + "\n"
                    f_out.write(l_out)

                # Go to the end of the session
                l_in = f_in.readline()
                while l_in and ( not '>>>END OF GENERATED ICNTL VALUES' in l_in):
                    l_in = f_in.readline()
                f_out.write(l_in)

            if '>>>GENERATED RCNTL VALUES' in l_in:
                for d in data['RCNTL'].values():
                    def_val = str(d['DEFAULT']).split()[0]
                    l_out = '    RCNTL(' + str(d['NAME']) + ') = ' + str(def_val) + "_8\n"
                    f_out.write(l_out)

                # Go to the end of the session
                l_in = f_in.readline()
                while l_in and ( not '>>>END OF GENERATED RCNTL VALUES' in l_in):
                    l_in = f_in.readline()
                f_out.write(l_in)

            l_in = f_in.readline()

        f_out.close()

# Finally put the generated files at the correct places
os.system("mv " + TMP_HEADER + " " + HEADER_FNAME)
os.system("mv " + TMP_MAPHYS_AUX + " " + DEFAULT_FNAME)
os.system("mv " + DOC_CNTL_FNAME + " " + os.path.join(dir_path, "..", "doc", "userguide"))
os.system("mv " + DOC_INFO_FNAME + " " + os.path.join(dir_path, "..", "doc", "userguide"))
os.system("mv " + REFCARD_FNAME  + " " + os.path.join(dir_path, "..", "doc", "userguide"))
os.system("mv " + ENUM_FNAME     + " " + os.path.join(dir_path, "..", "hybrid"))
