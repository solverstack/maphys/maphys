#!/bin/bash

dir=$(dirname $0)
cd $dir/..

FILES=`find . | grep -i "\.f90"`

for f1 in $FILES
do
    FUNCNAMES=`cat $f1 | grep -i "end subroutine" | awk '{print $3}'`
    FUNCNAMES="$FUNCNAMES `cat $f1 | grep -i "end function" | awk '{print $3}'`"
    for func in $FUNCNAMES
    do
        for f2 in $FILES
        do
            sed -i -e "s/$func/$func/gI" $f2
        done
    done
done
