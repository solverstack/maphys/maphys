
###
#
# @file Makefile
#
#  MaPHyS is a software package provided by INRIA.
#  Generate the user's guide in different formats
#
# @version 0.1.4
# @author Yohan Lee-tin-yien
# @date 2011-07-11
#
###

SHELL=bash # For echo issues

# generate the user guide in different
MAPHYS_VERSION=$(shell sed -n '1p' ../../VERSION)
# if DEVVERSION is set to YES, generate a user guide for the developpement version.
# developpement version user guide contains:
# - unimplemented features.
# - in developement features.
# - several enumerations.
DEVVERSION?=NO
TEX2PDF=pdflatex
ETAGS=etags

users_guide_SOURCES =  notes.tex users_guide.tex main_functionalities.tex \
	call_sequence.tex io_parameters.tex \
	control_parameters.tex  info_parameters.tex error_diags.tex \
	examples.tex version.tex installation.tex  intro.tex

.PHONY: pdf dvi ps html TAGS
all: pdf
pdf: ../users_guide.pdf 

../users_guide.pdf : $(users_guide_SOURCES)
	$(TEX2PDF) $(@F:.pdf=.tex) ; $(TEX2PDF) $(@F:.pdf=.tex)
	bibtex users_guide
	$(TEX2PDF) $(@F:.pdf=.tex) ; $(TEX2PDF) $(@F:.pdf=.tex)
	mv $(@F) $@
	$(RM) *.ky *.cp *.fn *.pg *.tp *.vr *.vrs *.toc *.aux *.log version.tex

refcard: ../refcard.pdf

../refcard.pdf: refcard.tex
	$(TEX2PDF) $(@F:.pdf=.tex) ; $(TEX2PDF) $(@F:.pdf=.tex)
	$(TEX2PDF) $(@F:.pdf=.tex) ; $(TEX2PDF) $(@F:.pdf=.tex)
	mv $(@F) $@

users_guide.tex:version.tex

version.tex:../../VERSION 
	echo '\newcommand{\mversion}{$(MAPHYS_VERSION)}' > $@
	echo '\newtoggle{devversion}' >> $@
ifneq (,$(findstring YES,$(DEVVERSION)))
	echo '\toggletrue{devversion}' >> $@
else 
	echo '\togglefalse{devversion}' >>$@
endif

TAGS : $(users_guide_SOURCES)
	$(ETAGS) $^

clean:
	$(RM) ../users_guide.pdf
	$(RM) ../refcard.pdf
	$(RM) version.tex
	$(RM) *.blg *.pdfsync *.bbl *.ky *.cp *.fn *.pg *.tp *.vr *.vrs *.toc *.aux *.log *.out


