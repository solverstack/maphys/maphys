/* Example program using the C interface to the
 * double precision version of Maphys, dmph_maphys_driver_c. */
#include <stdio.h>
#include "mpi.h"
#include "dmph_maphys_type_c.h"
#define JOB_INIT -1
#define JOB_END -2
#define USE_COMM_WORLD -987654
int main(int argc, char ** argv) {
  DMPH_maphys_t_c id;
  int n = 1000;
  int nnz = 5998;
  int i;
  int irn[nnz];
  int jcn[nnz];
  double a[nnz];
  double rhs[n];
  double sol[n];
  int myid, ierr;
  ierr = MPI_Init(&argc, &argv);
  ierr = MPI_Comm_rank(MPI_COMM_WORLD, &myid);

  /* Initialize a Maphys instance. Use MPI_COMM_WORLD. */
  id.job=JOB_INIT; id.sym=2;id.comm=MPI_COMM_WORLD;
  dmph_maphys_driver_c(&id);

  /* Define A and rhs */
  for (i=0; i<1000; i++) {
    rhs[i]=1.0;
    a[i]=(i+1)*1.0;
    irn[i]=i+1;
    jcn[i]=i+1;
    sol[i]=0.0;
  };
  for (i=0; i<999; i++) {
    a[1000+i]=(i+1)*1.0;
    irn[1000+i]=i+2;
    jcn[1000+i]=i+1;
  };
  for (i=0; i<999; i++) {
    a[1999+i]=(i+1)*1.01;
    irn[1999+i]=i+1;
    jcn[1999+i]=i+2;
  };
  for (i=0; i<1000; i++) {
    a[2998+i]=(i+1)*1.0;
    irn[2998+i]=i+1;
    jcn[2998+i]=499;
    a[3998+i]=(i+1)*1.0;
    irn[3998+i]=i+1;
    jcn[3998+i]=500;
    a[4998+i]=(i+1)*1.0;
    irn[4998+i]=i+1;
    jcn[4998+i]=501;
  };
  id.sym=2;

  /* Define the problem on the host */
  if (myid == 0) {
    id.n = n; id.nnz =nnz; id.rows=irn; id.cols=jcn; id.sol=sol;
    id.values = a; id.rhs = rhs; id.nrhs=1;
  }
#define ICNTL(I) icntl[(I)-1] /* macro s.t. indices match documentation */
#define RCNTL(I) rcntl[(I)-1] /* macro s.t. indices match documentation */
  /* Call the Maphys package. */
  id.ICNTL(6) = 2;
  id.ICNTL(24) = 500;
  id.ICNTL(26) = 500;
  id.ICNTL(27) = 1;
  id.ICNTL(22) = 3;
  id.RCNTL(21) = 1.0e-5;
  id.RCNTL(11) = 1.0e-6;
  id.job=6;
  dmph_maphys_driver_c(&id);
  id.job=JOB_END; dmph_maphys_driver_c(&id); /* Terminate instance */
  if (myid == 0) {
    printf("Ending maphys ok");
  }
  ierr = MPI_Finalize();
  if (ierr != 0) {
    fprintf(stderr, "Problem with mpi finalise");
  }
  return 0;
}
