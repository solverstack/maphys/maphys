
\section{General overview}
\label{generalOverview}
MaPHyS is a software package for the solution of sparse linear system
$$
{\cal A} x = b 
$$
where ${\cal  A}$ is  a square  real or  complex non  singular general
matrix, $b$ is a given right-hand side vector, and $x$ is the solution
vector to be computed.  It  follows a non-overlapping algebraic domain
decomposition method  that first reorders  the linear system into  a $2
\times 2$ block system
\begin{equation}
\left(
\begin{array}{cc}
{\cal A}_{II} & {\cal A}_{I\Gamma} \\
{\cal A}_{\Gamma I} & {\cal A}_{\Gamma\Gamma} \\
\end{array}
\right)
\left( \begin{array}{c} x_{I} \\ x_{\Gamma} \\ \end{array} \right) =
\left( \begin{array}{c} b_{I} \\ b_{\Gamma} \\ \end{array} \right),
\end{equation}
where  ${\cal  A}_{II}$  and  ${\cal  A}_{\Gamma\Gamma}$  respectively
represent interior subdomains and separators, and ${\cal A}_{I\Gamma}$
and  ${\cal  A}_{\Gamma I}$  are  the  coupling between  interior  and
separators.  By eliminating the  unknowns associated with the interior
subdomains ${\cal A}_{II}$ we get
\begin{equation}
\left(
\begin{array}{cc}
{\cal A}_{II} & {\cal A}_{I\Gamma} \\
0 & {\cal S} \\
\end{array}
\right)
\left( \begin{array}{c} x_{I} \\ x_{\Gamma} \\ \end{array} \right) =
\left( \begin{array}{c} b_{I} \\ f \\ \end{array} \right),
\end{equation}
with
\begin{equation}
{\cal S}={\cal A}_{\Gamma\Gamma}-{\cal A}_{\Gamma I} {\cal A}_{II}^{-1} {\cal A}_{I\Gamma}
\; \textrm{ and} \;
f = b_\Gamma -{\cal A}_{\Gamma I} {\cal A}_{II}^{-1} b_{I}.
\end{equation}
The matrix  ${\cal S}$  is referred  to as  the {\it  Schur complement
  matrix}.   Because  most  of  the   fill-in  appears  in  the  Schur
complement,   the  Schur   complement   system  is   solved  using   a
preconditioned  Krylov subspace  method while  the interior  subdomain
systems are solved using a  sparse direct solver.  Although, the Schur
complement  system  is  significantly   better  conditioned  than  the
original  matrix  ${\cal A}$,  it  is  important to  consider  further
preconditioning when employing a Krylov method.

In   MaPHyS,  several   overlapping  block   diagonal  preconditioning
techniques are  implemented, where  each diagonal block  is associated
with the interface of a subdomain:
\begin{enumerate}
 \item  dense block  diagonal  factorization: each  diagonal block  is
   factorized using the appropriated \lapack subroutine,
 \item  sparsified block  diagonal factorization:  the dense  diagonal
   block is first  sparsified by droppring entry $s_{i,j}$  if it is
   lower than $\xi ( \vert s_{i,i}  \vert + \vert s_{j,j}\vert )$. The
   sparse factorization is performed by a sparse direct solver.
 \item  sparse   approximation  of   the  diagonal  block:   a  sparse
   approximation of the diagonal block is computed by replacing ${\cal
     A}_{II}^{-1}$  by an  incomplete  $ILU(t,p)$ factorization.   The
   computed  approximation   of  the   Schur  complement   is  further
   sparsified.

\end{enumerate}

Because   of   its   diagonal   nature   (consequently   local),   the
preconditioner  tends  to  be  less   efficient  when  the  number  of
subdomains is  increased.  The  efficient solution provided  by MaPHyS
results from a trade-off between  the two contradictory ideas that are
increasing the  number of  domains to  reduce the  cost of  the sparse
factorization of  the interior  domain on one  hand; and  reducing the
number  of  domains to  make  easier  the  iterative solution  for  the
interface solution on the other hand.

\section{General introduction}
%
Solving large sparse  linear systems ${\cal A} x=b$,  where ${\cal A}$
is a given matrix, $b$ is a given vector, and $x$ is an unknown vector
to be  computed, appears  often in the  inner-most loops  of intensive
simulation  codes.    It  is  consequently  the   most  time-consuming
computation in  many large-scale  computer simulations in  science and
engineering,  such  as  computational incompressible  fluid  dynamics,
structural analysis, wave propagation, and  design of new materials in
nanoscience, to name a few.  Over the past decade or so, several teams
have  been  developing  innovative  numerical  algorithms  to  exploit
advanced  high performance,  large-scale parallel  computers to  solve
these  equations  efficiently.  There  are  two  basic approaches  for
solving  linear systems  of  equations: direct  methods and  iterative
methods.  Those  two large  classes of  methods have  somehow opposite
features with  respect to their numerical  and parallel implementation
efficiencies.

Direct methods are based on  the Gaussian elimination that is probably
among the oldest method for solving linear systems.  Tremendous effort
has been  devoted to the  design of sparse Gaussian  eliminations that
efficiently exploit the sparsity of the matrices. These methods indeed
aim at  exhibiting dense submatrices  that can then be  processed with
computational intensive standard dense  linear algebra kernels such as
\blas, \lapack  and \scalapack.  Sparse  direct solvers have  been for
years the  methods of choice  for solving linear systems  of equations
because of their reliable numerical behavior~\cite{hig:02}.  There are
on-going  efforts in  further  improving  existing parallel  packages.
However, it  is admitted  that such  approaches are  intrinsically not
scalable  in terms  of computational  complexity or  memory for  large
problems  such  as those  arising  from  the discretization  of  large
3-dimensional partial differential equations (PDEs).
%Those efforts are mainly related to advanced software engineering and
%we might expect effective scalability up to a few tens or hundreds of
%cores but not beyond.
Furthermore, the  linear systems involved in  the numerical simulation
of  complex phenomena  result  from some  modeling and  discretization
which   contains   some   uncertainties  and   approximation   errors.
Consequently,  the highly  accurate  but costly  solution provided  by
stable Gaussian eliminations might not be mandatory.

Iterative  methods,   on  the   other  hand,  generate   sequences  of
approximations to the  solution either through fixed  point schemes or
via  search  in  Krylov   subspaces~\cite{saad:03}.   The  best  known
representatives of these latter numerical techniques are the conjugate
gradient~\cite{hest:52} and  the GMRES~\cite{sasc:86}  methods.  These
methods have  the advantage  that the  memory requirements  are small.
Also,  they tend  to be  easier  to parallelize  than direct  methods.
However, the  main problem with this  class of methods is  the rate of
convergence,  which depends  on the  properties of  the matrix.   Very
effective techniques have  been designed for classes  of problems such
as  the multigrid  methods~\cite{hack:85},  that are  well suited  for
specific problems  such as  those arising  from the  discretization of
elliptic PDEs.
%Originally  designed  to  effectively   exploit  both  the  geometric
%information of  the underlying  mesh and the  properties of  the PDE,
%they have been successfully extended  to other classes of PDE without
%exploiting geometrical informations: AMG  techniques.  The essence of
%the multigrid approaches is to  hierarchically reduce the size of the
%problem  via   coarsening  mechanisms   and  solving   recursively  a
%correction  equation  on  a  sequence  of  embedded  problems.   This
%efficient numerical mechanism based on coarsening techniques leads to
%a reduce  amount of  computation when it  goes sequentially  down the
%hierarchy  which  limits  the  possible parallelism  on  large  scale
%platforms.  Some attempts have been made to develop additive variants
%to express  reasonable amount of parallelism  by enabling independent
%treatment between the different levels  of the hierarchy of problems.
%However, these  schemes have encountered  a limited success on  a few
%academic problems and their  generalization to general linear systems
%remains an open question.  Another trend to preserve a good trade-off
%between numerical efficiency and large amount of parallelism consists
%in the  so-called aggressive  coarsening that  attempts to  limit the
%number  of  levels  in  the  hierarchy.   We  refer  to~\cite[chapter
%10]{siam:06} and the references therein for a recent survey.
One way to  improve the convergence rate of Krylov  subspace solver is
through preconditioning  and fixed  point iteration schemes  are often
used as  preconditioner.  In many computational  science areas, highly
accurate solutions  are not  required as  long as  the quality  of the
computed  solution  can  be  assessed  against  measurements  or  data
uncertainties.   In such  a framework,  the iterative  schemes play  a
central role  as they might be  stopped as soon as  an accurate enough
solution is found.   In our work, we consider  stopping criteria based
on the backward error analysis~\cite{templates,drsg:95,gree:97}.


Our approach  to high-performance,  scalable solution of  large sparse
linear systems in  parallel scientific computing is  to combine direct
and iterative methods.  Such a hybrid approach exploits the advantages
of both direct and iterative  methods.  The iterative component allows
us to  use a  small amount of  memory and provides  a natural  way for
parallelization.   The direct  part provides  its favorable  numerical
properties.   Furthermore,  this  combination enables  us  to  exploit
naturally  several  levels of  parallelism  that  logically match  the
hardware feature of emerging many-core  platforms as stated in Section
\ref{generalOverview}.    In   particular,   we   can   use   parallel
multi-threaded sparse direct solvers within the many-core nodes of the
machine and  message passing among  the nodes to implement  the gluing
parallel iterative scheme.

The general  underlying ideas  are not  new.  They  have been  used to
design domain  decomposition techniques for the  numerical solution of
PDEs~\cite{smbjgr:96,quva:99,mathew:08}.  Domain  decomposition refers
to the splitting of the  computational domain into sub-domains with or
without overlap.   The splitting strategies are  generally governed by
various  constraints/objectives  but  the   main  one  is  to  enhance
parallelism.  The  numerical properties of  the PDEs to be  solved are
usually extensively exploited at the  continuous or discrete levels to
design   the  numerical   algorithms.   Consequently,   the  resulting
specialized technique will  only work for the class  of linear systems
associated with  the targeted  PDEs.  In our  work, we  develop domain
decomposition  techniques  for  general unstructured  linear  systems.
More  precisely,   we  consider   numerical  techniques  based   on  a
non-overlapping decomposition of the  graph associated with the sparse
matrices.    The    vertex   separator,   constructed    using   graph
partitioning~\cite{kaku:95b,chpe:08}, defines  the interface variables
that will  be solved  iteratively using  a Schur  complement approach,
while the  variables associated with  the interior sub-graphs  will be
handled  by a  sparse direct  solver.  Although  the Schur  complement
system  is usually  more tractable  than  the original  problem by  an
iterative technique, preconditioning treatment is still required.  For
that  purpose, we  developed parallel  preconditioners and  designed a
hierarchical parallel implementation.  Linear  systems with a few tens
of millions unknowns have been solved  on a few thousand of processors
using our designed software.




\subsection{Parallel hierarchical implementation}
\label{sec:intro-hierarchical}
In  this  section,  methods   based  on  non-overlapping  regions  are
described.  Such domain decomposition algorithms are often referred to
as substructuring schemes. This  terminology comes from the structural
mechanics discipline where non-overlapping ideas were first developed.
The  structural analysis  finite  element community  has been  heavily
involved in the  design and development of these  techniques. Not only
is their definition  fairly natural in a finite  element framework but
their implementation can preserve data structures and concepts already
present in large engineering software packages.

Let us now further describe this technique  and let ${\cal A} x= b$ be
the linear problem.  For the sake of simplicity, we assume that ${\cal
  A}$  is symmetric  in pattern  and we  denote $G  = \{V,  E \}$  the
adjacency  graph associated  with  ${\cal A}$.   In  this graph,  each
vertex is associated with a row or column of the matrix ${\cal A}$ and
it  exists an  edge between  the  vertices $i$  and $j$  if the  entry
$a_{i,j}$ is non zero.

We assume that  the graph $G$ is partitioned  into $N$ non-overlapping
sub-graphs  $G_1$,  ...,  $G_N$   with  boundaries  $\Gamma_1$,  ....,
$\Gamma_N$.   The  governing  idea   behind  substructuring  or  Schur
complement  methods is  to split  the  unknowns in  two subsets.  This
induces the following block reordered linear system~:
\begin{equation}\label{eq:SchurMat}
\left(
\begin{array}{cc}
{\cal A}_{II} & {\cal A}_{I\Gamma} \\
{\cal A}_{\Gamma I} & {\cal A}_{\Gamma\Gamma} \\
\end{array}
\right)
\left( \begin{array}{c} x_{I} \\ x_{\Gamma} \\ \end{array} \right) =
\left( \begin{array}{c} b_{I} \\ b_{\Gamma} \\ \end{array} \right),
\end{equation}
where  $x_\Gamma$  contains  all unknowns  associated  with  sub-graph
interfaces and  $x_I$ contains the remaining  unknowns associated with
sub-graph interiors.   Because the interior points  are only connected
to either interior points in the  same sub-graph or with points on the
boundary of  the sub-graphs,  the matrix ${\cal  A}_{II}$ has  a block
diagonal  structure,  where each  diagonal  block  corresponds to  one
sub-graph.    Eliminating  $x_I$   from  the   second  block   row  of
Equation~\eqref{eq:SchurMat} leads to the reduced system
\begin{equation} \label{eq:Schursystem}
{\cal S} x_\Gamma = f,
\end{equation}
where
\begin{equation} \label{eq:SchurComplement}
{\cal S}={\cal A}_{\Gamma\Gamma}-{\cal A}_{\Gamma I} {\cal A}_{II}^{-1} {\cal A}_{I\Gamma}
\; \textrm{ and} \;
f = b_\Gamma -{\cal A}_{\Gamma I} {\cal A}_{II}^{-1} b_{I}.
\end{equation}
The matrix  ${\cal S}$  is referred  to as  the {\it  Schur complement
  matrix}. This reformulation leads to  a general strategy for solving
Equation \eqref{eq:SchurMat}.   Specifically, an iterative  method can
be  applied to  Equation \eqref{eq:Schursystem}.   Once $x_\Gamma$  is
known,  $x_I$  can  be  computed  with one  additional  solve  on  the
sub-graph interiors.
%Further, when  ${\cal A}$ is  symmetric positive definite  (SPD), the
%matrix ${\cal S}$ inherits this  property and so a conjugate gradient
%method can be employed.

Let  $\Gamma$ denote  the entire  interface defined  by $\Gamma=  \cup
\phantom{|}  \Gamma_i$; we  notice that  if two  sub-graphs $G_i$  and
$G_j$  share  an  interface   then  $\Gamma_i  \bigcap  \Gamma_j  \neq
\emptyset$.   As  interior  unknowns  are no  longer  considered,  new
restriction  operators  must  be   defined  as  follows.   Let  ${\cal
  R}_{\Gamma_i}  :  \Gamma  \rightarrow  \Gamma_i$  be  the  canonical
point-wise  restriction which  maps full  vectors defined  on $\Gamma$
into  vectors  defined on  $\Gamma_i$.   Thus,  in  the case  of  many
sub-graphs, the fully assembled global Schur ${\cal S}$ is obtained by
summing  the  contributions over  the  sub-graphs.   The global  Schur
complement matrix, Equation~\eqref{eq:SchurComplement}, can be written
as the sum of elementary matrices
\begin{equation}\label{eq:Schur_sum}
\displaystyle {\cal S} = \sum_{i=1}^N {\cal R}_{\Gamma_i}^T {\cal S}_i 
{\cal R}_{\Gamma_i} ,
\end{equation}
where
\begin{equation}\label{eq:SchurLoc}
{\cal S}_i = {\cal A}_{\Gamma_i \Gamma_i} - 
     {\cal A}_{\Gamma_i {\cal I}_i} 
     {\cal A}_{ {\cal I}_i {\cal I}_i}^{-1} 
     {\cal A}_{{\cal I}_i \Gamma_i}
\end{equation}
is a local Schur complement associated  with $G_i$ and is in general a
dense matrix  (eg : if  ${\cal A}_{{\cal I}{\cal I}}$  is irreducible,
${\cal S}_i$ is dense).
% associated with subdomain $\Omega_i$
It  can be  defined in  terms of  sub-matrices from  the local  matrix
${\cal A}_i$ defined by
\begin{equation}\label{eq:LocalNeuman}
{\cal A}_i =
\left(
\begin{array}{cc}
{\cal A}_{{\cal I}_i {\cal I}_i} & {\cal A}_{{\cal I}_i \Gamma_i} \\
A_{\Gamma_i {\cal I}_i} & A_{\Gamma_i \Gamma_i}
\end{array}
\right).
\end{equation}

%Notice that this form of the Schur complement has only one layer of
%interface unknowns between subdomains and allows for a straight-forward
%parallel implementation.

While the Schur complement  system is significantly better conditioned
than  the original  matrix ${\cal  A}$,  it is  important to  consider
further  preconditioning  when  employing   a  Krylov  method.  It  is
well-known, for  example, that  $\kappa(A) =  {\cal O}  (h^{-2})$ when
${\cal A}$  corresponds to  a standard discretization  (e.g. piecewise
linear finite elements) of the Laplace operator on a mesh with spacing
$h$  between the  grid  points. Using  two non-overlapping  sub-graphs
effectively  reduces  the condition  number  of  the Schur  complement
matrix to
%gives rise to a Schur complement matrix S with a 
%condition number of 
$\kappa(S) = {\cal O}  (h^{-1})$.  While improved, preconditioning can
significantly lower this condition number further.

We introduce the general form of the preconditioner considered in this
work.   The preconditioner  presented  below  was originally  proposed
in~\cite{cagm:01} in two dimensions  and successfully applied to large
three    dimensional    problems    and   real    life    applications
in~\cite{gihw:08, haid:08}.  To describe this preconditioner we define
the  local  assembled  Schur   complement,  $\bar{\cal  S}_i  =  {\cal
  R}_{\Gamma_i}  S {\cal  R}_{\Gamma_i}^T$,  that  corresponds to  the
restriction of the Schur complement to the interface $\Gamma_i$.  This
local  assembled preconditioner  can  be built  from  the local  Schur
complements ${\cal S}_i$ by assembling their diagonal blocks.

With these notations the preconditioner reads
\begin{equation}\label{eq:MAS}
M_d = \sum_{i=1}^N {\cal R}_{\Gamma_i}^T \bar{\cal S}_i^{-1} {\cal R}_{\Gamma_i}.
\end{equation}

If we considered a planar graph partitioned into horizontal strips (1D
decomposition),  the resulting  Schur  complement matrix  has a  block
tridiagonal structure as depicted in Equation~\eqref{eq:Schur1D}
\begin{equation}\label{eq:Schur1D}
{\cal S} = 
\left (
\begin{array}{llllll}
\ddots & &&&& \\
\cline{2-3}
& \multicolumn{1}{|c}{S_{k,k}} & \multicolumn{1}{c |}{S_{k,k+1}} &    & \\
\cline{3-4}
& \multicolumn{1}{|c}{S_{k+1,k}} & \multicolumn{1}{| c |}{S_{k+1,k+1}} & \multicolumn{1}{c |}{S_{k+1,k+2}} & \\
\cline{2-3}
&        &  \multicolumn{1}{|c}{S_{k+1,k+2}} & \multicolumn{1}{c |}{S_{k+2,k+2}} & \\
\cline{3-4}
& &&&& \ddots \\

\end{array}
\right ).
\end{equation}

For that  particular structure  of $\cal S$  the submatrices  in boxes
correspond  to  the  $\bar{\cal  S}_i$.  Such  diagonal  blocks,  that
overlap, are  similar to  the classical block  overlap of  the Schwarz
method when  written in a  matrix form for 1D  decomposition.  Similar
ideas  have been  developed in  a  pure algebraic  context in  earlier
papers~\cite{casa:96,  raro:87} for  the  solution  of general  sparse
linear systems.  Because  of this link, the  preconditioner defined by
Equation~\eqref{eq:MAS} is  referred to as algebraic  additive Schwarz
for the Schur complement.  One  advantage of using the assembled local
Schur complements instead of the  local Schur complements (like in the
Neumann-Neumann~\cite{bgtv:89,Roeck:1991:FIS}) is that in the SPD case
the assembled Schur  complements cannot be singular (as  ${\cal S}$ is
SPD~\cite{cagm:01}).

The  original  idea  of non-overlapping  domain  decomposition  method
consists  into   subdividing  the  graph  into   sub-graphs  that  are
individually mapped  to one  processor.  With this  data distribution,
each  processor  $P_i$  can  concurrently partially  factorize  it  to
compute its  local Schur complement  ${\cal S}_i$.  This is  the first
computational phase  that is performed concurrently  and independently
by  all   the  processors.   The   second  step  corresponds   to  the
construction of the preconditioner.   Each processor communicates with
its neighbors (in the graph  partitioning sense) to assemble its local
Schur  complement $\bar{\cal  S}_i$  and  performs its  factorization.
This step only requires a few point-to-point communications.  Finally,
the  last step  is the  iterative solution  of the  interface problem,
Equation~(   \ref{eq:Schursystem}).    For  that   purpose,   parallel
matrix-vector   product  involving   $\cal   S$,  the   preconditioner
$M_{\star}$ and  dot-product calculation  must be performed.   For the
matrix-vector  product   each  processor  $P_i$  performs   its  local
matrix-vector  product  involving  its   local  Schur  complement  and
communicates with  its neighbors  to assemble  the computed  vector to
comply     with     Equation~(\ref{eq:Schur_sum}).     Because     the
preconditioner,  Equation~(\ref{eq:MAS}), has  a similar  form as  the
Schur   complement,    Equation~(\ref{eq:Schur_sum}),   its   parallel
application to  a vector is  implemented similarly.  Finally,  the dot
products are performed  as follows: each processor  $P_i$ performs its
local  dot-product and  a global  reduction  is used  to assemble  the
result.  In this  way, the hybrid implementation can  be summarized by
the above main three phases.
%They are further detailed in the \bxp framework in Section~\ref{sec:2level_impl}.

\subsection{Dropping strategy to sparsify the preconditioner}
\label{dropstrat}
The  construction  of  the   proposed  local  preconditioners  can  be
computationally  expensive because  the  dense  matrices ${\cal  S}_i$
should  be  factorized.  We  intend  to  reduce  the storage  and  the
computational  cost to  form  and apply  the  preconditioner by  using
sparse  approximation  of  $\bar{\cal  S}_i$ in  $M_d$  following  the
strategy described by Equation~(\ref{eq:dropping}).  The approximation
$\hat{\cal  S}_i$  can be  constructed  by  dropping the  elements  of
$\bar{S}_{i}$  that   are  smaller  than  a   given  threshold.   More
precisely, the following symmetric dropping formula can be applied:
\begin{equation}\label{eq:dropping}
  \tilde{\overline{s}}_{\ell j} = \left \{ \begin{array}{l l}
0,& \mbox{\rm if } \; |\bar{s}_{\ell j}|\le \xi (|\bar{s}_{\ell \ell}| + |\bar{s}_{jj}|),
      \\ \bar{s}_{\ell j},& \mbox{\rm otherwise,}
   \end{array} \right . 
\end{equation}
where $\bar{s}_{\ell j}$ denotes the entries of $\bar{\cal S}_i$.
The resulting preconditioner based on these sparse approximations reads

\begin{equation}\label{eq:spsprecond}
 M_{sp} = \sum_{i=1}^N {\cal R}_{\Gamma_i}^T \left ( \tilde{\overline{ S}}_i \right )^{-1} {\cal R}_{\Gamma_i}.  
\end{equation}
We notice that such a dropping strategy preserves the symmetry in
the symmetric case but it requires to first assemble $\bar{S}_{i}$ before sparsifying it.
%From a computing view point, the assembling and the sparsification are cheap compared
%to the calculation of ${\cal S}_i$

\emph{Note : see \ref{itm:icntl21}=2 or 3 and \ref{itm:rcntl11} for more details on how to control this dropping strategy}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Sparse approximation based on partial $ILU(t,p)$}\label{sec:ilut}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\label{sparseILU}
One can design a computationally and memory cheaper alternative to approximate the
local Schur complements ${S}^{(i)}$.
Among the possibilities, we consider in this report a variant based on the $ILU(t,p)$~\cite{Saad-ILUT} that is
also implemented in pARMS~\cite{pARMS}.


The approach consists in applying a partial incomplete factorisation to the matrix $A^{(i)}$.
The incomplete factorisation is only run on $A_{ii}$ and it computes its 
ILU factors 
$\tilde{L}_i$ and $\tilde{U}_i$ using to the dropping parameter threshold $t_{factor}$.
$$
pILU~(A^{(i)}) \equiv
pILU~
\begin{pmatrix}
  A_{ii}        & A_{i \Gamma_i} \\
  A_{\Gamma_i i} & A^{(i)}_{\Gamma_i\Gamma_i}
  \end{pmatrix} 
\equiv
\begin{pmatrix}
  \tilde{L}_i                     & 0 \\
  A_{\Gamma i}\tilde{U}^{-1}_i    & I
  \end{pmatrix} 
\begin{pmatrix}
  \tilde{U}_i                    & \tilde{L}_i^{-1}A_{i \Gamma} \\
   0                            & \tilde{S}^{(i)}
  \end{pmatrix} 
$$
where
%\begin{equation}\label{eq:Schurilut}
$$
\tilde{S}^{(i)} = A_{\Gamma_i\Gamma_i}^{(i)} - A_{\Gamma_i i} \tilde{U}_i^{-1} \tilde{L}_i^{-1} A_{i \Gamma_i}
$$
%\end{equation}

The incomplete factors are then used to compute an approximation of the local Schur complement.
%that can be further sparsified to drop the smallest entries using a strategy similar 
%to the one defined by~\eqref{eq:dropping}.
Because our main objective is to get an approximation of the local Schur complement we switch to
another less stringent threshold parameter $t_{Schur}$ to compute the sparse approximation of the local Schur
complement.

Such a calculation can be performed using a IKJ-variant of the Gaussian elimination~\cite{saad:03}, where the
$\tilde{L}$ factor is computed but not stored as we are only interested in an approximation of $\tilde{S}_i$.
This further alleviate the memory cost.


Contrary to Equation~(\ref{eq:spsprecond}) (see Section \ref{dropstrat}), the assembly step can only be performed after the sparsification, which directly results from the ILU process. as a consequence, the assembly step is performed on sparse data structures thanks to a few neighbour to neighbour communications to form $\overline{\tilde{S}}^{(i)}$ from $\tilde{S}^{(i)}$. The preconditionner finally reads : 
\begin{equation}\label{eq:SparseApprox}
M_{sp} = \sum_{i=1}^N R_{\Gamma_i}^T \left (\overline{\tilde{S}}^{(i)} \right )^{-1}
{R}_{\Gamma_i}.  
\end{equation}


\emph{Note: To use this method, you should put \ref{itm:icntl30}=2, the control parameters related to the ILUT method are : \ref{itm:icntl8}, \ref{itm:icntl9}, \ref{itm:rcntl8} and \ref{itm:rcntl9}.} 

%****************************************************************************************
\subsection{Exact vs. approximated Schur algorithm to build the precond}\label{subsec:implementation}
%****************************************************************************************
In a full MPI implementation, the domain decomposition
strategy is followed to assign each local PDE problem (sub-domain) to one
MPi process that works independently of other processes and exchange data
using message passing. 
In that computational framework, the implementation of the algorithms based on preconditioners built from
the exact or approximated local Schur complement only differ
in the preliminary phases.
The parallel SPMD algorithm is as follow:
\begin{enumerate}
 \item Initialization phase:
   \begin{itemize}
      \item \underline{Exact Schur} : using the sparse direct solver, we compute at once the $LU$ factorization
        of $A_{ii}$ and the local Schur complement $S^{(i)}$ using $A^{(i)}$ as input matrix;
      \item \underline{Approximated Schur} : using the sparse direct solver we only compute the $LU$ factorization 
        of $A_{ii}$, then we compute the approximation of the local Schur complement $\tilde{S}^{(i)}$ by performing
        a partial $ILU$ factorization of $A^{(i)}$.
   \end{itemize}
 \item Set-up of the preconditioner:
   \begin{itemize}
      \item \underline{Exact Schur} : we first assemble the diagonal problem thanks to few neighbour to neighbour communications (computation of
         $\bar{S}^{(i)}$), we sparsify the assembled local Schur (i.e., $\hat{S}^{(i)}$) that is then factorized.
      \item \underline{Approximated Schur} : we assemble the sparse approximation also thanks to few neighbour to neighbour communications
         and we factorize the resulting sparse approximation of the assembled local Schur.
   \end{itemize}
 \item Krylov subspace iteration: the same numerical kernels are used. The only difference is the sparse factors that are considered
      in the preconditioning step dependent on the selected strategy (exact v.s. approximated).
\end{enumerate}

From a high performance computing point of view, the main difference relies in the computation of the local Schur complement.
In the exact situation, this calculation is performed using sparse direct techniques which make intensive use of BLAS-3 technology
as most of the data structure and computation schedule are performed in a symbolic analysis phase when fill-in is analyzed.
For partial incomplete factorization, because fill-in entries might be dropped depending on their numerical values, no prescription of
the structure of the pattern of the factors can be symbolically computed. 
Consequently this calculation is mainly based on sparse BLAS-1
operations that are much less favorable to an efficient use of the memory hierarchy  and therefore less effective in terms of their 
floating point operation rate.
In short,  the second case leads to fewer operations but at a 
lower computing rate, which might result 
in higher overall elapsed time in some situations.
Nevertheless, in all cases the approximated Schur approach consumes much 
less memory as illustrated later on in this report.

\emph{Note : To choose between each strategy you should use \ref{itm:icntl30}.}

%****************************************************************************************
\subsection{Multiple right hand sides in \maphys}\label{subsec:multirhs}
%****************************************************************************************

Since version 0.9.5, it is possible to set multiple right hand sides. \maphys
will use the \fabulous library, which implements different block GMRes
algorithms.
%TODO: cite?

\iftoggle{devversion}{
\textcolor{red}{
\section{Various possible scenarii to use Maphys}
\begin{enumerate}
  \item Preconditioner variants
    \begin{enumerate}
       \item Dense preconditioner
       \item Sparsified preconditioner
       \item Sparse approximation based on partial $ILU(t,p)$
    \end{enumerate}
  \item Matrix-vector calculation
    \begin{enumerate}
       \item Explicit
       \item Implicit
    \end{enumerate}
\end{enumerate}
}
}{}
%\cleardoublepage

\bibliographystyle{plain}

\bibliography{users_guide}


