%
%documentstyle and options : ARTICLE STYLE
%%\documentclass[a4paper,12pt]{article}
\documentclass[a4paper]{article}
%
\usepackage{amsmath,amsfonts}
\usepackage{tabularx}
\usepackage{multicol,lscape}
\usepackage{algorithmic}
\usepackage{algorithm}
%
%
% definitions
%
\setlength{\oddsidemargin}{0.5cm}
\setlength{\evensidemargin}{-0.10in}
\setlength{\topmargin}{-0.10in}
\setlength{\headsep}{1.5cm}
\setlength{\textwidth}{15.cm}
\setlength{\textheight}{21.cm}
\parindent=7mm
%
\usepackage{float}
\floatstyle{boxed}
\newfloat{algorithm}{thp}{loa}
\floatname{algorithm}{Algorithm}
%

%
\def\rit{\hbox{\it I\hskip -2pt R}}
\def\nit{\hbox{\it I\hskip -2pt N}}
\def\cit{\hbox{\it l\hskip -5.5pt C\/}}
\def\lcit{\hbox{\it l\hskip -10.5pt C\/}}
\def\fiteps{\hbox{\it l\hskip -2pt F\/}_\epsilon}
\def\fit{\hbox{\it l\hskip -2pt F\/}}
\def\lsim{\hbox{\hspace{-.7cm} $\vcenter{\hsize=1.0cm ${\stackrel{<}\sim}$}$ }}
\def\gsim{\hbox{\hspace{-.7cm} $\vcenter{\hsize=1.0cm ${\stackrel{>}\sim}$}$ }}
%
\newcommand{\ds}[2]{\displaystyle{\frac{#1}{#2}}}
\newcommand{\nb}[2]{ #1 \enskip 10^{#2} }
\newcommand{\md}[1]{   \left\vert {#1} \right\vert}
\newcommand{\norm}[1]{ \left\Vert {#1} \right\Vert}
\newcommand{\ndual}[1]{ \left\Vert {#1} \right\Vert_*}
\newcommand{\nav}[1]{  \left\Vert {#1} \right\Vert_{\rm av} }
\newcommand{\ninf}[1]{ \left\Vert {#1} \right\Vert_\infty}
\newcommand{\neuc}[1]{ \left\Vert {#1} \right\Vert_2}
\newcommand{\nfro}[1]{ \left\Vert {#1} \right\Vert_F}
\newcommand{\nun}[1]{  \left\Vert {#1} \right\Vert_1}
\newcommand{\normA}[1]{  \left\Vert {#1} \right\Vert_A}
\newcommand{\bx}{\overline{x}}
\newcommand{\br}{\overline{r}}
\newcommand{\bz}{\overline{z}}
\newcommand{\bX}{\overline{X}}
\newcommand{\tx}{\tilde{x}}
\newcommand{\tl}{\tilde{\lambda}}
\newcommand{\tX}{\widetilde{X}}
\newcommand{\ty}{\tilde{y}}
\newcommand{\tr}{\tilde{r}}
\newcommand{\td}{\tilde{d}}
\newcommand{\tz}{\tilde{z}}
\newcommand{\tg}{\widetilde{G}}
\newcommand{\da}{\Delta A}
\newcommand{\db}{\Delta b}
\newcommand{\dr}{\Delta r}
\newcommand{\dx}{\Delta x}
\newcommand{\dy}{\Delta y}
\newcommand{\dX}{\Delta X}
\newcommand{\dd}{\Delta d}
\newcommand{\dz}{\Delta z}
\newcommand{\dl}{\Delta \lambda}
\newcommand{\gam}[1]{\gamma_{#1}}
\newcommand{\IA}{ A^{-1}}
\newcommand{\la}{ \lambda }
\newcommand{\lap}{\lambda'}
\newcommand{\al}{\alpha}
\newcommand{\ep}{\epsilon }
\newcommand{\xg}{x_G}
\newcommand{\yg}{y_G}
\newcommand{\xa}{x_\alpha}
\newcommand{\xt}{x_\theta}
\newcommand{\ga}{G_\alpha}
\newcommand{\gt}{G_\theta}
\newcommand{\ya}{y_\alpha}
\newcommand{\yt}{y_\theta}
\newcommand{\txg}{\tilde{x}_G}
\newcommand{\tyg}{\tilde{y}_G}
\newcommand{\dxa}{\Delta x_\alpha }
\newcommand{\dxt}{\Delta x_\theta }
\newcommand{\dya}{\Delta y_\alpha }
\newcommand{\dyt}{\Delta y_\theta }
\newcommand{\dzt}{\Delta \zeta_\theta }
\newcommand{\dza}{\Delta z_\alpha }
\newcommand{\dwt}{\Delta w_\theta }
\newcommand{\bigo}{{\rm O }}
%
\newcommand{\am}[1]{\left(\matrix{0&{#1}\cr
                           {#1}^{*}&0\cr}\right)}
\newcommand{\vecto}[1]{\stackrel{\makebox{\rightarrowfill}}{{#1}}}
%
\begin{document}
\bibliographystyle{plain}
%
\title{A Set of Conjugate Gradient Routines \\ 
for Real and Complex Arithmetics}
\author{Val\'erie Frayss\'e \and Luc Giraud 
}
\date{May 2017 }
\maketitle
%
\maketitle
\begin{abstract}
In this report we describe the implementations of the preconditioned
conjugate gradient (CG) algorithm for both real and complex, 
single and double precision arithmetics
suitable for serial, shared memory and distributed memory computers.
For the sake of simplicity, flexibility and efficiency the CG solvers
have been implemented using the reverse communication mechanism
for the matrix-vector product, the preconditioning and the dot product
computations. 
Finally the implemented stopping criterion is based on a normwise
backward error.
After a short presentation of the CG algorithm
we give a detailed description of the user interface.
 \\
 
\noindent
{\bf Keywords}~: linear systems, Krylov methods, CG, reverse
communication, distributed memory.
\end{abstract}
\makeatletter
\renewcommand{\thefootnote}{\fnsymbol{footnote}}
%
\makeatother
\renewcommand{\thefootnote}{\arabic{footnote}}
%
\section {The Conjugate Gradient algorithm}
%
\subsection{General description}
%
 The Conjugate Gradient method was proposed in different versions
in the early 50s in separate contributions by Lanczos~\cite{lanc:52} and
Hestenes and Stiefel~\cite{hest:52}.
It becomes the method of choice for the solution of large sparse  hermitian
positive definite linear system and is the starting point of the extensive
work on the Krylov methods~\cite{savo:99}.


Let $A = A^H$ (where $A^H$ denotes the conjugate transpose of $A$)
be a square nonsingular $n \times n$ complex hermitian positive
definite matrix, and $b$ be
a complex vector of length $n$, defining the linear system
\begin{equation}
A x = b
\label{eq:linsys}
\end{equation}
to be solved.

Let $x^{(0)} \in \Bbb{C}^n$ be an initial guess for this linear system,
$r^{(0)} = b - Ax^{(0)}$ be its corresponding residual and $M^{-1}$ be the 
preconditioner.
 The algorithm is classically described by
\newpage
\begin{algorithm}[!ht]
\caption{\label{alg:CG} Conjugate Gradient}
\begin{algorithmic}
\STATE $k=0$
\STATE $r^{(0)} = b - Ax^{(0)}$
\REPEAT
\STATE $k=k+1$
\STATE Solve $M z^{(k)} = r^{(k)} $
\IF{$k=1$}
  \STATE $p^{(1)} = z^{(0)}$
\ELSE
  \STATE $\beta^{(k-1)} = z^{(k-1)^H}r^{(k-1)}/z^{(k-2)^H}r{(k-2)}$
  \STATE $p^{(k)} = z^{(k-1)}+\beta^{(k-1)}p^{(k-1)}$
\ENDIF
\STATE $q^{(k)} = A p^{(k)}$
\STATE $\alpha^{(k)} = z^{(k-1)^H}r^{(k-1)}/p^{(k)^H}q^{(k)}$
\STATE $x^{(k)} = x^{(k-1)}+\alpha^{(k)}p^{(k)}$
\STATE $r^{(k)} = r^{(k-1)}-\alpha^{(k)}q^{(k)}$
\UNTIL convergence 
\end{algorithmic}
\end{algorithm}

The conjugate gradient algorithm constructs the minimum error solution
in A-norm over the Krylov space 
${\cal K}_k = {\rm span}\left\{ r^{(0)}, Ar^{(0)}, \dots, A^{k-1}r^{(0)} \right\}$.
It exists a rich literature
dedicated to this method: for more details we, non-exhaustively,
refer to~\cite{axel:94, GOL.LOA.96, SAA.96} and the references
therein.

%
\subsection{Relationship with the symmetric Lanczos matrix}\label{sec:lanczos}
 There is a close relationship between the conjugate gradient method
and the Lanczos algorithm for the solution of eigenproblem~\cite{GOL.LOA.96}.
 The Lanczos algorithm applied to a matrix $A$ constructs a tridiagonal 
matrix whose extremal eigenvalues converge to the extremal eigenvalues of
$A$ usually denoted $\lambda_{\min}(A)$ and $\lambda_{\max}(A)$.
 Thanks to the relationship between Lanczos and Conjugate Gradient, the
entries of the tridiagonal matrix can be recovered from the computed information
produced by CG at very low extra cost.
More precisely the
entries of the symmetric tridiagonal Lanczos
matrix $T$ can be recovered from the conjugate gradient iterations
by:
$$ T_{k,k} = \displaystyle \frac{1}{\alpha^{(k)}} +
  \frac{\beta^{(k-1)}}{\alpha^{(k-1)}}$$
 and
$$ T_{k-1,k} = \displaystyle -\frac{\sqrt{\beta^{(k-1)}}}{\alpha^{(k-1)}}.$$

 When computed from preconditioned iterations, the extremal eigenvalues 
of the tridiagonal Lanczos matrix converge to the extremal eigenvalues of the
preconditioned matrix $M^{-1}A$.
 Therefore one can get an estimation of the condition number of the
preconditioned linear system defined by 
$\displaystyle \frac{\lambda_{min}(M^{-1} A)}{\lambda_{Max}(M^{-1} A)}$ 
by computing the smallest and largest eigenvalues of the tridiagonal matrix.

\subsection{Stopping criteria}\label{sec:stop}
%
\subsubsection{Backward-error based stopping criterion}
For linear systems with SPD matrices, stopping criteria of different nature can be
considered.
 The first one is fully algebraic and relies on the normwise backward
error~\cite{CHA.FRA.96}.
The second is particularly suited for the solution of linear systems arising
from the discretization of elliptic PDE and relies on a lower (often tight) bound of the
A-norm of the error.
This error can be related to the approximation order that for order-2 scheme is ${\cal O}(h^2)$.

The backward error analysis, introduced by Givens and Wilkinson~\cite{WIL.63},
 is a powerful concept for analyzing the quality of an
approximate solution:
\begin{enumerate}
  \item it is independent from the details of round-off propagation:
        the error introduced during the computation are interpreted
        in terms of perturbations of the initial data, and the computed 
        solution is considered as exact for the perturbed problem;
  \item because round-off errors are seen as data perturbations,
        they can be compared with errors due to numerical approximations
        (consistency of numerical schemes) or to physical measurements
        (uncertainties on data coming from experiments for instance).
\end{enumerate}
The backward error measures in fact the distance between the data of the
initial problem and those of the perturbed problem; therefore it relies 
upon the choice of the data allowed to vary and the norm to measure these 
variations.
In the context of linear systems, classical choices are the normwise
and the component-wise perturbations~\cite{CHA.FRA.96}.
These choices lead to explicit formulas for the backward error
(often a normalized residual) which is then easily evaluated.
For iterative methods, it is generally admitted that the normwise model
of perturbation is appropriate~\cite{AR.DU.RU.92}.\\

Let $x^{(m)}$ be an approximation of the solution $x =A^{-1}b$.
Then the quantity
\begin{eqnarray*}
\eta(x^{(m)}) &=& \min \left\{ \varepsilon > 0;\ \neuc{\Delta A} \leq \varepsilon
\alpha,\ \neuc{\Delta b} \leq \varepsilon \beta \ {\rm and}\ 
(A + \Delta A)x^{(m)} = b + \Delta b \right\} \\
&=& \ds{\neuc{b - Ax^{(m)}}}{\alpha \neuc{x^{(m)}} + \beta}
\end{eqnarray*}
is called the {\em normwise backward error} associated with $x^{(m)}$.
The best one can require from an algorithm is a backward error of the order
of the machine precision.
In practice, the approximation of the solution is acceptable when its
backward error is lower than the uncertainty on the data.
Therefore, there is no gain in iterating after the backward error has
reached machine precision (or data accuracy).
The unpreconditioned residual is iteratively computed by the algorithm
via  a short recurrence.
Therefore, the backward error can be obtained at a low cost and we can use
$$
\eta_{CG} = \ds{\md{r^{(m)}}}{\alpha \neuc{x^{(m)}} + \beta}
$$
as the stopping criterion of the CG iterations.
However, it is well-known that, in finite precision arithmetic, the
computed residual given by the short recurrence may differ
significantly from the true residual.
Therefore, it is not safe to use exclusively $\eta_{CG}$ as the stopping
criterion.
Our strategy is the following: first we iterate until $\eta_{CG}$ becomes
lower than the tolerance, then afterwards, we iterate until $\eta$ becomes
itself lower than the tolerance.
We hope in this way to minimize the number of residual computations
(involving the computation of matrix-vector product) necessary 
to evaluate $\eta$, while having a reliable stopping criterion.\\

How to choose $\alpha$ and $\beta$~?
Classical choices for $\alpha$ and $\beta$ that appear in the literature
are $\alpha = \neuc{A}$ and $\beta = \neuc{b}$.
Any other choice that reflects the possible uncertainty on the data can 
also be plugged in.
In our implementation, default values are used when the user's input
is $\alpha = \beta = 0$.
Table~\ref{tab:stop2} lists the stopping criteria for different choices
of $\alpha$ and $\beta$.
%
\begin{table}
\begin{center}
\begin{tabular}{|c|c|c|}
\hline
$\alpha$ & $\beta$ & Information on the unpreconditioned system \\
\hline
& & \\
$0$ & $0$ & $\ds{\neuc{A x^{(m)} - b}}{\neuc{b}}$ \\
& & \\
$0$ & $\neq 0$ & $\ds{\neuc{A x^{(m)} - b}}{\beta}$ \\
& & \\
$\neq 0$ & $0$ & $\ds{\neuc{A x^{(m)} - b}}{\alpha\neuc{x^{(m)}}}$ \\
& & \\
$\neq 0$ & $\neq 0$ & $\ds{\neuc{A x^{(m)} - b}}{\alpha\neuc{x^{(m)}} + \beta}$ \\
& & \\
\hline
\end{tabular}
\end{center}
\caption{Stopping criterion for the CG method}
\label{tab:stop2}
\end{table}

\subsubsection{$A$-norm error based stopping criterion}
The lower bound of the A-norm of the error, that is the quantity minimizes by the conjugate gradient on
the Krylov space can be estimated using by-products of the algorihtm, consequently at almost no
extra cost.
We only give a brief description on this estimate and refer the reader to~\cite{ario:04, stti:05} for 
detail discussion.
For a given $d$, an iterate of the CG algorihtm $x^{(k)}$ satisfy
$$
\Vert x - x^{(k)} \Vert_A^2 = 
     \sum_{i=k}^{k+d-1} \alpha^{(i)}(z^{(i-1)^H}r^{(i-1)})  + \Vert x - x^{(k+d)} \Vert_A^2 .
$$

Assuming a reasonable decrease of the $A$-norm of the error in the step $k+1$ through $k+d$, the square root
of
$$
\mu_{k,d} = \sum_{i=k}^{k+d-1} \alpha^{(i)}(z^{(i-1)^H}r^{(i-1)}) 
$$
gives a tight lower bound for $\Vert x - x^{(k)} \Vert_A$ using by-products of the PCG algorithm.

For defining a relative $A$-norm of the error 
$$
\frac{\Vert x - x^{(k)} \Vert_A}{\Vert x \Vert_A},
$$
that can be related to the discretization error~\cite{ario:04}, one needs to work on
for ${\Vert x \Vert_A}$.
Following~\cite{stti:05}, one has
$$
  \Vert x \Vert_A^2  =  \left ( \mu_{n,k+d} + \left (b+ r^{(0)} \right )^Tx^{(0)} \right ) +
\Vert x - x^{(k+d)} \Vert_A^2.
$$

Consequently
$$
 0 < \frac{\Vert x - x^{(k)} \Vert_A^2  - \Vert x - x^{(k+d)} \Vert_A^2}{\Vert x \Vert_A^2 - \Vert x - x^{(k+d)} \Vert_A^2}
  \leq \frac{\Vert x - x^{(k)} \Vert_A}{\Vert x \Vert_A}
$$
that requires $ 0 \leq \left (b+ r^{(0)} \right )^Tx^{(0)} $ to make sure the above bound is valid for all $k$.
However, as discussed in~\cite{stti:05}, $ \left (b+ r^{(0)} \right )^Tx^{(0)}$
 corresponds to a poor choice of the initial guess
that complies with the following relation
$$
 \Vert x - x^{(0)} \Vert_A^2 \le \Vert  x \Vert_A^2.
$$ 
This latter inequality indicates that it is unfortunate for an algorithm that minimizes the $A$-norm of the error
to start from an initial guess that has a larger $A$-norm error than the null vector.
In our implementation, if the user supplies an initial guess that has this property, we reset it to zero.

To summarize, when chosen by the user, the following lower bound on the relative $A$-norm of the error is used
$$
    \sqrt{\frac{\mu_{k,d}}{\mu_{1,k+d} + \left (b+ r^{(0)} \right )^Tx^{(0)}}}, 
$$
notice that $d-1$ extra iterations have to be performed to actually detect that the convergence had occured
at iteration $k$.



%
\newcommand{\verba}[1]{\texttt{#1}}
\newcommand{\Paramlabel}[1]{\mbox{\texttt{#1}}\hfill}
\newenvironment{listparam}
 {\begin{list}{}
     {\renewcommand{\makelabel}{\Paramlabel}
        \setlength{\labelwidth}{2.0cm}
        \setlength{\itemsep}{-0.1cm}
        \setlength{\leftmargin}{2.1cm}
     }
  }
 {\end{list}}
%
\section{Implementation of CG}\label{sec:interf}

%
\subsection{The user interface}
%
  For the sake of simplicity and portability, the CG implementation
is based on the reverse communication mechanism 
\begin{itemize}
\item for implementing the numerical kernels that depend on the data 
structure of the matrix $A$ and the preconditioners,
\item for performing the dot products.
\end{itemize}
This last point has been implemented to allow the use of CG
in a parallel distributed memory environment, where only the user
knows how he has spread his data.
We have one driver per arithmetic, and we use the BLAS and LAPACK
terminology that is 
\begin{center}
\begin{tabularx}{\linewidth}{l X}
 \texttt{DRIVE\_SCG} & for real single precision arithmetic computation, \\
 \texttt{DRIVE\_DCG} & for real double precision arithmetic computation, \\
 \texttt{DRIVE\_CCG} & for complex single precision arithmetic computation,\\
 \texttt{DRIVE\_ZCG} & for complex double precision arithmetic computation. \\
\end{tabularx}
\end{center}
Finally, to hide as much as possible the numerical method from the user,
only a few parameters are required by the drivers, whose interfaces are
similar for all arithmetics. 
Below we present the interface for the real double precision driver:
\begin{verbatim}
       CALL DRIVE_DCG(N,NLOC,LWORK,WORK,LWORKR,WORKR,IRC,ICNTL,CNTL,INFO,RINFO)
\end{verbatim}
%
\begin{listparam}
 \item[N]       is an {INTEGER} variable that must be set by the user to
                the order $n$ of the matrix $A$. 
                It is not altered by the subroutine.
 \item[NLOC]    is an {INTEGER} variable that must be set by the user to
                the size of the subset of entries of $b$ and $x$ that are
                allocated to the calling process in a distributed memory
                environment.
                For serial or shared memory computers \texttt{NLOC} should be
                equal to \texttt{N}.
                It is not altered by the subroutine.
 \item[LWORK]   is an INTEGER variable that must be set by the user to the
                size of the workspace \verba{WORK}. 
                \verba{LWORK} must be greater than or equal to 
                \verba{6*NLOC+1} .
                It is not altered by the subroutine.
 \item[WORK]    is a SINGLE/DOUBLE PRECISION REAL/COMPLEX array of length
                \verba{LWORK}.
                The first \verba{NLOC} entries contain the initial guess
                $x_0$ in input and the computed approximation of the 
                unpreconditioned solution in output. 
                The nexnext \verba{NLOC} entries 
                contain the right-hand side $b$ of the unpreconditioned
                system. The remaining entries are
                used as workspace by the subroutine.
 \item[LWORKR]   is an INTEGER variable that must be set by the user to the
                size of the workspace \verba{WORKR}. 
                \verba{LWORKR} must be greater than or equal to 
                \verba{ICNTL(9)+1}.
                If  \verba{ICNTL(7) == 1}, \verba{2*(ICNTL(6)+1)} entries should be
                added to this workspace.
                It is not altered by the subroutine.
 \item[WORKR]   is a SINGLE/DOUBLE PRECISION REAL array of length
                \verba{LWORKR}. It is used to store 
                a vector of length $d+1$ (delay to estimate the relative $A$-norm
                of the error (d=\verba{ICNTL(9)} see Section~\ref{sec:stop} for a dscription
                of the $A$-norm of the error calcualtion) as well as the Lanczos matrix,
                when the estimation of the condition number
                of the preconditioned matrix is required (\verba{ICNTL(7) = 1}).
                When \verba{ICNTL(7) = 0}, it is not accessed.
  \item[IRC]    is an INTEGER array of length 5 that need not be set by the
                user.
                This array controls the reverse communication. Details of
                the reverse communication management are given in 
                Section~\ref{sec:reverse}.
  \item[ICNTL]  is an INTEGER array of length 9 that contains control
                parameters that must be set by the user. Details of the 
                control parameters are given in Section~\ref{sec:controlPar}.
  \item[CNTL]   is a SINGLE/DOUBLE PRECISION REAL array of length 3 that 
                contains control parameters that must be set by the user. 
                Details of the control parameters are given in 
                Section~\ref{sec:controlPar}.
  \item[INFO]   is an INTEGER array of length 3 which contains information on
                the reasons of exiting CG. Details are given in
                Section~\ref{sec:infoPar}.
  \item[RINFO]  is a SINGLE/DOUBLE PRECISION REAL array of length 3 that
                contains the backward error for the linear system and
                the estimation of the extremal eigenvalues if requested.
\end{listparam}
%
\subsection{The reverse communication management}\label{sec:reverse}
%
The INTEGER array \verba{IRC} permits to implement the reverse
communication.
None of its entries need be set by the user. \\
On each exit, \verba{IRC(1)} indicates the action that must be performed
by the user before invoking the driver again.
Possible values of \verba{IRC(1)} and the associated actions are as follows:
\begin{listparam}
  \item[~0] Normal exit. 
  \item[~1] The user must perform the matrix vector product 
            $z \leftarrow A x$.
  \item[~2] The user must perform the right preconditioning
            $z \leftarrow M_i^{-1} x$.
  \item[~3] The user must perform the product of x$^H$ and  y
            $z \leftarrow x^H y$.
\end{listparam}
On each exit with \verba{IRC(1) >}~0, \verba{IRC(2)} and \verba{IRC(3)}
indicate the index in
\verba{WORK} where $x$ and $y$ should be read and  \verba{IRC(4)} indicates the
index in \verba{WORK} where $z$ should be written.\\


We refer to Section~\ref{sec:example} for an example of use of the driver
routine.
%
\subsection{The control parameters}\label{sec:controlPar}
%
The entries of the array \verba{ICNTL} control the execution of the 
\verba{DRIVE\_CG} subroutine.
All entries of \verba{ICNTL} are input parameters. \\
%
\begin{listparam}
 \item[ICNTL(1)] is the stream number for the error messages.
  {\bf (Default is 6)}.
 \item[ICNTL(2)] is the stream number for the warning messages. No
	warning message if set to 0.
  {\bf (Default is 6)}.
 \item[ICNTL(3)] is the stream number for the convergence history.
	No convergence history if set to 0.
  {\bf (Default is 0)}.
 \item[ICNTL(4)] indicates if a preconditioner will be used
  {\bf (Default is 0)}.
 \item[ICNTL(5)] controls whether the user wishes to supply an initial
                 guess of the solution vector. 
                 If ICNTL(5)=0, the initial guess is set to zero.
  {\bf (Default is 0)}.
 \item[ICNTL(6)] is the maximum number of iterations allowed.
  {\bf (Default is -1)}.
 \item[ICNTL(7)] indicates if eigenvalues estimation is requested.
  {\bf (Default is 0)}.
 \item[ICNTL(8)] controls the stopping criterion to be used.
  {\bf (Default is 1)}.
 \item[ICNTL(9)] define the delay to be used to evaluate the scaled $A$-norm of the error.
  {\bf (Default is 5)}.
\end{listparam}

Possible values for \verba{ICNTL(4)} are 
\begin{listparam}
  \item[~0] no preconditioning,
  \item[~1] preconditioning,
  \item[~2] error, default set in the routine \verba{INIT\_CG}.
\end{listparam}
 
Possible values for \verba{ICNTL(7)} are
\begin{listparam}
  \item[~0] no eigenvalue estimation requested, default set in the routine \verba{INIT\_CG}.
  \item[~1] estimation of the extremal eigenvalues of the preconditioner system
	is requested.
\end{listparam}

Possible values for \verba{ICNTL(8)} are
\begin{listparam}
      \item[1] regular normwise backward error,
      \item[2] scaled $A$-norm of the error.
\end{listparam}

The entries of the \verba{CNTL} array define the tolerance and the
normalizing factors (see Section~\ref{sec:stop}) that control the 
execution of the algorithm:
\begin{listparam}
 \item[CNTL(1)]  is the convergence tolerance for the backward error
            (see Section~\ref{sec:stop} for details).
 \item[CNTL(2)]  is the normalizing factor $\alpha$.
 \item[CNTL(3)]  is the normalizing factor $\beta$.
\end{listparam}
%
\subsection{The information  parameters}\label{sec:infoPar}
%
Once \verba{IRC(1) = }~0, the entries of the array \verba{INFO} explain 
the circumstances under which CG was exited.
All entries of \verba{INFO} are output parameters. \\

Possible values for \verba{INFO(1)} are 
\begin{listparam}
  \item[~0] normal exit. Convergence has been observed.
  \item[-1] erroneous value $n < 1$.
  \item[-2] \verba{LWORK} or \verba{LWORKD} too small.
  \item[-3] convergence not achieved after \verba{ICNTL(6)} iterations.
  \item[-4] illegal preconditioning information provided.
\end{listparam}  

If \verba{INFO(1) = 0}, then \verba{INFO(2)} contains the number of
iterations performed until achievement of the convergence and
\verba{INFO(3)} gives the minimal size for workspace.
If \verba{INFO(1) = -3}, then \verba{INFO(2)} contains the minimal size
necessary for the workspace. \\

If \verba{INFO(1) = 0}, then \verba{RINFO(1)} contains the backward error
for the linear system. 
 Additionnally, if the estimation has been erquested
(i.e., \verba{INFO(7) = 1})  \verba{RINFO(2)}
contains an estimation of the smallest eigenvalue of the preconditioned
system and \verba{RINFO(3)} an estimation of the largest eigenvalue.
Thus an estimation of the condition number of the preconditioned
system can be computed.

%
\subsection{Initialization of the parameters}
%
An initialization routine is available to the user for each arithmetic:
\begin{center}
\begin{tabularx}{\linewidth}{l X}
 \texttt{INIT\_SCG} & for real single precision arithmetic computation, \\
 \texttt{INIT\_DCG} & for double precision arithmetic computation, \\
 \texttt{INIT\_CCG} & for complex single precision arithmetic
                        computation,\\
 \texttt{INIT\_ZCG} & for complex double precision arithmetic computation.
\\
\end{tabularx}
\end{center}
These routines set the input control parameters \verba{ICNTL} and
\verba{CNTL} defined above to default values.
The generic interface is
\begin{verbatim}
       CALL INIT_CG(ICNTL,CNTL)
\end{verbatim}
The default value for
\begin{listparam}
 \item[ICNTL(1)]  is 6,
 \item[ICNTL(2)]  is 6,
 \item[ICNTL(3)]  is 0: no convergence history,
 \item[ICNTL(4)]  is 2: undefined preconditioner,
 \item[ICNTL(5)]  is 0: default initial guess $x_0 = 0$,
 \item[ICNTL(6)]  is -1: the user must specify explicitly
                  the maximum number of iterations,
 \item[ICNTL(7)]  is 0: no estimation of the extremal eigenvalues,
 \item[ICNTL(8)]  is 1: normwise backward error based stopping criterion,
 \item[ICNTL(9)]  is 5: delay to estimate the scaled $A$-norm of the error.
 \item[CNTL(1)]   is $10^{-6}$,
 \item[CNTL(2)]   is $0$,
 \item[CNTL(3)]   is $0$.
\end{listparam}  
%
%

\section{An example of use}\label{sec:example}
%

\begin{landscape}
\begin{small}
\setlength{\columnseprule}{0.5pt}
\begin{multicols}{2}
\begin{verbatim}

**************************************************************
**      TEST PROGRAMME FOR THE CG CODE
**************************************************************

      program validation
*
      integer lda,  lwork
      parameter (lda = 1000)
      parameter (lwork = 6*lda, lworkR=2*lda+2)
*
      integer i, j, n
      integer revcom, colx, coly, colz
      integer irc(7), icntl(9), info(3)
*
      integer matvec, precondLeft,  dotProd
      parameter (matvec=1, precondLeft=2, dotProd=3)
*
      integer nout
*
      double precision  a(lda,lda), work(lwork), workR(lworkR)
      double precision  cntl(5), rinfo(3)
*
      double precision ZERO, ONE, aux
      parameter (ZERO = 0.0d0, ONE = 1.0d0)
*
      double precision ddot
      external ddot
*
*************************************************************
** Generate the test matrix a and set the right-hand side
** in positions (n+1) to 2n of the array work.
** The right-hand side is chosen such that the exact solution
** is the vector of all ones.
*************************************************************
*
      write(*,*) '*******************************************'
      write(*,*) 'This code is an example of use of CG'
      write(*,*) 'in single precision double precision arithmetic'
      write(*,*) 'Results are written in output files'
      write(*,*) 'fort.20 : log file of CG iterations '
      write(*,*) 'and sol_Testcg : output of the computation.'
      write(*,*) '*******************************************'
      write(*,*)
      write(*,*) 'Matrix size < ', lda
      read(*,*) n
      if (n.gt.lda) then
        write(*,*) 'You are asking for a too large matrix'
        goto 100
      endif
*
      do j = 1,n
        do i = 1,n
          a(i,j) = ZERO
        enddo
        aux = mod(j,7)
c       work(j) = sqrt(aux)
        work(j) = (aux)
      enddo
*
      do i = 1,n
        a(i,i) = 5.0d0
      enddo
      do i = 1,n-1
        a(i,i+1) = -2.0d0
        a(i+1,i) = -2.0d0
      enddo
*
      call DSYMV('U',n,ONE,A,lda,work(1),1,ZERO,work(n+1),1)
*
      do j = 1,n
         work(j) = ZERO
      enddo
*
*******************************************************
** Initialize the control parameters to default value
*******************************************************
* setup the monitoring CG variables to default values
        call init_dcg(icntl,cntl)
*
        icntl(5) = 0
*
* Define the bound for the stopping criterion
        cntl(1)  = 1.0d-11
* Define the stream for the convergence history
        icntl(3) = 6
* Define the maximum number of iterations
        print *, ' Itermax '
        read(*,*) i
        icntl(6) = i
* Ask for the estimation of the condition number
*    (if icntl(7) == 1)
        icntl(7) = 1
* Define the stopping criterion 1=backward-error (default)
*     2=A-norm of the error
        icntl(8) = 2
* No preconditioner is provided
        icntl(4) = 0
*
*****************************************
** Reverse communication implementation
*****************************************
*
10     call drive_dcg(n,n,lwork,work,lworkR,workR,
     &         irc,icntl,cntl,info,rinfo)

       revcom = irc(1)
       colx   = irc(2)
       coly   = irc(3)
       colz   = irc(4)
*
       if (revcom.eq.matvec) then
* perform the matrix vector product for the CG iteration
*        work(colz) <-- A * work(colx)
         call dsymv ('U', n, ONE, A, lda,
     &            work(colx), 1, ZERO , work(colz), 1)
           goto 10
*
        else if (revcom.eq.precondleft) then
*        work(colz) <-- M * work(colx)
           call dcopy(n,work(colx),1,work(colz),1)
           goto 10
        else if (revcom.eq.dotProd) then
*      perform the scalar product for the CG iteration
*      work(colz) <-- work(colx) work(coly)
*
         work(colz)= ddot(n, work(colx),1, work(coly),1)
         goto 10
       endif
*
*******************************
* dump the solution on a file
*******************************
*
      if (icntl(7).eq.1) then
         write(*,*) ' Est. smallest eig. ', rinfo(2)
         write(*,*) ' Est. biggest  eig. ', rinfo(3)
      endif
*
      nout = 11
      open(nout,FILE='sol_dTestcg',STATUS='unknown')
      write(nout,*) 'info(1) = ',info(1),'  info(2) = ',info(2)
      write(nout,*) 'rinfo(1) = ',rinfo(1)
      write(nout,*) 'rinfo(2) = ',rinfo(2),'  rinfo(3) = ',rinfo(3)
      write(nout,*) 'Optimal workspace = ', info(3)
      write(nout,*) 'Solution : '
      do j=1,n
        write(nout,*) work(j)
        if (j.le.min(n,10)) write(*,*) j,work(j)
      enddo
      write(nout,*) '   '
*
100    continue
*
      stop
      end
\end{verbatim}
\end{multicols}
\end{small}
\end{landscape}
%
%
%
\bibliography{biblio}
%
\end{document}
