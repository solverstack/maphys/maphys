###
#
# @copyright (c) 2009-2014 The University of Tennessee and The University
#                          of Tennessee Research Foundation.
#                          All rights reserved.
# @copyright (c) 2012-2016 Inria. All rights reserved.
# @copyright (c) 2012-2014 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria, Univ. Bordeaux. All rights reserved.
#
###
#
#  @file GenPkgConfig.cmake
#
#  @project MORSE
#  MORSE is a software package provided by:
#     Inria Bordeaux - Sud-Ouest,
#     Univ. of Tennessee,
#     King Abdullah Univesity of Science and Technology
#     Univ. of California Berkeley,
#     Univ. of Colorado Denver.
#
#  @version 0.9.0
#  @author Cedric Castagnede
#  @author Emmanuel Agullo
#  @author Mathieu Faverge
#  @author Florent Pruvost
#  @date 19-08-2016
#
###

###
#
# GENERATE_PKGCONFIG_FILE: generate a file .pc according to the options
#
###
macro(GENERATE_PKGCONFIG_FILE)

    # The link flags specific to this package and any required libraries
    # that don't support PkgConfig
    set(MAPHYS_PKGCONFIG_LIBS "")
    # The link flags for private libraries required by this package but not
    # exposed to applications
    set(MAPHYS_PKGCONFIG_LIBS_PRIVATE "")
    # A list of packages required by this package
    set(MAPHYS_PKGCONFIG_REQUIRED "")
    # A list of private packages required by this package but not exposed to
    # applications
    set(MAPHYS_PKGCONFIG_REQUIRED_PRIVATE "")

    list(APPEND MAPHYS_PKGCONFIG_LIBS -lmaphys -lpackcg -lpackgmres -lslatec -lmph_toolkit)
    if (MAPHYS_BLASMT)
       list(APPEND MAPHYS_PKGCONFIG_LIBS -lmph_mt_tool)
    endif()

    list(APPEND MAPHYS_PKGCONFIG_LIBS_PRIVATE ${MAPHYS_EXTRA_LIBRARIES})
    list(REMOVE_ITEM MAPHYS_PKGCONFIG_LIBS_PRIVATE "hwloc")

    list(REMOVE_DUPLICATES MAPHYS_PKGCONFIG_LIBS)
    list(REMOVE_DUPLICATES MAPHYS_PKGCONFIG_LIBS_PRIVATE)
    list(REMOVE_DUPLICATES MAPHYS_PKGCONFIG_REQUIRED)
    list(REMOVE_DUPLICATES MAPHYS_PKGCONFIG_REQUIRED_PRIVATE)

    string(REPLACE ";" " " MAPHYS_PKGCONFIG_LIBS "${MAPHYS_PKGCONFIG_LIBS}")
    string(REPLACE ";" " " MAPHYS_PKGCONFIG_LIBS_PRIVATE "${MAPHYS_PKGCONFIG_LIBS_PRIVATE}")
    string(REPLACE ";" " " MAPHYS_PKGCONFIG_REQUIRED "${MAPHYS_PKGCONFIG_REQUIRED}")
    string(REPLACE ";" " " MAPHYS_PKGCONFIG_REQUIRED_PRIVATE "${MAPHYS_PKGCONFIG_REQUIRED_PRIVATE}")

    # Create .pc file
    # ---------------
    set(_output_file "${CMAKE_BINARY_DIR}/maphys.pc")
    set(_input_file "${CMAKE_CURRENT_SOURCE_DIR}/lib/pkgconfig/maphys.pc.in")
    # TODO: add url of MORSE releases in .pc file
    configure_file("${_input_file}" "${_output_file}" @ONLY)

    # installation
    # ------------
    INSTALL(FILES ${_output_file} DESTINATION lib/pkgconfig)

    # Copy headers to the install include directory

    INSTALL( FILES
      ${CMAKE_SOURCE_DIR}/include/mph_defs_f.h
      ${CMAKE_SOURCE_DIR}/include/mph_macros_f.h
      ${CMAKE_SOURCE_DIR}/include/mph_env_type_f.inc
      ${CMAKE_BINARY_DIR}/lib/cmph_maphys_type_c.h
      ${CMAKE_BINARY_DIR}/lib/dmph_maphys_type_c.h
      ${CMAKE_BINARY_DIR}/lib/smph_maphys_type_c.h
      ${CMAKE_BINARY_DIR}/lib/zmph_maphys_type_c.h
      DESTINATION ${CMAKE_INSTALL_PREFIX}/include
      )

endmacro(GENERATE_PKGCONFIG_FILE)

###
#
# generate_env_file: generate files maphys.pc
#
###
macro(generate_env_file)

    # Create .sh file
    # ---------------
    configure_file(
      "${CMAKE_CURRENT_SOURCE_DIR}/maphys_env.sh.in"
      "${CMAKE_CURRENT_BINARY_DIR}/bin/maphys_env.sh" @ONLY)

    # installation
    # ------------
    install(FILES "${CMAKE_CURRENT_BINARY_DIR}/bin/maphys_env.sh"
      DESTINATION bin)

endmacro(generate_env_file)

##
## @end file GenPkgConfig.cmake
##
