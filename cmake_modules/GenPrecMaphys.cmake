###
#
# @copyright (c) 2009-2014 The University of Tennessee and The University
#                          of Tennessee Research Foundation.
#                          All rights reserved.
# @copyright (c) 2012-2016 Inria. All rights reserved.
# @copyright (c) 2012-2014 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria, Univ. Bordeaux. All rights reserved.
#
###
#
#  @file CMakeLists.txt
#
#  @project MORSE
#  MORSE is a software package provided by:
#     Inria Bordeaux - Sud-Ouest,
#     Univ. of Tennessee,
#     King Abdullah Univesity of Science and Technology
#     Univ. of California Berkeley,
#     Univ. of Colorado Denver.
#
#  @author Florent Pruvost
#  @date 12-08-2016
#
###

include(ParseArguments)

# Generate sources depending on precisions
# ----------------------------------------
macro(generate_precisions_maphys)

  parse_arguments(MAPHYS_GP "TARGETDIR;PRECISIONS" "" ${ARGN})

  # The first is the output variable list
  car(OUTPUTLIST ${MAPHYS_GP_DEFAULT_ARGS})
  # Everything else should be source files
  cdr(SOURCES ${MAPHYS_GP_DEFAULT_ARGS})

  foreach(_prec ${MAPHYS_GP_PRECISIONS})

    foreach(_source ${SOURCES})

      set(gencmd ${PROJECT_SOURCE_DIR}/scripts/arithmetic_preprocessing ${_prec} ${PROJECT_SOURCE_DIR}/${MAPHYS_GP_TARGETDIR}/x${_source} ${CMAKE_CURRENT_BINARY_DIR}/${_prec}${_source})
      add_custom_command(
      OUTPUT  ${CMAKE_CURRENT_BINARY_DIR}/${_prec}${_source}
      COMMAND ${gencmd}
      DEPENDS ${PROJECT_SOURCE_DIR}/${MAPHYS_GP_TARGETDIR}/x${_source}
      )
      list(APPEND ${OUTPUTLIST} "${CMAKE_CURRENT_BINARY_DIR}/${_prec}${_source}")

    endforeach()

  endforeach()

endmacro(generate_precisions_maphys)