set(MAPHYS_TESTGENDIST)
set(MAPHYS_TESTGENDIST_X
  mph_testdistsys.F90
  )

generate_precisions_maphys(MAPHYS_TESTGENDIST "${MAPHYS_TESTGENDIST_X}" PRECISIONS ${MAPHYS_PRECISIONS} TARGETDIR gendistsys)

# Compilation step
# ----------------
foreach(_file ${MAPHYS_TESTGENDIST})
  get_filename_component(_name_exe ${_file} NAME_WE)
  add_executable(${_name_exe} ${_file})
  target_link_libraries(${_name_exe} maphys mph_toolkit ${MAPHYS_EXTRA_LIBRARIES})
  install(TARGETS ${_name_exe} DESTINATION ${CMAKE_INSTALL_PREFIX}/lib/maphys/gendistsys)
endforeach()

# install input files for gendistsys
set(MAPHYS_TESTGENDIST_INPUT_FILES
  cube.in
  dmph.in
  gds.in
  )

install(FILES ${MAPHYS_TESTGENDIST_INPUT_FILES} DESTINATION ${CMAKE_INSTALL_PREFIX}/lib/maphys/gendistsys)
