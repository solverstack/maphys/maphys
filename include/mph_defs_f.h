#ifndef MPH_DEFS_F_H__
#define MPH_DEFS_F_H__

#ifndef MAPHYS_VERSION
#define MAPHYS_VERSION '1.0.0'
#endif

#define MPH_SUCCESS         0

#define MPH_BOOL            INTEGER
#define MPH_TRUE            0
#define MPH_FALSE           1
#define MPH_BOOL_UNSET      2

#define MPH_INT             Integer
#define MPH_INTKIND         4
#define MPH_INTBYTESIZE     4
#define MPH_INTMPI          MPI_INTEGER

#define MAPHYS_STRL        1024
#define MAPHYS_ICNTL_SIZE  62
#define MAPHYS_RCNTL_SIZE  21
#define MAPHYS_IINFO_SIZE  37
#define MAPHYS_RINFO_SIZE  39
#define MAPHYS_IINFOG_SIZE  6
#define MAPHYS_RINFOG_SIZE  4
#define MAPHYS_IKEEP_SIZE  71
#define MAPHYS_RKEEP_SIZE  21

#define MTHREAD_ICNTL_SIZE        6

#define ANA_TIMING_SIZE          10

#define SDS_MAX_INDEX      3
#define DDS_MAX_INDEX      3

#endif
