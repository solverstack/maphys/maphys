#include "mpi.h"

#ifdef __cplusplus
 extern "C" {
#endif
#include <string.h>
#include <stdio.h>

typedef struct {float r,i;} maphys_complex;
typedef struct {double r,i;} maphys_double_complex;

#include "mph_defs_f.h"

typedef struct {

  /* ===============
     USER PARAMETERS
     ===============

      MPI communicator

      The MPI Communicator used by maphys.
      It must be set with a valid MPI Communicator before any call
      to maphys.
      */
  MPI_Comm comm;
  MPI_Fint fcomm;
     /* Input matrix (in coordinate format)
	--------------------------------------- */
     /* matrix symmetry
      - 0 = unsymmetric
      - 1 = symmetric
      - 2 = SPD
      . */
  int sym;
  /* !> order of the matrix */
  int n;
     /*  number of entries in the sparse matrix.
      If the matrix is symmetric or SPD,
      user only gives half of the entries. */
  int  nnz;
  int  *rows;
  int  *cols;
  XMPH_CFLOAT  *values;

  // If the user want to give the permutation he had to give some other info
  int  *permtab;
  int  *sizetab;

  // ask to write the input matrix to a file
  //string write_matrix;

  // Right-hand-side (in dense format ordered by columns)
  // ----------------------------------------------------
  int nrhs;
  XMPH_CFLOAT *rhs;

  // Solution (in dense format ordered by columns)
  // ----------------------------------------------------
  XMPH_CFLOAT *sol;

  // Controls
  // --------
  int job ;
  int   icntl[MAPHYS_ICNTL_SIZE];
  double rcntl[MAPHYS_RCNTL_SIZE];

  // Statistics
  // -----------
  // version
  char version[MAPHYS_STRL+2];

  // on this process (MPI)

  int  iinfo[MAPHYS_IINFO_SIZE];
  double rinfo[MAPHYS_RINFO_SIZE];

  // on all process (MPI)

  int iinfomin[MAPHYS_IINFO_SIZE];
  int iinfomax[MAPHYS_IINFO_SIZE];
  double iinfoavg[MAPHYS_IINFO_SIZE];
  double iinfosig[MAPHYS_IINFO_SIZE];

  double rinfomin[MAPHYS_RINFO_SIZE];
  double rinfomax[MAPHYS_RINFO_SIZE];
  double rinfoavg[MAPHYS_RINFO_SIZE];
  double rinfosig[MAPHYS_RINFO_SIZE];

  int iinfog[MAPHYS_IINFOG_SIZE];
  double rinfog[MAPHYS_RINFOG_SIZE];

} XMPH_maphys_t_c;

void xmph_maphys_driver_c( XMPH_maphys_t_c *cmaph );

void xmph_maphys_create_domain_c(XMPH_maphys_t_c * maphys_par, int myndof, int myndofinterior, int myndofintrf,
			       int lenindintrf, int mysizeintrf, int gballintrf, int mynbvi, int* myindexvi,
			       int * myptrindexvi, int* myindexintrf, int* myinterface, int myndoflogicintrf, int *mylogicintrf,
			       double* weight);

void XMPH_distributed_interface_c(XMPH_maphys_t_c * maphys_par, int myndof,
				  int mysizeintrf, int* myinterface, int mynbvi, int* myindexvi,
				  int * myptrindexvi, int* myindexintrf);
#ifdef __cplusplus
 }
#endif
