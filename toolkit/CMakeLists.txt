###
#
# @copyright (c) 2009-2014 The University of Tennessee and The University
#                          of Tennessee Research Foundation.
#                          All rights reserved.
# @copyright (c) 2012-2016 Inria. All rights reserved.
# @copyright (c) 2012-2014 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria, Univ. Bordeaux. All rights reserved.
#
###
#
#  @file CMakeLists.txt
#
#  @project MORSE
#  MORSE is a software package provided by:
#     Inria Bordeaux - Sud-Ouest,
#     Univ. of Tennessee,
#     King Abdullah Univesity of Science and Technology
#     Univ. of California Berkeley,
#     Univ. of Colorado Denver.
#
#  @author Florent Pruvost
#  @date 11-08-2016
#
###


# Define the list of sources
# --------------------------

set(MAPHYS_TOOLKIT
  mph_toolkit_parser_mod.F90
  )
set(MAPHYS_TOOLKIT_X
  mph_toolkit_gen_mod.F90
  mph_toolkit_mod.F90
  mph_toolkit_read_mod.F90
  )
generate_precisions_maphys(MAPHYS_TOOLKIT "${MAPHYS_TOOLKIT_X}" PRECISIONS ${MAPHYS_PRECISIONS} TARGETDIR toolkit)

# Define the library from sources
# -------------------------------
add_library(mph_toolkit ${MAPHYS_TOOLKIT})
target_link_libraries(mph_toolkit maphys packcg packgmres slatec)

# installation
# ------------
install(TARGETS mph_toolkit
  DESTINATION lib)

#
# Tools for multithreading
#

if (MAPHYS_BLASMT)
  add_library(mph_mt_tool mph_toolkit_bindproc.c)
  if(HWLOC_FOUND)
    target_compile_definitions(mph_mt_tool PUBLIC "-DHAVE_LIBHWLOC")
  endif()
  install(TARGETS mph_mt_tool DESTINATION lib)
endif()

###
### END CMakeLists.txt
###
