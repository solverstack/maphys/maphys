#ifdef HAVE_LIBHWLOC
#include <hwloc.h>
#include <mpi.h>
#include <omp.h>

#ifdef MULTITHREAD_WITH_OPENBLAS

#include <cblas.h>
int mph_toolkit_get_parallel_(int * is_mt){
  *is_mt = openblas_get_parallel();
  return 0;
}
#else
#include <mkl_blas.h>
int mph_toolkit_get_parallel_(int * is_mt){
  if (mkl_get_max_threads() == 1){
    *is_mt = 0;
  }
  else{
    *is_mt = 1;
  }
  return 0;
}
#endif

// TOOO change comments
/*> bind an MPI process to a core.
**  Each MPI processes are separated by <topo_t> cores.
**
** param [in] my_mpirank the mpirank of the process
** param [in] topo_n     the number of nodes
** param [in] topo_c     the number of cores per node
** param [in] topo_t     the number of threads per MPI process
** param [in] topo_p     the number of MPI processes
 */
int
mph_toolkit_get_multicore_info_ (int *nb_nodes,
				 int *nb_cores,
				 int *comm_size)
{
  hwloc_topology_t topology; /* Topology object */
  int comm_size_ = 0;                   /* cpu rank to be computed */
  int nb_nodes_ = 0, nb_cores_ = 0;

/*   int myrank; */
/*   MPI_Comm_rank(MPI_COMM_WORLD, &myrank); */

/*   if (myrank == 0) {  */
/*     scanf("%*c"); */
/*     printf("!!!!!!!!!!!!!! passed !!!!!!!!!!!!! \n"); */
/*   }   */


  /* Allocate and initialize topology object.  */
  hwloc_topology_init(&topology);

  /* Perform the topology detection.  */
  hwloc_topology_load(topology);

  /* Get the number of cores */
  nb_cores_ = hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_CORE);

  /* Get the number of cores on the node */
  MPI_Comm_size(MPI_COMM_WORLD, &comm_size_);

  /*   Get the number of nodes used  */
  system("cat $(echo $PBS_NODEFILE) | uniq > /tmp/machines");
  char *machinefile = "/tmp/machines";

  FILE *file = fopen(machinefile, "r");
  nb_nodes_ = 0;

  if ( file != NULL )
    {
      char line [ 256 ];
      /* read a line */
      while ( fgets(line, 256, file) != NULL )
	nb_nodes_++;

      fclose(file);
    }
  else
    {
      perror(machinefile); /* why didn't the file open? */
      exit(-1);
    }

  *nb_nodes = nb_nodes_;
  *nb_cores = nb_cores_;
  *comm_size = comm_size_;

  /* Destroy topology object.  */
  hwloc_topology_destroy(topology);

  /* return success */
  return 0;
}


int
mph_toolkit_unbind_threads_()
{
  hwloc_topology_t topology; /* Hwloc topology object */
  hwloc_obj_t      obj;      /* Hwloc object    */
  hwloc_bitmap_t   cpuset = hwloc_bitmap_alloc ();   /* HwLoc cpuset    */
  hwloc_bitmap_t   cpuset_tmp = hwloc_bitmap_alloc ();   /* HwLoc cpuset    */
  int cpu ;                   /* core to whic needs to be binded  */
  int i;
  int nb_cores;
  char string[200];

  /* Allocate and initialize topology object.  */
  hwloc_topology_init(&topology);

  /* Perform the topology detection.  */
  hwloc_topology_load(topology);

  nb_cores = hwloc_get_nbobjs_by_type (topology, HWLOC_OBJ_CORE);

  for ( i = 0, cpu= 0; i < nb_cores;  i++, cpu++)
    {
      obj = hwloc_get_obj_by_type(topology, HWLOC_OBJ_CORE, cpu);

      if (!obj)

	return 0;

      cpuset_tmp = hwloc_bitmap_dup(obj->cpuset);

      /* Get only one logical processor (in case the core is SMT/hyperthreaded).  */
      hwloc_bitmap_singlify(cpuset_tmp);

      hwloc_bitmap_or(cpuset, cpuset_tmp, cpuset);
    }

  if (hwloc_set_cpubind(topology, cpuset, HWLOC_CPUBIND_THREAD)) {
    char *str = NULL;
    hwloc_bitmap_asprintf(&str, obj->cpuset);
    free(str);
  }


#pragma omp parallel default(shared)

  {

    hwloc_set_cpubind(topology, cpuset, HWLOC_CPUBIND_THREAD);

  }

  /* And try to bind ourself there.  */

  /* Free our cpuset copy */
  hwloc_bitmap_free(cpuset);

  /* Destroy topology object.  */
  hwloc_topology_destroy(topology);

  /* return success */
  return 0;
}




int
mph_toolkit_bind_main_thread_(int *my_mpirank,
			      int *nb_proc,
			      int *nb_nodes,
			      int *nb_cores)
{
  hwloc_topology_t topology; /* Hwloc topology object */
  hwloc_obj_t      obj;      /* Hwloc object    */
  hwloc_bitmap_t   cpuset;   /* HwLoc cpuset    */
  int cpu;                   /* core to whic needs to be binded  */
  int p_per_n;               /* processus per node */

  /* Allocate and initialize topology object.  */
  hwloc_topology_init(&topology);

  /* Perform the topology detection.  */
  hwloc_topology_load(topology);

  p_per_n = (*nb_proc + *nb_nodes - 1) / *nb_nodes;
  cpu = (*my_mpirank % p_per_n) * (*nb_cores / p_per_n);

  /* Get last one.  */
  obj = hwloc_get_obj_by_type(topology, HWLOC_OBJ_CORE, cpu);
  if (!obj)
    return 0;

  /* Get a copy of its cpuset that we may modify.  */
  cpuset = hwloc_bitmap_dup(obj->cpuset);

  /* Get only one logical processor (in case the core is SMT/hyperthreaded).  */
  hwloc_bitmap_singlify(cpuset);

  /* And try to bind ourself there.  */
  if (hwloc_set_cpubind(topology, cpuset, HWLOC_CPUBIND_THREAD)) {
    char *str = NULL;
    hwloc_bitmap_asprintf(&str, obj->cpuset);
    free(str);
  }

  /* Free our cpuset copy */
  hwloc_bitmap_free(cpuset);

  /* Destroy topology object.  */
  hwloc_topology_destroy(topology);

  /* return success */
  return 0;
}

int
mph_toolkit_grouped_bind_(int *my_mpirank,
			  int *nb_proc,
			  int *nb_nodes,
			  int *nb_cores)
{
  hwloc_topology_t topology; /* Hwloc topology object */
  hwloc_obj_t      obj;      /* Hwloc object    */
  hwloc_bitmap_t   cpuset = hwloc_bitmap_alloc ();   /* HwLoc cpuset    */
  hwloc_bitmap_t   cpuset_tmp = hwloc_bitmap_alloc ();   /* HwLoc cpuset    */
  int cpu ;                   /* core to whic needs to be binded  */
  int p_per_n;               /* processus per node */
  int nb_threads;            /* Number of threads */
  int i;
  char string[200];

  /* Allocate and initialize topology object.  */
  hwloc_topology_init(&topology);

  /* Perform the topology detection.  */
  hwloc_topology_load(topology);

  nb_threads = ((double) *nb_nodes /  (double) *nb_proc) *  (double) *nb_cores;
  p_per_n = (*nb_proc + *nb_nodes - 1) / *nb_nodes;
  cpu = (*my_mpirank % p_per_n) * (*nb_cores / p_per_n);

  for ( i = 0; i < nb_threads;  i++, cpu++)
    {
      obj = hwloc_get_obj_by_type(topology, HWLOC_OBJ_CORE, cpu);

      if (!obj)
	return 0;

      cpuset_tmp = hwloc_bitmap_dup(obj->cpuset);

      /* Get only one logical processor (in case the core is SMT/hyperthreaded).  */
      hwloc_bitmap_singlify(cpuset_tmp);

      hwloc_bitmap_or(cpuset, cpuset_tmp, cpuset);
    }

  /* And try to bind ourself there.  */

  if (hwloc_set_cpubind(topology, cpuset, HWLOC_CPUBIND_PROCESS)) {
    char *str = NULL;
    hwloc_bitmap_asprintf(&str, obj->cpuset);
    free(str);
  }

  /* Free our cpuset copy */
  hwloc_bitmap_free(cpuset);

  /* Destroy topology object.  */
  hwloc_topology_destroy(topology);

  /* return success */
  return 0;
}



int
mph_toolkit_bind_openmp_threads_(int *my_mpirank,
				 int *nb_proc,
				 int *nb_nodes,
				 int *nb_cores)
{
  hwloc_topology_t topology; /* Topology object */
  int p_per_n;               /* processus per node */
  int first_core;            /* The core to where the first threads needs to be binded */

  p_per_n = (*nb_proc + *nb_nodes - 1) / *nb_nodes;
  first_core = (*my_mpirank % p_per_n) * (*nb_cores / p_per_n);

  /* Allocate and initialize topology object.  */
  hwloc_topology_init(&topology);

  /* Perform the topology detection.  */
  hwloc_topology_load(topology);

  int size = 1000;
  double *vect = malloc( size * sizeof(*vect) );
  int one = 1;
  int i;
  for ( i = 0; i < size; i++)
    vect[i] = 1.0;
  cblas_ddot(size, vect, one, vect, one);
  free(vect);

#pragma omp parallel default(shared)

  {

    hwloc_obj_t      obj;      /* Hwloc object    */
    hwloc_bitmap_t   bitmap;   /* HwLoc cpuset    */
    int cpu;                   /* cpu rank to be computed */

    cpu = omp_get_thread_num() + first_core;

    /* Get last one.  */
    obj = hwloc_get_obj_by_type(topology, HWLOC_OBJ_CORE, cpu);

    /* Get a copy of its cpuset that we may modify.  */
    bitmap = hwloc_bitmap_dup(obj->cpuset);

    /* Get only one logical processor (in case the core is SMT/hyperthreaded).  */
    hwloc_bitmap_singlify(bitmap);

    /* And try to bind ourself there.  */

    if (hwloc_set_cpubind(topology, bitmap, HWLOC_CPUBIND_THREAD)) {
      char *str = NULL;
      hwloc_bitmap_asprintf(&str, obj->cpuset);
      printf("Couldn't bind to cpuset %s\n", str);
      free(str);
    }

    /*   Free our cpuset copy */
    hwloc_bitmap_free(bitmap);

  }

  /* Destroy topology object.  */
  hwloc_topology_destroy(topology);

  /* return success */
  return 0;

}

#else
/* dummy function */
int
mph_toolkit_get_parallel_
(int * is_mt)
{ *is_mt = -1 ; return 0; }

int
mph_toolkit_get_multicore_info_
(int *nb_nodes, int *nb_cores, int *comm_size)
{return 0;}

int
mph_toolkit_bind_main_thread_(int *my_mpirank,
			      int *nb_cores,
			      int *nb_threads)
{return 0;}

int
mph_toolkit_unbind_threads_()
{return 0;}

int
mph_toolkit_grouped_bind_(int *my_mpirank,
                          int *nb_proc,
                          int *nb_nodes,
                          int *nb_cores)
{return 0;};


int
mph_toolkit_bind_openmp_threads_(int *my_mpirank,
				 int *nb_cores,
				 int *nb_threads)
{return 0;}
#endif

