configure_file(test.sh.in test.sh @ONLY)
add_test(NAME alaindelon_si
  WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
  COMMAND ${CMAKE_CURRENT_BINARY_DIR}/test.sh  ${CMAKE_BINARY_DIR}/examples/dmph_examplerestart)

set_tests_properties(alaindelon_si PROPERTIES FAIL_REGULAR_EXPRESSION "MAPHYS_FAILED")
set_tests_properties(alaindelon_si PROPERTIES TIMEOUT 60)
