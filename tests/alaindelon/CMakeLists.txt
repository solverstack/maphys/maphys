###
#
# Alain Delon test case directories
#
###
if(MAPHYS_BUILD_EXAMPLES)
  add_subdirectory(ss)
  add_subdirectory(st)
  add_subdirectory(sth)
  add_subdirectory(si)
  add_subdirectory(ms)
  add_subdirectory(mt)
  add_subdirectory(mm)
  add_subdirectory(mi)
endif()
###
### END CMakeLists.txt
###
