# Define the list of sources for the lib
# --------------------------------------

# Regression tests
# --------------------------------------
set (MAPHYS_TEST_REGR
  test_initguess.F90)

# Test of CGC with all possible strategies

configure_file(test_cgc.sh.in test_cgc.sh @ONLY)

install(FILES ${CMAKE_CURRENT_BINARY_DIR}/test_cgc.sh
  DESTINATION ${CMAKE_INSTALL_PREFIX}/tests
  PERMISSIONS OWNER_EXECUTE OWNER_WRITE OWNER_READ
  GROUP_EXECUTE GROUP_READ)

add_test(test_cgc_SD  ./test_cgc.sh 0)
add_test(test_cgc_DC  ./test_cgc.sh 1)
add_test(test_cgc_SC  ./test_cgc.sh 2)
add_test(test_cgc_SRC ./test_cgc.sh 3)
add_test(test_cgc_SHD ./test_cgc.sh 4)
set_tests_properties(test_cgc_SD PROPERTIES TIMEOUT 60)
set_tests_properties(test_cgc_DC PROPERTIES TIMEOUT 60)
set_tests_properties(test_cgc_SC PROPERTIES TIMEOUT 60)
set_tests_properties(test_cgc_SRC PROPERTIES TIMEOUT 60)
set_tests_properties(test_cgc_SHD PROPERTIES TIMEOUT 60)

if ( FABULOUS_FOUND )

  configure_file(test_fab.sh.in test_fab.sh @ONLY)

  set (MAPHYS_TEST_REGR
    ${MAPHYS_TEST_REGR}
    test_multirhs_centr.F90
    test_multirhs_dist.F90
    test_multirhs_centr_block.F90
    test_multirhs_dist_block.F90
    test_c_multirhs_dist.c
    )

  # Test of fabulous with all possibilities for ICNTL(29)
  install(FILES ${CMAKE_CURRENT_BINARY_DIR}/test_fab.sh
    DESTINATION ${CMAKE_INSTALL_PREFIX}/tests
    PERMISSIONS OWNER_EXECUTE OWNER_WRITE OWNER_READ
    GROUP_EXECUTE GROUP_READ)

  add_test(test_fabulous_algo_std    ./test_fab.sh 0)
  add_test(test_fabulous_algo_ib     ./test_fab.sh 1)
  add_test(test_fabulous_algo_qr     ./test_fab.sh 2)
  add_test(test_fabulous_algo_dr     ./test_fab.sh 3)
  add_test(test_fabulous_algo_ibdr   ./test_fab.sh 4)
  add_test(test_fabulous_algo_qrib   ./test_fab.sh 5)
  add_test(test_fabulous_algo_qrdr   ./test_fab.sh 6)
  add_test(test_fabulous_algo_qribdr ./test_fab.sh 7)
  add_test(test_fabulous_algo_cgr    ./test_fab.sh 8)

endif()

foreach(_file ${MAPHYS_TEST_REGR})
  get_filename_component(_name_exe ${_file} NAME_WE)
  add_executable(${_name_exe} ${_file})
  target_link_libraries(${_name_exe} maphys mph_toolkit slatec ${MAPHYS_EXTRA_LIBRARIES})
  install(TARGETS ${_name_exe} DESTINATION ${CMAKE_INSTALL_PREFIX}/tests)
  add_test(${_name_exe} ${MPIEXEC_EXECUTABLE} ${MPIEXEC_NUMPROC_FLAG} 2 ${MPIEXEC_PREFLAGS} ./${_name_exe} ${MPIEXEC_POSTFLAGS})
  set_tests_properties(${_name_exe} PROPERTIES FAIL_REGULAR_EXPRESSION "MAPHYS_ERROR")
  set_tests_properties(${_neme_exe} PROPERTIES TIMEOUT 60)
endforeach()

###
### END CMakeLists.txt
###
