#ifndef MATRIX_H
#define MATRIX_H

typedef struct PhidalMatrix_ PhidalMatrix;
struct UPhidalMatrix_ {
  PhidalMatrix* L;
  PhidalMatrix* U;
};
typedef struct UPhidalMatrix_ UPhidalMatrix;

typedef struct PhidalDistrMatrix_ PhidalDistrMatrix;
struct UPhidalDistrMatrix_ {
  PhidalDistrMatrix* L;
  PhidalDistrMatrix* U;
};
typedef struct UPhidalDistrMatrix_ UPhidalDistrMatrix;

typedef struct DBMatrix_ DBMatrix;
struct UDBMatrix_ {
  DBMatrix* L;
  DBMatrix* U;
};
typedef struct UDBMatrix_ UDBMatrix;

typedef struct DBDistrMatrix_ DBDistrMatrix;
struct UDBDistrMatrix_ {
  DBDistrMatrix* L;
  DBDistrMatrix* U;
};
typedef struct UDBDistrMatrix_ UDBDistrMatrix;

#ifndef PARALLEL
#define D(arg) arg
#else
#define D(arg) d ## arg
#endif

#define M_MALLOC(mtx, t)  { mtx = (Matrix*)malloc(sizeof(Matrix)); mtx->type = t; }
#define M_CLEAN(mtx)    { if (mtx != NULL) { free(mtx); mtx=NULL; } }

#define M_BLOCK(mtx) mtx->m.D(b)
#define M_SCAL(mtx)  mtx->m.D(s)

enum MatrixType {SCAL, BLOCK, dSCAL, dBLOCK};

typedef union  {
  UPhidalMatrix       s;
  UPhidalDistrMatrix ds;
  UDBMatrix           b;
  UDBDistrMatrix     db;
} Matrix__;

typedef struct Matrix_ Matrix;
struct Matrix_ {
  flag_t   type;
  Matrix__ m;
};

#endif /*MATRIX_H*/
