/* @authors J. GAIDAMOUR, P. HENON, X. LACOSTE */


/*
Title: HIPS
 
  
   Description:
   HIPS (Hierarchical Iterative Parallel Solver) is a C-library for solving 
   large sparse linear systems on parallel platforms. 
   The code provides : 
   - multistage ILU(t) (formaly known as PHIDAL) to enable a highly parallel scheme whereby several subdomains can be assigned a process. 
   - a hybrid method which blends direct and iterative solvers. HIPS exploits the partitioning and multistage ILU(t) techniques   to enable a highly parallel scheme 
   whereby several subdomains can be assigned a process. 
    
   HIPS is a parallel solver for sparse linear (symmetric, unsymmetric, real or complex) systems. 
   HIPS can be used as a stand-alone program that reads a sparse linear system from a file ; it also
   provides an interface to be called from any C, C++ or Fortran code.



  Integers:
     In all HIPS function : integer parameters can be *INTS* or *INTL*.

     Depending on your configuration of "makefile.inc", 
     *INTS* and *INTL* can be 32 or 64 bits.
     
     By default (no define of INTSIZE in makefile.inc), *INTS* and *INTL*  
     are set to the standard C type "int" and the standard Fortran type "INTEGER".
     On some architecture,  C "int" and Fortran "INTEGER" do not have the same length in bytes :
     in this case it is mandatory to use INTSIZE32 or INTSIZE64 in makefile.inc .
     
   
     If *-DINTSIZE32* is defined in makefile.inc, *INTS* and *INTL* will both be 32
     bits integers.
   
     If *-DINTSIZE64* is defined in makefile.inc, *INTS* will be 32 bits integers
     whereas *INTL* will be INT64 integers.
     
     The option *-DINTSSIZE64*, (both *INTS* and *INTL* will be 64
     bit integers) is not available in the beta version.
     
     Anyway, we do not recommand to use *-DINTSSIZE64*, INTS do not need to be code on 64 bit integer since it represented usually number less than 2.10^9.
     It would be a memory waste.

  Coefficients:

     The coefficients of the matrices and vectors are of type *COEF*.
   


     *COEF* can be simple or double precision and real or complex
     depending on compilation options.
     - In this beta version, only double precision is supported -
   
     By default of if *-DPREC_DOUBLE* is defined in makefile.inc, 
     *COEF* will be real double precision.
     
     If *-DPREC_SIMPLE* is defined in makefile.inc,  *COEF*
     will be coded in simple precision.
   
     If *-DTYPE_COMPLEX* is defined, *COEF* means complex, otherwise,
     by default or if *-DTYPE_REAL* is defined, it means REAL.

  Floating parameters:
    
     Other floating parameters are of type *REAL* which can be simple or 
     double precision.
     
     As for Coefficients, *REAL* precision also depends on 
     *-DPREC_DOUBLE* or *-DPREC_SIMPLE*  compilation option.
     

  HIPS contributors :

       Jérémie Gaidamour - gaidamou@labri.fr 
       Pascal  Hénon     - henon@labri.fr 
       Yousef  Saad      - saad@cs.umn.edu
*/


#ifndef HIPS_H
#define HIPS_H
#include <stdio.h>
#include "type.h"

#ifdef INTL
#warning INTL already defined
#undef INTL
#endif

#ifdef INTS
#warning INTS already defined
#undef INTS
#endif

#ifdef INTSIZE32
#define INTS           int32_t
#define INTL           int32_t
#elif (defined INTSIZE64)
#define INTS           int32_t
#define INTL           int64_t
#elif (defined INTSSIZE64)
#define INTS           int64_t 
#define INTL           int64_t 
#else
/*
#elif defined(LONG)
#define INTS           int
#define INTL           long
#else
*/
#define INTS           int
#define INTL           int
#endif

#ifndef   PREC_SIMPLE
#define REAL double 

#ifdef    TYPE_COMPLEX
#include <complex.h>
#define COEF double complex
#else  
#define COEF double
#endif 

#else  

#define REAL float
#ifdef    TYPE_COMPLEX
#include <complex.h>
#define COEF float complex
#else  
#define COEF float

#endif 
#endif 

/* 
Internal type for hips 
*/ 
typedef INTS dim_t;
typedef INTS int_t;
typedef INTS flag_t;
typedef INTS mpi_t;
typedef INTS blas_t;



/* Only works if no FLOAT in args */
#if (defined _COMPLEX_H || defined _H_COMPLEX)
#define MULTIPLE_TYPE_DEFINE(functype, funcname, funcargs) \
  functype S ## funcname funcargs;			   \
  functype D ## funcname funcargs;			   \
  functype C ## funcname funcargs;			   \
  functype Z ## funcname funcargs;
#else
#define MULTIPLE_TYPE_DEFINE(functype, funcname, funcargs) \
  functype S ## funcname funcargs;			   \
  functype D ## funcname funcargs;
#endif
#if (defined _COMPLEX_H || defined _H_COMPLEX)
#define MULTIPLE_TYPE_DEFINE_F(functype,			\
			       funcname,			\
			       funcargs_s,			\
			       funcargs_d,			\
			       funcargs_c,			\
			       funcargs_z)			\
  functype S ## funcname funcargs_s;				\
  functype D ## funcname funcargs_d;				\
  functype C ## funcname funcargs_c;				\
  functype Z ## funcname funcargs_z;
#else
#define MULTIPLE_TYPE_DEFINE_F(functype,			\
			       funcname,			\
			       funcargs_s,			\
			       funcargs_d,			\
			       funcargs_c,			\
			       funcargs_z)			\
  functype S ## funcname funcargs_s;				\
  functype D ## funcname funcargs_d;
#endif
#define SCOEF float
#define DCOEF double
#define CCOEF float  complex
#define ZCOEF double complex



/* 
   Group: Solver setup functions
*/
/*
  Function: HIPS_Initialize

  In HIPS, each different linear problem is identified by an id.
  This id corresponds to a internal structure that
  contains intern data such as a matrix, options for a given problem.
  Unless you need to solve concurently several linear systems you do not need 
  to set idnbr > 1. 

  Parameters: 
    idnbr        - Maximum number of different problems that will be
            created.

  Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.


  Fortran interface:
  >
  > SUBROUTINE HIPS_INITIALIZE(IDNBR, IERROR)
  >   INTS, INTENT(IN)  :: IDNBR
  >   INTS, INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_INITIALIZE
 */
INTS HIPS_Initialize(INTS idnbr);
MULTIPLE_TYPE_DEFINE(INTS, HIPS_Initialize, (INTS idnbr));


/*
  Function: HIPS_SetDefaultOptions

  Set default options corresponding to a specific solver strategy for the problem number *id*.
  A strategy is identified by a *stratnum* ID.
  In HIPS, you can choose between a full iterative and a hybrid direct/iterative strategy.
  
  Parameters: 
    id           - Problem identification number.  
    stratnum     - Solver strategy id. Can be set to HIPS_ITERATIVE or HIPS_HYBRID.


  Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error.
                   This number can be used in HIPS_PrintError 
		   or HIPS_ExitOnError.
  Fortran interface:
  >
  > SUBROUTINE HIPS_SETDEFAULTOPTIONS(ID, STRATNUM, IERROR)
  >   INTS, INTENT(IN)  :: ID, STRATNUM
  >   INTS, INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_SETDEFAULTOPTIONS
 */
INTS HIPS_SetDefaultOptions(INTS id, INTS stratnum);
MULTIPLE_TYPE_DEFINE(INTS, HIPS_SetDefaultOptions, (INTS id, INTS stratnum));


/*
  Function: HIPS_SetOptionINT

  Set an option described by an integer number. An option is indentified by *number*. *value* contain the value to assign to the option *number* 
  for the problem *id*.

  Parameters: 
    id     - Problem identification number.  
    number - Identification of the integer parameter.  
    value  - Value to assign.

  Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.



  Fortran interface:
  >
  > SUBROUTINE HIPS_SETOPTIONINT(ID, NUMBER, VALUE, IERROR)
  >   INTS, INTENT(IN)    :: ID, NUMBER, VALUE
  >   INTS, INTENT(OUT)   :: IERROR
  > END SUBROUTINE HIPS_SETOPTIONINT
 */
INTS HIPS_SetOptionINT(INTS id, INTS number, INTS value);
MULTIPLE_TYPE_DEFINE(INTS, HIPS_SetOptionINT, (INTS id, INTS number, INTS value));

/*
  Function: HIPS_SetOptionREAL

  Set an option described by a real number. An option is indentified by *number*. *value* contain the value to assign to the option *number* 
  for the problem *id*.

  Parameters: 
    id     - Problem identification number.  
    number - Identification of the integer parameter.  
    value  - Value to set the parameter to.

  Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.
  Fortran interface:
  >
  > SUBROUTINE HIPS_SETOPTIONREAL(ID, NUMBER, VALUE, IERROR)
  >   INTS, INTENT(IN)    :: ID, NUMBER
  >   REAL, INTENT(IN)    :: VALUE
  >   INTS, INTENT(OUT)   :: IERROR
  > END SUBROUTINE HIPS_SETOPTIONREAL
 */
INTS HIPS_SetOptionREAL(INTS id, INTS number, REAL value);
MULTIPLE_TYPE_DEFINE(INTS, HIPS_SetOptionREAL, (INTS id, INTS number, REAL value));


/* 
   Group: Graph setup function
*/
/*
  Function: HIPS_GraphBegin

  Begin building the adjency graph for renumbering and all 
  preprocessing.

  Allocate temporary structures needed to build the graph.
  
  Parameters:
    id      - Problem identification number.   
    n       - Global number of nodes in the graph.
    edgenbr - Number of edges which will be added in the graph by proc.

  Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.


  Fortran interface:
  >
  > SUBROUTINE HIPS_GRAPHBEGIN(ID, N, EDGENBR, IERROR)
  >   INTS,      INTENT(IN)  :: ID, N
  >   INTL,      INTENT(IN)  :: EDGENBR
  >   INTS,      INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_GRAPHBEGIN
*/ 
INTS HIPS_GraphBegin(INTS id, INTS n, INTL edgenbr);
MULTIPLE_TYPE_DEFINE(INTS, HIPS_GraphBegin, (INTS id, INTS n, INTL edgenbr));

/*
  Function: HIPS_GraphEdge

  Adds an edge to the graph.

   <HIPS_GraphBegin> must have been called before.

  Parameters: 
    id      - Problem identification number.  
    row     - First vertex of the edge.
    col     - Second vertex of the edge.

  Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.



  Fortran interface:
  >
  > SUBROUTINE HIPS_GRAPHEDGE(ID, ROW, COL, IERROR)
  >   INTS,      INTENT(IN)  :: ID, ROW, COL
  >   INTS,      INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_GRAPHEDGE
*/ 
INTS HIPS_GraphEdge(INTS id, INTS col, INTS row);
MULTIPLE_TYPE_DEFINE(INTS, HIPS_GraphEdge, (INTS id, INTS col, INTS row));


/*
  Function: HIPS_GraphEnd

  End the graph building.
  <HIPS_GraphBegin> must have been called before.
  
  Parameters: 
    id     - Problem identification number.  


    Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.



  Fortran interface:
  >
  > SUBROUTINE HIPS_GRAPHEND(ID, IERROR)
  >   INTS,      INTENT(IN)  :: ID
  >   INTS,      INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_GRAPHEND
*/ 
INTS HIPS_GraphEnd(INTS id);
MULTIPLE_TYPE_DEFINE(INTS, HIPS_GraphEnd, (INTS id));


/*
  Function: HIPS_GraphDistrCSR

  Enter the matrix adjacency graph using the distributed  Compress Sparse Row  format.
  Each processors has a set of row : the list of these rows (in global number) is in nodelist.
  If the set of rows 


  Parameters: 
    id          - Problem identification number.
    n           - total number of vertice
    ln          - Number of local rows
    nodelist    - List of the local row in global numerbing : cols(lrowptr(i):lrowptr(i+1)) are the columns indices of edges in the row nodelist(i)  
    lrowptr     - Index of the first element of each row in *LCOLS* and *VALUES* arrays.
    lcols       - Local column indice array.

  Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.

  Fortran interface:
  >
  > SUBROUTINE HIPS_GRAPHDISTRCSR(ID, N, LN, NODELIST, LROWPTR, COLS, IERROR)
  >   INTS,               INTENT(IN)  :: ID, LN, N
  >   INTS, DIMENSION(0), INTENT(IN)  :: NODELIST, COLS
  >   INTL, DIMENSION(0), INTENT(IN)  :: LROWPTR
  >   INTS,               INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_GRAPHDISTRCSR
*/ 
INTS HIPS_GraphDistrCSR(INTS id,  INTS n, INTS ln, INTS *nodelist, INTL *lrowptr, INTS *cols);
MULTIPLE_TYPE_DEFINE(INTS, HIPS_GraphDistrCSR, 
		     (INTS id, INTS n, INTS ln, INTS * nodelist, INTL *lrowptr, INTS *cols));

/*
  Function: HIPS_GraphGlobalCSR

  Build an adjency graph from a Compress Sparse Row matrix pattern.


  Parameters: 
    id     - Problem identification number.
    n       - Global number of columns
    rowptr  - Index of the first element of each row in *COLS* array.
    cols    - Global column numbers array.
    root    - Root processor : this processor enter the global data. 

 Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.

  Fortran interface:
  >
  > SUBROUTINE HIPS_GRAPHGLOBALCSR(ID, N, ROWPTR, COLS, ROOT, IERROR)
  >   INTS,               INTENT(IN)  :: ID, N, ROOT
  >   INTL, DIMENSION(0), INTENT(IN)  :: ROWPTR
  >   INTS, DIMENSION(0), INTENT(IN)  :: COLS
  >   INTS,               INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_GRAPHGLOBALCSR
*/ 
INTS HIPS_GraphGlobalCSR(INTS id, INTS n, INTL *rowptr, INTS *cols, INTS root);
MULTIPLE_TYPE_DEFINE(INTS, HIPS_GraphGlobalCSR, 
		     (INTS id, INTS n, INTL *rowptr, INTS *cols, INTS root));

/*
  Function: HIPS_GraphGlobalCSC

  Build an adjency graph from a Compress Sparse Column matrix pattern.
  
  Needs <HIPS_SetDefaultOptions> to be called before.

  This function depends on integer parameter *HIPS_BASEVAL*.

  Parameters: 
    id     - Problem identification number.
    n       - Global number of columns
    colptr  - Index of the first element of each column in *ROWS* array.
    rows    - Global row number array.
    root    - Root processor : this processor enter the global data. 

 Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.

  Fortran interface:
  >
  > SUBROUTINE HIPS_GRAPHGLOBALCSC(ID, N, COLPTR, ROWS, ROOT, IERROR)
  >   INTS,               INTENT(IN)  :: ID, N, ROOT
  >   INTL, DIMENSION(0), INTENT(IN)  :: COLPTR
  >   INTL, DIMENSION(0), INTENT(IN)  :: ROWS
  >   INTS,               INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_GRAPHGLOBALCSC
*/ 
INTS HIPS_GraphGlobalCSC(INTS id, INTS n, INTL *colptr, INTS *rows, INTS root);
MULTIPLE_TYPE_DEFINE(INTS, HIPS_GraphGlobalCSC, 
		     (INTS id, INTS n, INTL *colptr, INTS *rows, INTS root));

/*
  Function: HIPS_GraphGlobalIJV

  Build an adjency graph from a Compress Sparse Column matrix pattern.
  
  Needs <HIPS_SetDefaultOptions> to be called before.

  This function depends on integer parameter *HIPS_BASEVAL*.

  Parameters: 
    id      - Problem identification number.
    n       - Global number of unknowns.
    nnz     - Global number of non zeros.
    row     - Global column number array.
edges.
    col     - Global row number array.
    root    - Root processor : this processor enter the global data. 


 Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.

  Fortran interface:
  >
  > SUBROUTINE HIPS_GRAPHGLOBALIJV(ID, N, NNZ, ROW, COL, ROOT, IERROR)
  >   INTS,               INTENT(IN)  :: ID, N, ROOT
  >   INTL,               INTENT(IN)  :: NNZ
  >   INTS, DIMENSION(0), INTENT(IN)  :: ROW
  >   INTS, DIMENSION(0), INTENT(IN)  :: COL
  >   INTS,               INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_GRAPHGLOBALIJV
*/ 
INTS HIPS_GraphGlobalIJV(INTS id, INTS n, INTL nnz, INTS *row, INTS *col, INTS root);

MULTIPLE_TYPE_DEFINE(INTS, HIPS_GraphGlobalIJV, 
		     (INTS id, INTS n, INTL nnz, INTS *row, INTS * col, INTS root));

/* 
   Group: IO functions

   Allows to save and load solver state after preprocessing.
*/
/*
  Function: HIPS_SetupSave
  
  Save the result of the preprocessing steps (graph renumbering and partitioning) 
  on disk in *directory*. Then computation can be resumed in parallel by using <HIPS_Load>.

  To call this function, the graph of hte matrix must has been entered before by one of the HIPS graph function.
  Parameters: 
    id        - Solver instance identification number.  
    directory - Path to the directory where to save the preprocessing results.

  In Fortran, *STR_LEN* is the length of the string directory.

  Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.

  Fortran interface:
  >
  > SUBROUTINE HIPS_SETUPSAVE(ID, DIRECTORY, STR_LEN, IERROR)
  >   INTS,             INTENT(IN)  :: ID, STR_LEN
  >   CHARACTER(len=*), INTENT(IN)  :: DIRECTORY
  >   INTS,             INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_SETUPSAVE
*/
INTS HIPS_SetupSave(INTS id, char* directory);

MULTIPLE_TYPE_DEFINE(INTS, HIPS_SetupSave, 
		     (INTS id, char *directory));

/*
  Function: HIPS_SetupLoad
  
  Loads preprocessing result from disk, into *directory*, 
  where it had been saved by <HIPS_Save>.

  Parameters: 
    id        - Problem identification number.  
    directory - Path to the directory where to load the solver 
                preprocessing data.

  In Fortran, *STR_LEN* is the length of the string directory.


  Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.

  Fortran interface:
  >
  > SUBROUTINE HIPS_SETUPLOAD(ID, DIRECTORY, STR_LEN, IERROR)
  >   INTS,             INTENT(IN)  :: ID, STR_LEN
  >   CHARACTER(len=*), INTENT(IN)  :: DIRECTORY
  >   INTS,             INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_SETUPLOAD
*/
INTS HIPS_SetupLoad(INTS id, char* directory);

MULTIPLE_TYPE_DEFINE(INTS, HIPS_SetupLoad, 
		     (INTS id, char *directory));

/*
  Function: HIPS_LocalMatricesSave

  This function saves for each processor its local matrix.
  The local matrix is generated from the global one. 
  Each matrix is save in a different file.
  
  
  Parameters: 
    id        - Solver instance identification number.  
    nproc     - number of processors that will run the solver.
    n         - Number of columns.
    rowptr    - Index of the first element of each rows in *COLS* and 
                *values* array.
    cols      - Row number array.
    values    - values array.

    directory - Path to the directory where to save the local matrices.

  In Fortran, *STR_LEN* is the length of the string directory.

  Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.



  Fortran interface:
  >
  > SUBROUTINE HIPS_LOCALMATRICESSAVE(ID, NPROC, N, ROWPTR, COLS, VALUES, DIRECTORY, STR_LEN, IERROR)
  >   INTS,             INTENT(IN)  :: ID, NPROC, N, STR_LEN
  >   INTL, DIMENSION(0), INTENT(IN)  :: ROWPTR
  >   INTS, DIMENSION(0), INTENT(IN)  :: COLS
  >   COEF, DIMENSION(0), INTENT(IN)  :: VALUES
  >   CHARACTER(len=*), INTENT(IN)  :: DIRECTORY
  >   INTS,             INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_LOCALMATRICESSAVE
*/
INTS HIPS_LocalMatricesSave(INTS id, INTS nproc, INTS n, INTL *rowptr, INTS *col, COEF *values, char *directory);

MULTIPLE_TYPE_DEFINE_F(INTS, HIPS_LocalMatricesSave, 
		       (INTS id, INTS nproc, INTS n, INTL *rowptr, INTS *col, SCOEF *values, char *directory),
		       (INTS id, INTS nproc, INTS n, INTL *rowptr, INTS *col, DCOEF *values, char *directory),
		       (INTS id, INTS nproc, INTS n, INTL *rowptr, INTS *col, CCOEF *values, char *directory),
		       (INTS id, INTS nproc, INTS n, INTL *rowptr, INTS *col, ZCOEF *values, char *directory));


/*
  Function: HIPS_LocalMatriceLoad

  This function loads the local matrix of a processor from disk. The local matrix must have 
  been generated by <HIPS_LocalMatricesSave>.
  
  Parameters: 
    id        - Problem identification number.  
    sym       - Return value : 0 unsymetric matrix, 1 symmetric matrix
    n         - Number of columns.
    rowptr    - Index of the first element of each rows in *COLS* and 
                *values* array.
    cols      - Row number array.
    values    - values array.

    directory - Path to the directory where to save the local matrices.

  
  In C the arrays rowptr and cols are allocated by the function. 
  In Fortran, these array should have been allocated to a sufficient size.
  In Fortran, *STR_LEN* is the length of the string directory.

  Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.




  Fortran interface:
  >
  > SUBROUTINE HIPS_LOCALMATRICELOAD(ID, SYM, N, ROWPTR, COLS, VALUES, DIRECTORY, STR_LEN, IERROR)
  >   INTS,             INTENT(IN)  :: ID, STR_LEN 
  >   INTS,             INTENT(OUT)  :: SYM, N 
  >   INTL, DIMENSION(0), INTENT(OUT)  :: ROWPTR
  >   INTS, DIMENSION(0), INTENT(OUT)  :: COLS
  >   COEF, DIMENSION(0), INTENT(OUT)  :: VALUES
  >   CHARACTER(len=*), INTENT(IN)  :: DIRECTORY
  >   INTS,             INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_LOCALMATRICELOAD
*/
INTS HIPS_LocalMatriceLoad(INTS id, INTS *sym, INTS* n, INTL **rowptr, INTS **cols, COEF **values, char *sfile_path);
MULTIPLE_TYPE_DEFINE_F(INTS, HIPS_LocalMatriceLoad,
		       (INTS id, INTS *sym, INTS* n, INTL **rowptr, INTS **cols, SCOEF **values, char *sfile_path),
		       (INTS id, INTS *sym, INTS* n, INTL **rowptr, INTS **cols, DCOEF **values, char *sfile_path),
		       (INTS id, INTS *sym, INTS* n, INTL **rowptr, INTS **cols, CCOEF **values, char *sfile_path),
		       (INTS id, INTS *sym, INTS* n, INTL **rowptr, INTS **cols, ZCOEF **values, char *sfile_path));





/* 
   Group: Get the internal HIPS distribution
   
*/
/*
  Function: HIPS_GetLocalNodeNbr
  
  This function gives the number of local nodes in the HIPS distribution. 
  Be aware that the HIPS node partition uses an overlap (one node wide) between each domain.

  The graph is required to get the distribution. It must has been entered by one of the HIPS graph 
  input function.

  Parameters:
    id        - Problem identification number.  
    nodenbr   - Number of local nodes.

  Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.

  Fortran interface:
  >
  > SUBROUTINE HIPS_GETLOCALNODENBR(ID, NODENBR, IERROR)
  >   INTS, INTENT(IN)  :: ID
  >   INTS, INTENT(OUT) :: NODENBR
  >   INTS, INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_GETLOCALNODENBR 
*/
INTS HIPS_GetLocalNodeNbr(INTS id, INTS *nodenbr);
MULTIPLE_TYPE_DEFINE(INTS, HIPS_GetLocalNodeNbr, (INTS id, INTS *nodenbr));
/*
  Function: HIPS_GetLocalNodeList
  
  This function gives the local node list corresponding to the HIPS domain partition.

  The array *nodelist* must have been allocated with a size of at least *nodenbr* 
  (obtained by the function <HIPS_GetLocalNodeNbr>).

  Parameters:
    id        - Problem identification number.  
    nodelist  - Array where to store the list of local nodes.

  Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.


  Fortran interface:
  >
  > SUBROUTINE HIPS_GETLOCALNODELIST(ID, NODELIST, IERROR)
  >   INTS,               INTENT(IN)  :: ID      
  >   ! Warning : 0 is not the size of the array.
  >   ! Writing DIMENSION(:) does not work with
  >   ! the C function call (fortran send the array size?)
  >   INTS, DIMENSION(0), INTENT(OUT) :: NODELIST
  >   INTS,               INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_GETLOCALNODELIST
*/
INTS HIPS_GetLocalNodeList(INTS id, INTS *nodelist);
MULTIPLE_TYPE_DEFINE(INTS, HIPS_GetLocalNodeList, (INTS id, INTS *nodelist));

/*
  Function: HIPS_GetLocalUnknownNbr
  
  This function gives the number of local unknowns in the HIPS distribution. 
  Be aware that the HIPS unknowns partition uses an overlap between each domain.
  If the *HIPS_DOF* integer option has been set to 1 (default value) then <HIPS_GetLocalUnknownNbr> is equivalent 
  to  <HIPS_GetLocalNodeNbr>.

  The graph is required to get the distribution. It must has been entered by one of the HIPS graph 
  input function.



  Parameters:
    id            - Problem identification number.  
    unkownnbr     - Number of local unknowns.

  Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.



  Fortran interface:
  >
  > SUBROUTINE HIPS_GETLOCALUNKOWNNBR(ID, UNKOWNNBR, IERROR)
  >   INTS, INTENT(IN)  :: ID
  >   INTS, INTENT(OUT) :: UNKOWNNBR
  >   INTS, INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_GETLOCALUNKOWNNBR 
*/
INTS HIPS_GetLocalUnknownNbr(INTS id, INTS *unkownnbr);
MULTIPLE_TYPE_DEFINE(INTS, HIPS_GetLocalUnknownNbr, (INTS id, INTS *unkownnbr));

/*
  Function: HIPS_GetLocalUnknownList
   
  This function gives the local unknown list corresponding to the HIPS domain partition.

  The array *unknownlist* must have been allocated with a size of at least *unknownnbr* 
  (obtained by the function <HIPS_GetLocalUnknownNbr>).
  If the *HIPS_DOF* integer option has been set to 1 (default value) then <HIPS_GetLocalUnknownList> is equivalent 
  to  <HIPS_GetLocalNodeList>.



  Parameters:
    id          - Problem identification number.  
    unkownlist  - Array where to store the list of local unknowns.

  Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.


  Fortran interface:
  >
  > SUBROUTINE HIPS_GETLOCALUNKOWNLIST(ID, UNKOWNLIST, IERROR)
  >   INTS,               INTENT(IN)  :: ID      
  >   INTS, DIMENSION(0), INTENT(OUT) :: UNKOWNLIST
  >   INTS,               INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_GETLOCALUNKOWNLIST
*/
INTS HIPS_GetLocalUnknownList(INTS id, INTS *unknownlist);
MULTIPLE_TYPE_DEFINE(INTS, HIPS_GetLocalUnknownList, (INTS id, INTS *unkownlist));


/*
  Function: HIPS_SetPartition
  Sets a partition defined by the user. It can only be used for the recursive ITERATIVE strategy.

  Parameters: 
    id     - Problem identification number.  
    ndom   - Number of domains
    mapptr - Array of indexes for domain in mapptr : mapp(mapptr(i):mapptr(i+1)-1) contains the nodes 
             domain i
    mapp   - Array that contains the node lists for each domain.

  Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.


  Fortran interface:
  >
  > SUBROUTINE HIPS_SETPARTITION(ID, NDOM, MAPPTR, MAPP, IERROR)
  >   INTS, INTENT(IN)    :: ID, NDOM
  >   INTS, DIMENSION(0), INTENT(IN)  ::  MAPPTR
  >   INTS, DIMENSION(0), INTENT(IN)  ::  MAPP
  >   INTS, INTENT(OUT)   :: IERROR
  > END SUBROUTINE HIPS_SETPARTITION
 */
INTS HIPS_SetPartition(INTS id, INTS ndom, INTS *mapptr, INTS *mapp);
MULTIPLE_TYPE_DEFINE(INTS, HIPS_SetPartition, (INTS id, INTS ndom, INTS * mapptr, INTS * mapp));




/*
  Function: HIPS_GraphPartition
  Compute a partition of a graph with or without overlap between the partition. The overlap is computed using some heuristic that try to minimizes its size : it tries to compute an overlap of one node (or unknown).
  When no overlap is needed this function is merely a call to METIS or SCOTCH (graph partitioner).

  Parameters: 
    ndom    - Number of domains
    overlap - 0 : no overlap in the partition , 1 : overlap as small as possible
    numflag - 0 : numbering start from 0 (like in C), 1: numbering start from 1 (like in Fortran). This concern the inputs as well as the ouput of the function.
    n       - number of vertice in the graph.
    rowptr  - Index of the first element of each row in *COLS* array.
    cols    - Global column numbers array.
    sym     - 0 : the graph is not symmetric (it will be symmetrize in intern). 1 : the graph is symmetric (better is will be used as this inside the function).
    mapptr  - Array of indexes for domain in mapptr : mapp(mapptr(i):mapptr(i+1)-1) contains the nodes 
             domain i
    mapp    - Array that contains the node lists for each domain.

  Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.


  Fortran interface:
     Not provided.
*/
INTS HIPS_GraphPartition(INTS ndom, INTS overlap, INTS numflag, INTS n, INTL *rowptr, INTS *cols, INTS sym, INTS **mapptr, INTS **mapp);

MULTIPLE_TYPE_DEFINE(INTS, HIPS_GraphPartition, 
		     (INTS ndom, INTS overlap, INTS numflag, INTS n, INTL *rowptr, INTS *cols, INTS sym, INTS **mapptr, INTS **mapp));
INTS  HIPS_GetLocalDomainNbr(INTS id, INTS *domnbr, INTS *listsize);
MULTIPLE_TYPE_DEFINE(INTS, HIPS_GetLocalDomainNbr, (INTS id, INTS *domnbr, INTS *listsize));
INTS  HIPS_GetLocalDomainList(INTS id, INTS *mapptr, INTS *mapp);
MULTIPLE_TYPE_DEFINE(INTS, HIPS_GetLocalDomainList, (INTS id, INTS *mapptr, INTS *mapp));





/* 
   Group: Enter the matrix coefficients
   
*/
/*
  Function: HIPS_AssemblyBegin

  Begin the assembly of the matrix coefficient.
  The assembly can not begin if the matrix graph has not been entered.
  
  Several assembly sequences can be performed on the same matrix.

  *mode* shouldn't be *HIPS_ASSEMBLY_RESPECT* if neither 
  <HIPS_GetLocalNodeList> nor <HIPS_GetLocalUnknownList> has been called.

  Parameters: 
    id      - Problem identification number.  
    coefnbr - Number of coefficients that will be entered in this assembly loop (a node is equivalent to dof*dof coefficients).
    op      - Operation to perform on the matrix problem : overwrite values of the existing matrix or add new entries to the former ones
              (see <HIPS_ASSEMBLY_OP>).
    op2     - Operation to perform if several entries are entered for a same (i,j) location on different processor
              (see <HIPS_ASSEMBLY_OP>). This allows to control the operation to do for overlapped part of the matrix.
    mode    - Indicates if the user ensures he will respect the HIPS internal unknowns distribution 
              (see <HIPS_ASSEMBLY_MODE>).
    sym     - Indicates if the user will give coefficients only in the lower triangular part
              and that HIPS must consider that these coefficients are also the same in the upper triangular part.
	      For a symmetric problem (indicated by the *HIPS_SYMMETRIC* integer option) any coefficient Aij = Aji 
	      so you should enter only the lower of upper triangular part of the matrix and set *sym* to 1.

  Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.

  Fortran interface:
  >
  > SUBROUTINE HIPS_ASSEMBLYBEGIN(ID, COEFNBR, OP, OP2, MODE, SYM, IERROR)
  >   INTS,      INTENT(IN)  :: ID, OP, OP2, MODE, SYM
  >   INTL,      INTENT(IN)  :: COEFNBR
  >   INTS,      INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_ASSEMBLYBEGIN
*/ 
INTS HIPS_AssemblyBegin(INTS id, INTL nnz, INTS op, INTS op2, INTS mode, INTS symmetric);
MULTIPLE_TYPE_DEFINE(INTS, HIPS_AssemblyBegin,
		     (INTS id, INTL nnz, INTS op, INTS op2, INTS mode, INTS symmetric));


/*
  Function: HIPS_AssemblySetValue

  Set a coefficient value in the matrix.
  
   <HIPS_AssemblyBegin> must have been called before.

  Parameters: 
    id      - Problem identification number.  
    row     - Row index (global numbering) of the coefficient.
    col     - Column index (global numbering) of the coefficient.
    value   - Value of the coefficient.

  Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.

  Fortran interface:
  >
  > SUBROUTINE HIPS_ASSEMBLYSETVALUE(ID, ROW, COL, VALUE, IERROR)
  >   INTS,      INTENT(IN)  :: ID, ROW, COL
  >   COEF,      INTENT(IN)  :: VALUE
  >   INTS,      INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_ASSEMBLYSETVALUE
*/ 
INTS HIPS_AssemblySetValue(INTS id, INTS row, INTS col, COEF value);
MULTIPLE_TYPE_DEFINE_F(INTS, HIPS_AssemblySetValue, 
		       (INTS id, INTS row, INTS col, SCOEF value),
		       (INTS id, INTS row, INTS col, DCOEF value),
		       (INTS id, INTS row, INTS col, CCOEF value),
		       (INTS id, INTS row, INTS col, ZCOEF value));


/*
  Function: HIPS_AssemblySetNodeValues

  Set coefficients  of a dense block corresponding to the edge 
  (i, j) in the node graph. 
  This function is useful only when the dof > 1 (see the *HIPS_DOF* integer parameter) 
  otherwise it is equivalent to <HIPS_AssemblySetValue>.
  Indeed each (i, j) entry of the node graph correspond to a (dof, dof) dense block in
  the linear system. The storage of the block is made by columns by columns in the vector 
  *values*.
  
  
  <HIPS_AssemblyBegin> must have been called before.

  Parameters: 
    id      - Problem identification number.  
    row     - Row index (global numbering) of the node.
    col     - Column index (global numbering) of the node.
    values  - Values of the dense block coefficient stored by columns.

  Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.
 

  Fortran interface:
  >
  > SUBROUTINE HIPS_ASSEMBLYSETNODEVALUES(ID, ROW, COL, VALUES, IERROR)
  >   INTS,               INTENT(IN)  :: ID, ROW, COL
  >   COEF, DIMENSION(0), INTENT(IN)  :: VALUES
  >   INTS,               INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_ASSEMBLYSETNODEVALUES
*/ 
INTS HIPS_AssemblySetNodeValues(INTS id, INTS row, INTS col, COEF *values);
MULTIPLE_TYPE_DEFINE_F(INTS, HIPS_AssemblySetNodeValues,
		       (INTS id, INTS row, INTS col, SCOEF *values),
		       (INTS id, INTS row, INTS col, DCOEF *values),
		       (INTS id, INTS row, INTS col, CCOEF *values),
		       (INTS id, INTS row, INTS col, ZCOEF *values));


/*
  Function: HIPS_AssemblySetBlockValues

  Set coefficients value for a dense submatrix (rowlist, colist).
  
  
  Typically, this function is to be used to enter elementary matrix arising from PDE discretization.
  
   <HIPS_AssemblyBegin> must have been called before.

  Parameters: 
    id     - Problem identification number.  
    nrow    - Number of rows in the dense matrix.
    rowlist - List of row indices (global ordering).
    ncol    - Number of columns in the dense matrix.
    collist - List of column indices (global numbering).
    values  - Values array, stored by column (Fortran style)
    
  Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.

  Fortran interface:
  >
  > SUBROUTINE HIPS_ASSEMBLYSETBLOCKVALUES(ID, NROW, ROWLIST, &
  >                                 & NCOL, COLLIST, VALUES, IERROR)
  >   INTS,               INTENT(IN)  :: ID, NROW, NCOL
  >   INTS, DIMENSION(0), INTENT(IN)  :: ROWLIST
  >   INTS, DIMENSION(0), INTENT(IN)  :: COLLIST
  >   COEF, DIMENSION(0), INTENT(IN)  :: VALUES
  >   INTS,               INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_ASSEMBLYSETBLOCKVALUES
*/ 
INTS HIPS_AssemblySetBlockValues(INTS id, INTS nrow, INTS *rowlist, INTS ncol, INTS *collist, COEF *values);
MULTIPLE_TYPE_DEFINE_F(INTS, HIPS_AssemblySetBlockValues,
		       (INTS id, INTS nrow, INTS *rowlist, INTS ncol, INTS *collist, SCOEF *values),
		       (INTS id, INTS nrow, INTS *rowlist, INTS ncol, INTS *collist, DCOEF *values),
		       (INTS id, INTS nrow, INTS *rowlist, INTS ncol, INTS *collist, CCOEF *values),
		       (INTS id, INTS nrow, INTS *rowlist, INTS ncol, INTS *collist, ZCOEF *values));

/*
  Function: HIPS_AssemblyEnd

  End an assembly loop.

   <HIPS_AssemblyBegin> must have been called before.

  Parameters: 
    id     - Problem identification number.


  Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.

  Fortran interface:
  >
  > SUBROUTINE HIPS_ASSEMBLYEND(ID, IERROR)
  >   INTS,      INTENT(IN)  :: ID
  >   INTS,      INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_ASSEMBLYEND
*/ 
INTS HIPS_AssemblyEnd(INTS id);
MULTIPLE_TYPE_DEFINE(INTS, HIPS_AssemblyEnd, (INTS id));

/*
  Function: HIPS_MatrixReset

  Reset the matrix structure.

  Parameters: 
    id     - Problem identification number.

  Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.

  Fortran interface:
  >
  > SUBROUTINE HIPS_MATRIXRESET(ID, IERROR)
  >   INTS,      INTENT(IN)  :: ID
  >   INTS,      INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_MATRIXRESET
*/ 
INTS HIPS_MatrixReset(INTS id);
MULTIPLE_TYPE_DEFINE(INTS, HIPS_MatrixReset,(INTS id));

/*
  Function: HIPS_FreePrecond

  Free the preconditioner matrices.

  Parameters: 
    id     - Problem identification number.

  Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.

  Fortran interface:
  >
  > SUBROUTINE HIPS_FREEPRECOND(ID, IERROR)
  >   INTS,      INTENT(IN)  :: ID
  >   INTS,      INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_FREEPRECOND
*/ 
INTS HIPS_FreePrecond(INTS id);
MULTIPLE_TYPE_DEFINE(INTS, HIPS_FreePrecond,(INTS id));


/*
  Function: HIPS_MatrixLocalCSR

  Add a submatrix in  Compress Sparse Row matrix to the matrix.
  The submatrix M corresponds to a matrix (unknownlist, unknownlist) in CSR format
  where unknownlist is a subset of the list of unknowns given by the function *HIPS_GetLocalUnknownList*.
  The submatrix is locally numbered : i.e. M(i, i) is to be added in A(nodelist, nodelist).
  

  IMPORTANT : the indices corresponds to the unknwon numbering, i.e. if you use HIPS_DOF > 1, you have to expand the
  matrix in unknwon indices.

  Parameters: 
    id          - Problem identification number.
    ln          - Dimension of the local matrix
    unknownlist - List of the local unknown in global numbering 
    lrowptr     - Index of the first element of each row in *LCOLS* and *VALUES* arrays.
    lcols       - Local column indice array.
    values      - values array.
    op          - Operation to perform on the matrix problem : overwrite values of the existing matrix or add new entries to the former ones (see <HIPS_ASSEMBLY_OP>).
    op2         - Operation to perform if several entries are entered for a same (i,j) location on different processor
              (see <HIPS_ASSEMBLY_OP>). This allows to control the operation to do for overlapped part of the matrix.
    sym         - Indicates if the user will give coefficients only in the lower triangular part
              and that HIPS must consider that these coefficients are also the same in the upper triangular part.
	      For a symmetric problem (indicated by the *HIPS_SYMMETRIC* integer option) any coefficient Aij = Aji 
	      so you should enter only the lower of upper triangular part of the matrix and set *sym* to 1.
	      

  Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.


  Fortran interface:
  >
  > SUBROUTINE HIPS_MATRIXLOCALCSR(ID, LN, UNKNOWNLIST, LROWPTR, LCOLS, VALUES, &
  >                                & OP, OP2, SYM, IERROR)
  >   INTS,               INTENT(IN)  :: ID, LN, OP, OP2, SYM
  >   INTS, DIMENSION(0), INTENT(IN)  :: UNKNOWNLIST, LCOLS
  >   INTL, DIMENSION(0), INTENT(IN)  :: LROWPTR
  >   COEF, DIMENSION(0), INTENT(IN)  :: VALUES
  >   INTS,               INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_MATRIXLOCALCSR
*/ 
INTS HIPS_MatrixLocalCSR(INTS id, INTS ln, INTS *unknownlist, INTL *lrowptr, INTS *lcols, COEF *values,  INTS op, INTS op2, INTS sym);
MULTIPLE_TYPE_DEFINE_F(INTS, HIPS_MatrixLocalCSR,
		       (INTS id, INTS ln, INTS *unknownlist, INTL *lrowptr, INTS *lcols, SCOEF *values,  INTS op, INTS op2, INTS sym),
		       (INTS id, INTS ln, INTS *unknownlist, INTL *lrowptr, INTS *lcols, DCOEF *values,  INTS op, INTS op2, INTS sym),
		       (INTS id, INTS ln, INTS *unknownlist, INTL *lrowptr, INTS *lcols, CCOEF *values,  INTS op, INTS op2, INTS sym),
		       (INTS id, INTS ln, INTS *unknownlist, INTL *lrowptr, INTS *lcols, ZCOEF *values,  INTS op, INTS op2, INTS sym));



/*
  Function: HIPS_MatrixDistrCSR

  Add a distrbuted matrix in  Compress Sparse Row matrix to the matrix.
  Each processors has a set of row : the list of these rows (in global number) is in unknownlist.
  If the set of rows 


  IMPORTANT : the indices corresponds to the unknwon numbering, i.e. if you use HIPS_DOF > 1, you have to expand the
  matrix in unknwon indices.

  Parameters: 
    id          - Problem identification number.
    ln          - Dimension of the local matrix
    unknownlist - List of the local row in global numerbing : cols(lrowptr(i):lrowptr(i+1) are the columns indices of coefficients in row unknownlist(i)) 
    lrowptr     - Index of the first element of each row in *LCOLS* and *VALUES* arrays.
    lcols       - Local column indice array.
    values      - values array.
    op          - Operation to perform on the matrix problem : overwrite values of the existing matrix or add new entries to the former ones (see <HIPS_ASSEMBLY_OP>).
    op2         - Operation to perform if several entries are entered for a same (i,j) location on different processor
                (see <HIPS_ASSEMBLY_OP>). This allows to control the operation to do for overlapped part of the matrix.
    mode        - Indicates if the user ensures he will respect the HIPS internal unknowns distribution 
                (see <HIPS_ASSEMBLY_MODE>). Be careful : if you choose HIPS_ASSEMBLY_RESPECT, then any coefficient that 
		is not in M(unknownlist1, unknownlist1) --where unknownlist1 is the HIPS internal distribution of the unknowns -- will be ignored. You can get the hips internal data distribution by using *HIPS_GetUnknownnbr* and *HIPS_GetUnknownlist*.
		
    sym         - Indicates if the user will give coefficients only in the lower triangular part
                and that HIPS must consider that these coefficients are also the same in the upper triangular part.
	        For a symmetric problem (indicated by the *HIPS_SYMMETRIC* integer option) any coefficient Aij = Aji 
	        so you should enter only the lower of upper triangular part of the matrix and set *sym* to 1.f

  Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.

  Fortran interface:
  >
  > SUBROUTINE HIPS_MATRIXDISTRCSR(ID, LN, UNKNOWNLIST, LROWPTR, COLS, VALUES, &
  >                                & OP, OP2, MODE, SYM, IERROR)
  >   INTS,               INTENT(IN)  :: ID, LN, OP, OP2, SYM, MODE
  >   INTS, DIMENSION(0), INTENT(IN)  :: UNKNOWNLIST, COLS
  >   INTL, DIMENSION(0), INTENT(IN)  :: LROWPTR
  >   COEF, DIMENSION(0), INTENT(IN)  :: VALUES
  >   INTS,               INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_MATRIXDISTRCSR
*/ 
INTS HIPS_MatrixDistrCSR(INTS id,  INTS ln, INTS *unknownlist, INTL *lrowptr, INTS *cols, COEF *values, INTS op, INTS op2, INTS mode, INTS sym);
MULTIPLE_TYPE_DEFINE_F(INTS, HIPS_MatrixDistrCSR,
		       (INTS id,  INTS ln, INTS *unknownlist, INTL *lrowptr, INTS *cols, SCOEF *values, INTS op, INTS op2, INTS mode, INTS sym),
		       (INTS id,  INTS ln, INTS *unknownlist, INTL *lrowptr, INTS *cols, DCOEF *values, INTS op, INTS op2, INTS mode, INTS sym),
		       (INTS id,  INTS ln, INTS *unknownlist, INTL *lrowptr, INTS *cols, CCOEF *values, INTS op, INTS op2, INTS mode, INTS sym),
		       (INTS id,  INTS ln, INTS *unknownlist, INTL *lrowptr, INTS *cols, ZCOEF *values, INTS op, INTS op2, INTS mode, INTS sym));

/*
  Function: HIPS_MatrixGlobalCSR

  Add the given global Compress Sparse Row matrix to the matrix.

  Parameters: 
    id      - Problem identification number.
    n       - Number of columns.
    rowptr  - Index of the first element of each row in *COLS* and 
              *values* array.
    cols    - Column number array.
    values  - values array.
    root    - Root processor for MPI communications.
    op      - Operation to perform on the matrix problem : overwrite values of the existing matrix or add new entries to the former ones
              (see <HIPS_ASSEMBLY_OP>).
    sym     - Indicates if the user will give coefficients only in the lower triangular part
              and that HIPS must consider that these coefficients are also the same in the upper triangular part.
	      For a symmetric problem (indicated by the *HIPS_SYMMETRIC* integer option) any coefficient Aij = Aji 
	      so you should enter only the lower of upper triangular part of the matrix and set *sym* to 1.
	      
  Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.

  Fortran interface:
  >
  > SUBROUTINE HIPS_MATRIXGLOBALCSR(ID, N, ROWPTR, COLS, VALUES, &
  >                                & ROOT, OP, SYM,  IERROR)
  >   INTS,               INTENT(IN)  :: ID, N, ROOT, OP, SYM
  >   INTL, DIMENSION(0), INTENT(IN)  :: ROWPTR
  >   INTS, DIMENSION(0), INTENT(IN)  :: COLS
  >   COEF, DIMENSION(0), INTENT(IN)  :: VALUES
  >   INTS,               INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_MATRIXGLOBALCSR
*/ 
INTS HIPS_MatrixGlobalCSR(INTS id, INTS n, INTL *rowptr, INTS *cols, COEF *values, INTS root, INTS op, INTS sym);
MULTIPLE_TYPE_DEFINE_F(INTS, HIPS_MatrixGlobalCSR,
		       (INTS id, INTS n, INTL *rowptr, INTS *cols, SCOEF *values, INTS root, INTS op, INTS sym),
		       (INTS id, INTS n, INTL *rowptr, INTS *cols, DCOEF *values, INTS root, INTS op, INTS sym),
		       (INTS id, INTS n, INTL *rowptr, INTS *cols, CCOEF *values, INTS root, INTS op, INTS sym),
		       (INTS id, INTS n, INTL *rowptr, INTS *cols, ZCOEF *values, INTS root, INTS op, INTS sym));





/*
  Function: HIPS_MatrixGlobalCSC

  Add the given global Compress Sparse Column matrix to the matrix.

  Parameters: 
    id     - Problem identification number.
    n       - Number of columns.
    colptr  - Index of the first element of each column in *ROWS* and 
              *values* array.
    rows    - Row number array.
    values  - values array.
    root    - Root processor for MPI communications.
    op      - Operation to perform on the matrix problem : overwrite values of the existing matrix or add new entries to the former ones
              (see <HIPS_ASSEMBLY_OP>).
    sym     - Indicates if the user will give coefficients only in the lower triangular part
              and that HIPS must consider that these coefficients are also the same in the upper triangular part.
	      For a symmetric problem (indicated by the *HIPS_SYMMETRIC* integer option) any coefficient Aij = Aji 
	      so you should enter only the lower of upper triangular part of the matrix and set *sym* to 1.
	      	      
 Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.

  Fortran interface:
  >
  > SUBROUTINE HIPS_MATRIXGLOBALCSC(ID, N, COLPTR, ROWS, &
  >                               & VALUES, ROOT, OP, SYM, IERROR)
  >   INTS,               INTENT(IN)  :: ID, N, ROOT, OP, SYM
  >   INTL, DIMENSION(0), INTENT(IN)  :: COLPTR
  >   INTS, DIMENSION(0), INTENT(IN)  :: ROWS
  >   COEF, DIMENSION(0), INTENT(IN)  :: VALUES
  >   INTS,               INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_MATRIXGLOBALCSC
*/ 
INTS HIPS_MatrixGlobalCSC(INTS id, INTS n, INTL *colptr, INTS *rows, 
			   COEF *values, INTS root, INTS op, INTS sym);
MULTIPLE_TYPE_DEFINE_F(INTS, HIPS_MatrixGlobalCSC,
		       (INTS id, INTS n, INTL *colptr, INTS *rows, 
			SCOEF *values, INTS root, INTS op, INTS sym),
		       (INTS id, INTS n, INTL *colptr, INTS *rows, 
			DCOEF *values, INTS root, INTS op, INTS sym),
		       (INTS id, INTS n, INTL *colptr, INTS *rows, 
			CCOEF *values, INTS root, INTS op, INTS sym),
		       (INTS id, INTS n, INTL *colptr, INTS *rows, 
			ZCOEF *values, INTS root, INTS op, INTS sym));

/*
  Function: HIPS_MatrixGlobalIJV

  Add the given global Compress Sparse Column matrix to the matrix.

  Parameters: 
    id     - Problem identification number.
    n       - Number of edges.
    nnz     - Number of non zeros.
    rows    - Global row number array.
    cols    - Global column number array.
    values  - values array.
    root    - Root processor for MPI communications.
    op      - Operation to perform on the matrix problem : overwrite values of the existing matrix or add new entries to the former ones
              (see <HIPS_ASSEMBLY_OP>).
    sym     - Indicates if the user will give coefficients only in the lower triangular part
              and that HIPS must consider that these coefficients are also the same in the upper triangular part.
	      For a symmetric problem (indicated by the *HIPS_SYMMETRIC* integer option) any coefficient Aij = Aji 
	      so you should enter only the lower of upper triangular part of the matrix and set *sym* to 1.
	      

  Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.


  Fortran interface:
  >
  > SUBROUTINE HIPS_MATRIXGLOBALIJV(ID, N, NNZ, ROWS, COLS, VALUES, &
  >                                & ROOT, OP, SYM, IERROR)
  >   INTS,               INTENT(IN)  :: ID, ROOT, OP, SYM, N
  >   INTL,               INTENT(IN)  :: NNZ
  >   INTS, DIMENSION(0), INTENT(IN)  :: ROWS, COLS
  >   COEF, DIMENSION(0), INTENT(IN)  :: VALUES
  >   INTS,               INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_MATRIXGLOBALIJV
*/ 
INTS HIPS_MatrixGlobalIJV(INTS id, INTS n, INTL nnz, INTS *rows, INTS *cols, 
			   COEF *values, INTS root, INTS op, INTS sym);
MULTIPLE_TYPE_DEFINE_F(INTS, HIPS_MatrixGlobalIJV,
		       (INTS id, INTS n, INTL nnz, INTS *rows, INTS *cols, 
			SCOEF *values, INTS root, INTS op, INTS sym),
		       (INTS id, INTS n, INTL nnz, INTS *rows, INTS *cols, 
			DCOEF *values, INTS root, INTS op, INTS sym),
		       (INTS id, INTS n, INTL nnz, INTS *rows, INTS *cols, 
			CCOEF *values, INTS root, INTS op, INTS sym),
		       (INTS id, INTS n, INTL nnz, INTS *rows, INTS *cols, 
			ZCOEF *values, INTS root, INTS op, INTS sym));


INTS HIPS_SetSubmatrixCoef(INTS id, INTS op, INTS op2, INTS n, INTL *ia, INTS *ja, COEF *a, 
			   INTS sym_matrix, INTS ln, INTS *nodelist);
MULTIPLE_TYPE_DEFINE_F(INTS, HIPS_SetSubmatrixCoef,
		       (INTS id, INTS op, INTS op2, INTS n, INTL *ia, INTS *ja, SCOEF *a, 
			INTS sym_matrix, INTS ln, INTS *nodelist),
		       (INTS id, INTS op, INTS op2, INTS n, INTL *ia, INTS *ja, DCOEF *a, 
			INTS sym_matrix, INTS ln, INTS *nodelist),
		       (INTS id, INTS op, INTS op2, INTS n, INTL *ia, INTS *ja, CCOEF *a, 
			INTS sym_matrix, INTS ln, INTS *nodelist),
		       (INTS id, INTS op, INTS op2, INTS n, INTL *ia, INTS *ja, ZCOEF *a, 
			INTS sym_matrix, INTS ln, INTS *nodelist));

/* 
   Group: Fill the right-hand-side member
   
*/
/*
  Function: HIPS_SetGlobalRHS

  Set the right-hand-side member in global mode.

  Parameters: 
    id     - Problem identification number.
    b      - Array of size global column number which correspond to the 
              right-hand-side member.
    op     - Operation on the right hand side : overwrite values of the existing rhs or add new entries to the former ones
              (see <HIPS_ASSEMBLY_OP>).
    root   - Indicates which processor enter the global right-hand-side member,

 Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.

  Fortran interface:
  >
  > SUBROUTINE HIPS_SETGLOBALRHS(ID, B, ROOT, OP, IERROR)
  >   INTS,               INTENT(IN)  :: ID, ROOT, OP
  >   COEF, DIMENSION(0), INTENT(IN)  :: B
  >   INTS,               INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_SETGLOBALRHS
*/
INTS HIPS_SetGlobalRHS(INTS id, COEF *b, INTS proc_root, INTS op);
MULTIPLE_TYPE_DEFINE_F(INTS, HIPS_SetGlobalRHS,
		       (INTS id, SCOEF *b, INTS proc_root, INTS op),
		       (INTS id, DCOEF *b, INTS proc_root, INTS op),
		       (INTS id, CCOEF *b, INTS proc_root, INTS op),
		       (INTS id, ZCOEF *b, INTS proc_root, INTS op));

/*
  Function: HIPS_SetLocalRHS

  Set the right-hand-side member in local mode.

  Parameters: 
    id      - Problem identification number.
    b       - Array of size local column number which correspond to the 
              right-hand-side member.
    op      - overwrite values of the existing matrix or add new entries to the former ones
              (see <HIPS_ASSEMBLY_OP>).	  
    op2     - Operation to perform if several entries are entered for a same vector component on different processor
              (see <HIPS_ASSEMBLY_OP>). This allows to control the operation to do for overlapped part of the rhs.    
 Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.

  Fortran interface:
  >
  > SUBROUTINE HIPS_SETLOCALRHS(ID, B, OP, OP2,  IERROR)
  >   INTS,               INTENT(IN)  :: ID, OP, OP2
  >   COEF, DIMENSION(0), INTENT(IN)  :: B
  >   INTS,               INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_SETLOCALRHS
*/
INTS HIPS_SetLocalRHS(INTS id, COEF *b, INTS op, INTS op2); 
MULTIPLE_TYPE_DEFINE_F(INTS, HIPS_SetLocalRHS,
		       (INTS id, SCOEF *b, INTS op, INTS op2),
		       (INTS id, DCOEF *b, INTS op, INTS op2),
		       (INTS id, CCOEF *b, INTS op, INTS op2),
		       (INTS id, ZCOEF *b, INTS op, INTS op2)); 

/*
  Function: HIPS_SetRHS

  Set the right-hand-side member, giving the list of 
  coefficient that we set.

  *mode* shouldn't be *HIPS_ASSEMBLY_RESPECT* if neither 
  <HIPS_GetLocalNodeList> nor <HIPS_GetLocalUnknownList> has been called.

  Parameters: 
    id       - Problem identification number.
    n        - Number of coefficients to set.
    coefsidx - List of global index of the coefficients to set.
    B        - Array of coefficients values. 
    op      - overwrite values of the existing matrix or add new entries to the former ones
              (see <HIPS_ASSEMBLY_OP>).	  
    op2     - Operation to perform if several entries are entered for a same vector component on different processor
              (see <HIPS_ASSEMBLY_OP>). This allows to control the operation to do for overlapped part of the rhs.   
    mode     - Indicates if user ensure he will respect solvers distribution 
               (see <HIPS_ASSEMBLY_MODE>).
 Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.


  Fortran interface:
  >
  > SUBROUTINE HIPS_SETRHS(ID, N, COEFSIDX, B, OP, OP2, MODE, IERROR)
  >   INTS,               INTENT(IN)  :: ID, N, OP, OP2, MODE
  >   INTS, DIMENSION(0), INTENT(IN)  :: COEFSIDX
  >   COEF, DIMENSION(0), INTENT(IN)  :: B
  >   INTS,               INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_SETRHS
*/
INTS HIPS_SetRHS(INTS id, INTS unknownnbr, INTS *unknownlist, COEF *b, INTS op, INTS op2, INTS mode); 
MULTIPLE_TYPE_DEFINE_F(INTS, HIPS_SetRHS,
		       (INTS id, INTS unknownnbr, INTS *unknownlist, SCOEF *b, INTS op, INTS op2, INTS mode),
		       (INTS id, INTS unknownnbr, INTS *unknownlist, DCOEF *b, INTS op, INTS op2, INTS mode),
		       (INTS id, INTS unknownnbr, INTS *unknownlist, CCOEF *b, INTS op, INTS op2, INTS mode),
		       (INTS id, INTS unknownnbr, INTS *unknownlist, ZCOEF *b, INTS op, INTS op2, INTS mode)); 

/*
  Function: HIPS_RHSReset

  Reset the right-hand-side.

  Parameters: 
    id     - Problem identification number.
 Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.

  Fortran interface:
  >
  > SUBROUTINE HIPS_RHSRESET(ID, IERROR)
  >   INTS,      INTENT(IN)  :: ID
  >   INTS,      INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_RHSRESET
*/ 
INTS HIPS_RHSReset(INTS id);
MULTIPLE_TYPE_DEFINE(INTS, HIPS_RHSReset,(INTS id));

/* 
   Group: Get  the solution
   
*/

/*
  Function: HIPS_GetGlobalSolution

  Perform Factorization and Solve, if needed, 
  and then fill the global solution in *x*.

  Parameters: 
    id     - Problem identification number.
    x       - Array of size global column number which will contain
              the solution
    root    - Indicates which processor will have the solution 
              at the end of the call, -1 for all.
 Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.

  Fortran interface:
  >
  > SUBROUTINE HIPS_GETGLOBALSOLUTION(ID, X, ROOT, IERROR)
  >   INTS,               INTENT(IN)  :: ID, ROOT
  >   COEF, DIMENSION(0), INTENT(OUT) :: X
  >   INTS,               INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_GETGLOBALSOLUTION
*/
INTS HIPS_GetGlobalSolution(INTS id, COEF *x, INTS root); 
MULTIPLE_TYPE_DEFINE_F(INTS, HIPS_GetGlobalSolution,
		       (INTS id, SCOEF *x, INTS root),
		       (INTS id, DCOEF *x, INTS root),
		       (INTS id, CCOEF *x, INTS root),
		       (INTS id, ZCOEF *x, INTS root)); 

/*
  Function: HIPS_GetLocalSolution

  Perform Factorization and Solve, if needed, 
  and then fill the local solution in *x*.

  Parameters: 
    id     - Problem identification number.
    x      - Array that will contain the local solution corresponding
              to the HIPS unknown distribution (that can be obtained by
	      HIPS_GetUnknownList).
 Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.

  Fortran interface:
  >
  > SUBROUTINE HIPS_GETLOCALSOLUTION(ID, X, IERROR)
  >   INTS,               INTENT(IN)  :: ID
  >   COEF, DIMENSION(0), INTENT(OUT) :: X
  >   INTS,               INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_GETLOCALSOLUTION
*/
INTS HIPS_GetLocalSolution(INTS id, COEF *x);
MULTIPLE_TYPE_DEFINE_F(INTS, HIPS_GetLocalSolution,
		       (INTS id, SCOEF *x),
		       (INTS id, DCOEF *x),
		       (INTS id, CCOEF *x),
		       (INTS id, ZCOEF *x));

/*
  Function: HIPS_GetSolution

  Perform Factorization and Solve, if needed, 
  and then fill the solution in *x* followin the given
  index list.

  Parameters: 
    id       - Problem identification number.
    n        - Number of coefficients user wants to get.
    coefsidx - List of the coefficients user wants to get.
    x        - Array that contain the local part of solution in return.
    mode     - Indicates if the user is sure to respect the distribution.

  Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.

  Fortran interface:
  >
  > SUBROUTINE HIPS_GETSOLUTION(ID, N, COEFSIDX, X, MODE, IERROR)
  >   INTS,               INTENT(IN)  :: ID, MODE, N
  >   INTS, DIMENSION(0), INTENT(IN)  :: COEFSIDX
  >   COEF, DIMENSION(0), INTENT(OUT) :: X
  >   INTS,               INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_GETSOLUTION
*/
INTS HIPS_GetSolution(INTS id,  INTS n, INTS *nlist, COEF *x, INTS mode); 
MULTIPLE_TYPE_DEFINE_F(INTS, HIPS_GetSolution,
		       (INTS id,  INTS n, INTS *nlist, SCOEF *x, INTS mode),
		       (INTS id,  INTS n, INTS *nlist, DCOEF *x, INTS mode),
		       (INTS id,  INTS n, INTS *nlist, CCOEF *x, INTS mode),
		       (INTS id,  INTS n, INTS *nlist, ZCOEF *x, INTS mode)); 



/* 
   Group: Clean up 
   
*/
/*
  Function: HIPS_Clean

  Clean the given instance of the solver structure's.

  Parameters: 
    id     - Problem identification number.


  Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.

  Fortran interface:
  >
  > SUBROUTINE HIPS_CLEAN(ID, IERROR)
  >   INTS, INTENT(IN)  :: ID
  >   INTS, INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_CLEAN
*/
INTS HIPS_Clean(INTS id);
MULTIPLE_TYPE_DEFINE(INTS, HIPS_Clean,(INTS id));

/*
  Function: HIPS_Finalize
  
  Clean all not cleaned instances and instances ID array.
  
  Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.

  Fortran interface:
  >
  > SUBROUTINE HIPS_FINALIZE(IERROR)
  >   INTS, INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_FINALIZE
*/
INTS HIPS_Finalize();
MULTIPLE_TYPE_DEFINE(INTS, HIPS_Finalize, ());



/*
  Group: Get HIPS's Infos
 */
/*
  Function: HIPS_GetInfoINT

  Get an info (integer number) from HIPS.

  See <HIPS_INFO_INT> and the solver documentation to 
  get available info list.
  
  Parameters:
    id      - Problem identification number.
    infonum - Wanted information number.
    value   - Integer which will contain the value of the information.

  Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.

    
  Fortran interface:
  > SUBROUTINE HIPS_GETINFOINT(ID, INFONUM, VALUE, IERROR)
  >  INTS, INTENT(IN)  :: ID, INFONUM
  >  INTL, INTENT(OUT) :: VALUE
  >  INTS, INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_GETINFOINT
 */
INTS HIPS_GetInfoINT(INTS id,  INTS infonum, INTL * value);
MULTIPLE_TYPE_DEFINE(INTS, HIPS_GetInfoINT,(INTS id,  INTS infonum, INTL * value));


/*
  Function: HIPS_GetInfoREAL

  Get an info (real number) from HIPS.

  See <HIPS_INFO_REAL> and the solver documentation to 
  get available info list.
  
  Parameters:
    id     - Problem identification number.
    infonum - Wanted information number.
    value   - Integer which will contain the value of the information.

  Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.

    
  Fortran interface:
  > SUBROUTINE HIPS_GETINFOREAL(ID, INFONUM, VALUE, IERROR)
  >  INTS, INTENT(IN)  :: ID, INFONUM
  >  REAL, INTENT(OUT) :: VALUE
  >  INTS, INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_GETINFOREAL
 */
MULTIPLE_TYPE_DEFINE(INTS, HIPS_GetInfoREAL,(INTS id,  INTS infonum, REAL * value));


/*
  Function: HIPS_PrintError
  
  Print the error message corresponding to ierror
  Parameters: 
    ierror       - Error identification number.

  Returns: 
    void


  Fortran interface:
  >
  > SUBROUTINE HIPS_PRINTERROR(IERROR)
  >   INTS, INTENT(IN):: IERROR
  > END SUBROUTINE HIPS_PRINTERROR
*/
void HIPS_PrintError(INTS ierror);
MULTIPLE_TYPE_DEFINE(void, HIPS_PrintError,(INTS ierror));

/*
  Function: HIPS_ExitOnError
  
  Print the error message corresponding to ierror.
  If the ierr is not HIPS_SUCCESS then the program is stopped.

  Parameters: 
    ierror         - Error identification number.

 Returns: 
    void

  Fortran interface:
  >
  > SUBROUTINE HIPS_EXITONERROR(IERROR)
  >   INTS, INTENT(IN) :: IERROR
  > END SUBROUTINE HIPS_EXITONERROR
*/
void HIPS_ExitOnError(INTS ierror);
MULTIPLE_TYPE_DEFINE(void, HIPS_ExitOnError,(INTS ierror));



/*
  Group: HIPS advanced functions
 */
#ifdef MPI_SUCCESS

/*
  Function: HIPS_SetCommunicator

  Sets MPI communicator for the given solver instance.

  Needs <HIPS_SetDefaultOptions> to be called before to initiate
  solver instance data.

  Musn't be called before <HIPS_SAVE>, <HIPS_LOAD>, 
  <HIPS_GetLocalNodeNbr> nor <HIPS_GetLocalUnknownNbr> 
  because the solver as to be runned with the same MPI 
  communicator all along.

  If this function is not called, MPI communicator will be
  *MPI_COMM_WORLD*.

  This function may not exist if the solver 
  has been compiled without MPI.
  
  Parameters: 
    id     - Problem identification number.  
    mpicomm - MPI communicator to be used for solving this problem.  


  Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.


  Fortran interface:
  >
  > SUBROUTINE HIPS_SETCOMMUNICATOR(ID, MPICOMM, IERROR)
  >   INTS,      INTENT(IN)  :: ID
  >   INTEGER,  INTENT(IN)  :: MPICOMM
  >   INTS,      INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_SETCOMMUNICATOR
*/
INTS HIPS_SetCommunicator(INTS id, MPI_Comm mpicom);
MULTIPLE_TYPE_DEFINE(INTS, HIPS_SetCommunicator,(INTS id, MPI_Comm mpicom));
#endif


/*
  Function: HIPS_MatrixVectorProduct
  
  This function does a the product y = A.c where A is 
  the matrix of the problem id.

  Parameters: 
    id          - Problem identification number.
    x           - Array that contain the local part of x corresponding to the HIPS unknown distribution (that can be obtained by HIPS_GetUnknownList).
    y           - Array that contain the local part of y corresponding to the HIPS unknown distribution (that can be obtained by HIPS_GetUnknownList).
    ierror      - Error identification number.

  Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.

  Fortran interface:
  >
  > SUBROUTINE HIPS_MATRIXVECTORPRODUCT(ID, X, Y, IERROR)
  >   INTS, INTENT(IN) :: ID
  >   COEF, DIMENSION(0), INTENT(IN) :: X
  >   COEF, DIMENSION(0), INTENT(OUT) :: Y
  >   INTS, INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_MATRIXVECTORPRODUCT
*/
INTS HIPS_MatrixVectorProduct(INTS id, COEF *x, COEF *y);
MULTIPLE_TYPE_DEFINE_F(INTS, HIPS_MatrixVectorProduct,
		       (INTS id, SCOEF *x, SCOEF *y),
		       (INTS id, DCOEF *x, DCOEF *y),
		       (INTS id, CCOEF *x, CCOEF *y),
		       (INTS id, ZCOEF *x, ZCOEF *y));

/*
  Function: HIPS_TransposeMatrix
  
  This function transpose the matrix problem.

  Parameters: 
    id          - Problem identification number.
    ierror      - Error identification number.

  Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.

  Fortran interface:
  >
  > SUBROUTINE HIPS_TRANSPOSEMATRIX(ID, IERROR)
  >   INTS, INTENT(IN) :: ID
  >   INTS, INTENT(OUT) :: IERROR
  > END SUBROUTINE HIPS_TRANSPOSEMATRIX
*/
INTS HIPS_TransposeMatrix(INTS id);
MULTIPLE_TYPE_DEFINE(INTS, HIPS_TransposeMatrix, (INTS id));


/*
  Function: HIPS_ReadOptionsFromFile

  This function reads the parameter in a "Inputs" file. This function can be used to write a code that load a matrix from disk and/or 
  should read some HIPS basic parameters from a file instead of using HIPS_SetOptionINT or HIPS_OptionREAL in the code.

  Parameters: 
    id           - Problem identification number.  
    inputsname   - name of the "inputs" file.
    sym_pattern  - return wether the matrix has a symmetric non zero pattern (1) or not (0) 
    sym_matrix   - return wether the matrix is symmetric (only lower triangular is stored in CSC) or not
    matrixname   - return name of the matrix file.
    rhsname      - return name of the rhs file

  Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.

>  SUBROUTINE HIPS_READOPTIONSFROMFILE(ID,  SYM_PATTERN, SYM_MATRIX, INPUTSNAME, MATRIXNAME, RHSNAME, IERROR)
>       INTS,             INTENT(IN)  :: ID 
>       INTS,             INTENT(OUT) :: SYM_PATTERN, SYM_MATRIX
>       CHARACTER(LEN=*),  INTENT(IN) :: INPUTSNAME
>       CHARACTER(LEN=*),  INTENT(OUT):: MATRIXNAME, RHSNAME
>       INTS,              INTENT(OUT):: IERROR
>  END SUBROUTINE  HIPS_READOPTIONSFROMFILE
*/
INTS HIPS_ReadOptionsFromFile(INTS id, char *inputsname,  INTS *sym_pattern, INTS *sym_matrix, char *matrixname, char *rhsname);
MULTIPLE_TYPE_DEFINE(INTS, HIPS_ReadOptionsFromFile, (INTS id, char *inputsname,  INTS *sym_pattern, INTS *sym_matrix, char *matrixname, char *rhsname));

/*
  Function: HIPS_CheckSolution

  This function check if the global solution (in initial numbering) obtained by HIPS is exact (i.e. respect the relative residual 
  norm error used to stop the convergence in HIPS). It multiplies the solution by the initial matrix in CSR and check the error compared to 
  the expected error

  Parameters: 
    id           - Problem identification number.  
    n       - Number of rows.
    rowptr  - Index of the first element of each row in *COLS* and 
              *values* array.
    cols    - Column number array.
    values  - values array.
    sym     - Indicates if the crs represents only the lower triangular part
              and that HIPS must consider that these coefficients are also the same in the upper triangular part.
	      

  Returns: 
    HIPS_SUCCESS - Successful return.
    HIP_ERR_xx   - A number that corresponds to a specific error 
                   that can be used with HIPS_PrintError 
		   or HIPS_ExitOnError.

>  SUBROUTINE HIPS_CHECKSOLUTION(ID,  N, ROWPTR, COLS, VALUES, SOL, RHS, SYM, IERROR)
>   INTS,               INTENT(IN)  :: ID, N, SYM
>   INTL, DIMENSION(0), INTENT(IN)  :: ROWPTR
>   INTS, DIMENSION(0), INTENT(IN)  :: COLS
>   COEF, DIMENSION(0), INTENT(IN)  :: VALUES
>   COEF, DIMENSION(0), INTENT(IN)  :: SOL
>   COEF, DIMENSION(0), INTENT(IN)  :: RHS
>   INTS,               INTENT(OUT) :: IERROR
>  END SUBROUTINE  HIPS_CHECKSOLUTION
*/
INTS HIPS_CheckSolution(INTS id, INTS n, INTL *rowptr, INTS *cols, COEF *values, COEF *sol, COEF *rhs, INTS sym);
MULTIPLE_TYPE_DEFINE_F(INTS, HIPS_CheckSolution, 
		       (INTS id, INTS n, INTL *rowptr, INTS *cols, SCOEF *values, SCOEF *sol, SCOEF *rhs, INTS sym), 
		       (INTS id, INTS n, INTL *rowptr, INTS *cols, DCOEF *values, DCOEF *sol, DCOEF *rhs, INTS sym), 
		       (INTS id, INTS n, INTL *rowptr, INTS *cols, CCOEF *values, CCOEF *sol, CCOEF *rhs, INTS sym), 
		       (INTS id, INTS n, INTL *rowptr, INTS *cols, ZCOEF *values, ZCOEF *sol, ZCOEF *rhs, INTS sym));




INTS HIPS_GetSubmatrix(INTS ln, INTS numflag, INTS *nodelist, INTS n, INTL *ia, INTS *ja, COEF *a, 
		       INTL **lia, INTS **lja, COEF **la);
MULTIPLE_TYPE_DEFINE_F(INTS, HIPS_GetSubmatrix,
		       (INTS ln, INTS numflag, INTS *nodelist, INTS n, INTL *ia, INTS *ja, 
			SCOEF *a, INTL **lia, INTS **lja, SCOEF **la),
		       (INTS ln, INTS numflag, INTS *nodelist, INTS n, INTL *ia, INTS *ja, 
			DCOEF *a, INTL **lia, INTS **lja, DCOEF **la),
		       (INTS ln, INTS numflag, INTS *nodelist, INTS n, INTL *ia, INTS *ja, 
			CCOEF *a, INTL **lia, INTS **lja, CCOEF **la),
		       (INTS ln, INTS numflag, INTS *nodelist, INTS n, INTL *ia, INTS *ja, 
			ZCOEF *a, INTL **lia, INTS **lja, ZCOEF **la));

/*
  Function: MATRIX_READ

  Read a matrix from file.
  This function is needed tby the hips test example in TEST/PARALLEL/

  Parameters: 
    

  Returns: 


> SUBROUTINE MATRIX_READ(job, t_n, t_nnz, ia, ja, a, sym_matrix, matrix)
>       INTS :: job
>       INTS :: t_n	
>       INTL   ::  t_nnz
>       INTL, DIMENSION(0)   :: ia
>       INTS, DIMENSION(0) :: ja
>       COEF, DIMENSION(0)  :: a
>       INTS :: sym_matrix
>       CHARACTER(LEN=200) :: matrix
>     END SUBROUTINE MATRIX_READ
*/
void Matrix_Read(INTS job, INTS *t_n, INTL* t_nnz, INTL * ia, INTS * ja, COEF* a, INTS* sym_matrix, char* matrix);
MULTIPLE_TYPE_DEFINE_F(void, Matrix_Read,
		       (INTS job, INTS *t_n, INTL* t_nnz, INTL * ia, INTS * ja, SCOEF* a, INTS* sym_matrix, char* matrix),
		       (INTS job, INTS *t_n, INTL* t_nnz, INTL * ia, INTS * ja, DCOEF* a, INTS* sym_matrix, char* matrix),
		       (INTS job, INTS *t_n, INTL* t_nnz, INTL * ia, INTS * ja, CCOEF* a, INTS* sym_matrix, char* matrix),
		       (INTS job, INTS *t_n, INTL* t_nnz, INTL * ia, INTS * ja, ZCOEF* a, INTS* sym_matrix, char* matrix));

/*
  Group: HIPS's constants
 */
/*
  Enum: HIPS_STRATNUM

  Hips strategy identifiers.
  
  Solvers may implement is own list of parameters.

  Contains: 
    HIPS_ITERATIVE         - Use the multistage ILUT precondionner
    HIPS_HYBRID            - Use the hybrid direct/iterative solver
*/
enum HIPS_STRATNUM{
  HIPS_DIRECT    = 0,
  HIPS_ILUT      = 1,       
  HIPS_ITERATIVE = 1,   
  HIPS_HYBRID    = 2,
  HIPS_BLOCK     = 3
};



/*
  Enum: HIPS_IPARAM

  Hips integer parameters identifiers.
  
  Solvers may implement is own list of parameters.
  Contains:
  HIPS_SYMMETRIC         -  [0,1] (default 0) this means that the matrix is symmetric and that you
  want to use the symmetric algorithms of HIPS (it saves half the computation and memory). {\bf In this case, only the upper triangular part of the CSR matrix given as input to HIPS functions are considered}. 
  HIPS_VERBOSE           -  [0-5] (default 2) level of informations printed in HIPS functions (0 the less information)

  HIPS_SCALE             - HIPS developers reserved. 
  HIPS_LOCALLY           - [0-] number of level that use the HIPS locally consistent fill-in pattern. This option defines the fill-in pattern (structurally and not numerically as in ILUT
  In general one will use 0 (no fill-in outside the original diagonal block pattern) or a high value (100 for example) to allow the fill-in anywhere in the Schur complement pattern.
  HIPS_KRYLOV_RESTART    - restart parameter of GMRES.
  HIPS_ITMAX             - maximum number of iteration allowed in the krylov method. In the full "ITERATIVE" mode (see *HIPS_STRATNUM*), you can set "-1" to make a simple forward/backward  substitution (for example if you want to use HIPS as a preconditioner in your method).
  HIPS_FORWARD           - HIPS developers reserved.
  HIPS_SCHUR_METHOD      - HIPS developers reserved. 
  HIPS_ITMAX_SCHUR       - 
  HIPS_PARTITION_TYPE    - HIPS developers reserved. 
  HIPS_KRYLOV_METHOD     - 0 Preconditioned GMRES, =1 Preconditioned Conjugate gradient. 
  HIPS_DOMSIZE           -
  HIPS_SMOOTH_ITER_RATIO - HIPS developers reserved.
  HIPS_DOMNBR            - 
  HIPS_REORDER           -  0 No reordering inside the subdomain to minimize fill-in, =1 reordering inside the subdomain to minimize fill-in. This option is only used in the recursive ITERATIVE preconditioneur.
  HIPS_SCALENBR          -  [1-] this value is used to set the number of time the normalisation is applied to the matrix. One should set a value >1 only in special case.
  HIPS_MASTER            -  HIPS pretreament (reodering, partitionning...) is done in sequential. The master processor is in charge of these computation. By default it is the processor 0 but you can change this with this option.
  HIPS_COARSE_GRID       - HIPS developers reserved.
  HIPS_CHECK_GRAPH       - [0, 1] (default 1) : set this option to 1 if you want to check (and repair) the matrix adjacency
    graph. This option ensures the graph is symmetric and that there is no double edge.
  HIPS_CHECK_MATRIX      -  =[0, 1] (default 1) : set this option to 1 if you want to check (and repair) the coefficients matrix. This option check if there are coefficient with the same indices and sum them up in this case.
  HIPS_DUMP_CSR          - HIPS developers reserved.
  HIPS_IMPROVE_PARTITION - HIPS developers reserved.
  HIPS_TAGNBR            - HIPS developers reserved.
  HIPS_SHIFT_DIAG        - HIPS developers reserved.
  HIPS_GRAPH_SYM         - =[0, 1] (default 1) : set this option to 0, if you are sure that the graph you give to HIPS is symmetric ; this disable the graph symmetrization.

  HIPS_GRID_DIM          - HIPS developers reserved.
  HIPS_GRID_3D           - HIPS developers reserved.
  HIPS_DISABLE_PRECOND   - [0, 1] if set to 1 then when new matrix coefficient are entered the preconditioner is not recalculated in HIPS. Nevertheless, this option is taken into account only if a preconditioner has already been computed.
  HIPS_FORTRAN_NUMBERING - [0, 1] (default 1) : numbering in indexes array will start at 0 or 1. This options modify the default numbering for the inputs and returns in all HIPS's functions. 
  HIPS_DOF               - [1-] (default 1) : number of unknowns per node in the matrix non-zero pattern graph. Usually, one needs this function when 
  a node represents several degree of freedom. That is to say : any entry (i, j) of the graph represents a dense block of (dof,dof) non null coefficients in the matrix.  
  HIPS_PIVOTING          -[0, 1] (default 0) : disable or enable column pivoting in ILUT (only for unsymmetric matrix). This option is not yet fully implemented in parallel.    
*/
enum HIPS_IPARAM{
  HIPS_SYMMETRIC         = 0,
  HIPS_VERBOSE           = 1,
  HIPS_SCALE             = 2,
  HIPS_LOCALLY           = 3,
  HIPS_KRYLOV_RESTART    = 4,
  HIPS_ITMAX             = 5,
  HIPS_FORWARD           = 6,
  HIPS_SCHUR_METHOD      = 7,
  HIPS_ITMAX_SCHUR       = 8,
  HIPS_PARTITION_TYPE    = 9,
  HIPS_KRYLOV_METHOD     = 10,
  HIPS_DOMSIZE           = 11,
  HIPS_SMOOTH_ITER_RATIO = 12,
  HIPS_DOMNBR            = 13,
  HIPS_REORDER           = 14,
  HIPS_SCALENBR          = 15,
  HIPS_MASTER            = 16,
  HIPS_COARSE_GRID       = 17,
  HIPS_CHECK_GRAPH       = 18,
  HIPS_CHECK_MATRIX      = 19,
  HIPS_DUMP_CSR          = 20,
  HIPS_IMPROVE_PARTITION = 21,
  HIPS_TAGNBR            = 22,
  HIPS_SHIFT_DIAG        = 23,
  HIPS_GRAPH_SYM         = 24,
  HIPS_GRID_DIM          = 25,
  HIPS_GRID_3D           = 26,
  HIPS_DISABLE_PRECOND   = 27,
  HIPS_FORTRAN_NUMBERING = 28,
  HIPS_DOF               = 29,
  HIPS_PIVOTING          = 30,
  HIPS_USE_PASTIX        = 31
};
#define OPTIONS_INT_NBR    50

/*
  Enum: HIPS_RPARAM

  Hips real parameters identifiers.
  
  Solvers may implement is own list of parameters.

  Contains: 
    HIPS_PREC         - Wanted norm error at the end of solve.
    HIPS_DROPTOL0     -
    HIPS_DROPTOL1     -
    HIPS_DROPTOLSCHUR -
    HIPS_DROPTOLE     -
    HIPS_AMALG        -
*/
enum HIPS_RPARAM{
  HIPS_PREC       = 0,
  HIPS_DROPTOL0   = 1,
  HIPS_DROPTOL1   = 2,
  HIPS_DROPSCHUR  = 3,
  HIPS_DROPTOLE   = 4,
  HIPS_AMALG      = 5
};
#define OPTIONS_REAL_NBR   50

/*
  Enum: HIPS_RETURNS

  HIPS error identifiers.
  These are the list of possible errors returned by HIPS functions.
  You can call HIPS_PrintError to print the information on a specific error number.
  You can call HIPS_ExitOnError to print the information on a specific error number and stop the program.
  
  Contains:
    HIPS_ERR_ALLOCATE ,
    HIPS_ERR_IO       ,
    HIPS_ERR_PARAMETER,
    HIPS_ERR_MATASSEMB,
    HIPS_ERR_RHSASSEMB,
    HIPS_ERR_PARASETUP,
    HIPS_ERR_CALL,
    HIPS_ERR_PRECOND,
    HIPS_ERR_SOLVE,  
    HIPS_ERR_KRYLOV,
    HIPS_ERR_CHECK,
*/
enum HIPS_RETURNS {
  HIPS_SUCCESS       = 1,
  HIPS_ERR_ALLOCATE ,
  HIPS_ERR_IO       ,
  HIPS_ERR_PARAMETER,
  HIPS_ERR_MATASSEMB,
  HIPS_ERR_RHSASSEMB,
  HIPS_ERR_PARASETUP,
  HIPS_ERR_CALL,
  HIPS_ERR_PRECOND,
  HIPS_ERR_SOLVE, 
  HIPS_ERR_KRYLOV, 
  HIPS_ERR_CHECK
};

/*
  Enum: HIPS_INFO_INT

  HIPS integer information identifiers.
  
  Contains:
    HIPS_INFO_NNZ         - Number of non zeros stored in the preconditioner (end of the preconditioning step).
    HIPS_INFO_NNZ_PEAK    - Maximum number (peak) of non-zeros stored during preconditioning step.
    HIPS_INFO_DIM         - Dimension of the global matrix 
    HIPS_INFO_OUTER_ITER, - Number of outer iterations
    HIPS_INFO_INNER_ITER, - Number of inner iterations (Schur complement)
    HIPS_INFO_ITER       - Number of iterations (inner for hybrid, outer for iterative)
*/
enum HIPS_INFO_INT {
  HIPS_INFO_NNZ =0,
  HIPS_INFO_NNZ_PEAK,
  HIPS_INFO_DIM,
  HIPS_INFO_OUTER_ITER,
  HIPS_INFO_INNER_ITER,
  HIPS_INFO_ITER
};

/*
  Enum: HIPS_INFO_REAL

  HIPS integer information identifiers.
  

  Contains: 
    HIPS_INFO_PRECOND_TIME  - Preconditioning time.
    HIPS_INFO_SOLVE_TIME - Solving time (total time in the preconditioned Krylov method).
    HIPS_INFO_FILL       - Fill-in (ratio with the number of non-zeros in the initial matrix) in the preconditioner.
    HIPS_INFO_FILL_PEAK  - Maximum fill-in (ratio with the number of non-zeros in the initial matrix) during the preconditioning step.
    HIPS_INFO_RES_NORM    - The relative residual norm achieved in the last resolution
*/
enum HIPS_INFO_REAL {
  HIPS_INFO_PRECOND_TIME = 0,
  HIPS_INFO_SOLVE_TIME,
  HIPS_INFO_FILL,
  HIPS_INFO_FILL_PEAK,
  HIPS_INFO_RES_NORM
};



/*
  Enum: HIPS_ASSEMBLY_MODE

    Indicates if user can ensure that the information he is giving respects
    the solver distribution.
  
    HIPS_ASSEMBLY_RESPECT - User ensure he respects distribution 
                             during assembly.
    HIPS_ASSEMBLY_FOOL    - User is not sure he will respect ditribution
                             during assembly
*/
enum HIPS_ASSEMBLY_MODE {
  HIPS_ASSEMBLY_RESPECT,
  HIPS_ASSEMBLY_FOOL
};


/*
  Enum: HIPS_ASSEMBLY_OP
   
    Operations possible when a coefficient appear twice.

    HIPS_ASSEMBLY_OVW - Coefficients will be overwriten during assembly.
    HIPS_ASSEMBLY_ADD - Coefficients will be added to the matrix. Do not sum overlaped values between processors.
*/
enum HIPS_ASSEMBLY_OP {
  HIPS_ASSEMBLY_OVW = 0,
  HIPS_ASSEMBLY_ADD
};

#endif
