This directory contains example files to illustrate the use of our
GMRES package.  The four arithmetic are treated (real-single, real-double,
complex-single, complex-double).

The test programs are making use the BLAS routine _GEMV and _COPY.
You should update the makefile to set up the link with your BLAS library.
If you do not have the BLAS routine, you can download them from the Netlib
website (www.netlib.org/blas).

REAL SINGLE arithmetic
------------------------
1. Once you have obtained by email the GMRES package corresponding to real
   single arithmetic, save it in the file "sPackcg.f" 
2. Update the makefile to set properly the link with BLAS
3. Compile with the command "make sarith"
4. The executable is "sTestcg"
5. The execution generates two files: "fort.10" and "sol_sTestcg"

REAL DOUBLE arithmetic
------------------------
1. Once you have obtained by email the GMRES package corresponding to real
   double arithmetic, save it in the file "dPackcg.f" 
2. Update the makefile to set properly the link with BLAS
3. Compile with the command "make darith"
4. The executable is "dTestcg"
5. The execution generates two files: "fort.20" and "sol_dTestcg"

COMPLEX SINGLE arithmetic
------------------------
1. Once you have obtained by email the GMRES package corresponding to real
   single arithmetic, save it in the file "cPackcg.f"
2. Update the makefile to set properly the link with BLAS
3. Compile with the command "make carith"
4. The executable is "cTestcg"
5. The execution generates two files: "fort.30" and "sol_cTestcg"

COMPLEX DOUBLE arithmetic
------------------------
1. Once you have obtained by email the GMRES package corresponding to real
   single arithmetic, save it in the file "zPackcg.f"
2. Update the makefile to set properly the link with BLAS
3. Compile with the command "make zarith"
4. The executable is "zTestcg"
5. The execution generates two files: "fort.40" and "sol_zTestcg"

"make all" generates the executable files for the four arithmetic at the
same time.

The test matrix is built from the programme. It is a tridiagonal matrix
with 4 on the main diagonal, -2 on upper diagonal and -1 on the sub
diagonal. The matrix size is chosen by the user at run-time. The right-hand
side is chosen so that the exact solution is the vector of all ones.
The excution generates two files, one containing the history of the
computation, the other one containing the computed solution.

Below is the contents of the output file for the solution of
a double precision complex matrix of order 10 with a restart equal to 3.

prompt% zTestcg
 
 ***********************************************
 This code is an example of use of CG
 in single precision real arithmetic
 Results are written in output files
 fort.40 : log file of CG iterations 
 and sol_Testcg : output of the computation.
 ***********************************************
 
 Matrix size <   1000
20
  Est. smallest eig.    0.25380630710024
  Est. biggest  eig.     20.746194182805
   1  (    2.0000000000000,    1.0000000000000)
   2  (    2.0000000000000,   1.00000000000000)
   3  (    2.0000000000000,    1.0000000000000)
   4  (    2.0000000000000,   1.00000000000000)
   5  (    2.0000000000000,    1.0000000000000)
   6  (    2.0000000000000,    1.0000000000000)
   7  (    2.0000000000000,    1.0000000000000)
   8  (    2.0000000000000,    1.0000000000000)
   9  (    2.0000000000000,    1.0000000000000)
   10  (    2.0000000000000,   1.00000000000000)
rompt%

* Contents of the output file fort.40:
======================================
                       CONVERGENCE HISTORY FOR CG
 
Errors are displayed in unit:  6
Warnings are displayed in unit:  6
Matrix size:      20
No preconditioning
Default initial guess x_0 = 0
Maximum number of iterations:    21
Tolerance for convergence: 0.10E-08
Backward error on the unpreconditioned system Ax = b:
    the residual is normalised by ||b||
Optimal size for the workspace:    165
 
Convergence history: b.e. on the unpreconditioned system
 Iteration   Approx. b.e.    True b.e.
    1           0.26E+00         --
    2           0.11E+00         --
    3           0.60E-01         --
    4           0.34E-01         --
    5           0.21E-01         --
    6           0.13E-01         --
    7           0.82E-02         --
    8           0.57E-02         --
    9           0.46E-02         --
   10           0.46E-02         --
   11           0.54E-02         --
   12           0.54E-02         --
   13           0.36E-02         --
   14           0.17E-02         --
   15           0.66E-03         --
   16           0.22E-03         --
   17           0.66E-04         --
   18           0.16E-04         --
   19           0.26E-05         --
   20           0.39E-15       0.38E-15
 
Convergence achieved
B.E. on the unpreconditioned system: 0.38E-15
info(1) =  0
Number of iterations (info(2)):    20

* Contents of the output file sol_zTestcg
============================================
 info(1) =   0  info(2) =   20
 rinfo(1) =     3.8207120850809D-16
 rinfo(2) =    0.25380630710024  rinfo(3) =     20.746194182805
 Optimal workspace =   165
 Solution : 
   (    2.0000000000000,    1.0000000000000)
   (    2.0000000000000,   1.00000000000000)
   (    2.0000000000000,    1.0000000000000)
   (    2.0000000000000,   1.00000000000000)
   (    2.0000000000000,    1.0000000000000)
   (    2.0000000000000,    1.0000000000000)
   (    2.0000000000000,    1.0000000000000)
   (    2.0000000000000,    1.0000000000000)
   (    2.0000000000000,    1.0000000000000)
   (    2.0000000000000,   1.00000000000000)
   (    2.0000000000000,   1.00000000000000)
   (    2.0000000000000,    1.0000000000000)
   (    2.0000000000000,    1.0000000000000)
   (    2.0000000000000,    1.0000000000000)
   (    2.0000000000000,    1.0000000000000)
   (    2.0000000000000,   1.00000000000000)
   (    2.0000000000000,   1.00000000000000)
   (    2.0000000000000,    1.0000000000000)
   (    2.0000000000000,    1.0000000000000)
   (    2.0000000000000,   1.00000000000000)

***********************************
C*
C*  Copyright (C) CERFACS 2000
C*
C*  SOFTWARE LICENSE AGREEMENT NOTICE - THIS SOFTWARE IS BEING PROVIDED TO
C*  YOU BY CERFACS UNDER THE FOLLOWING LICENSE. BY DOWN-LOADING, INSTALLING
C*  AND/OR USING THE SOFTWARE YOU AGREE THAT YOU HAVE READ, UNDERSTOOD AND
C*  WILL COMPLY WITH THESE FOLLOWING TERMS AND CONDITIONS.
C*
C*  1 - This software program provided in source code format ("the " Source
C*  Code ") and any associated documentation (the " Documentation ") are
C*  licensed, not sold, to you.
C*
C*  2 - CERFACS grants you a personal, non-exclusive, non-transferable and
C*  royalty-free right to use, copy or modify the Source Code and
C*  Documentation, provided that you agree to comply with the terms and
C*  restrictions of this agreement. You may modify the Source Code and
C*  Documentation to make source code derivative works, object code
C*  derivative works and/or documentation derivative Works (called "
C*  Derivative Works "). The Source Code, Documentation and Derivative
C*  Works (called " Licensed Software ") may be used by you for personal
C*  and non-commercial use only. " non-commercial use " means uses that are
C*  not or will not result in the sale, lease or rental of the Licensed
C*  Software and/or the use of the Licensed Software in any commercial
C*  product or service. CERFACS reserves all rights not expressly granted
C*  to you. No other licenses are granted or implied.
C*
C*  3 - The Source Code and Documentation are and will remain the sole
C*  property of CERFACS. The Source Code and Documentation are copyrighted
C*  works. You agree to treat any modification or derivative work of the
C*  Licensed Software as if it were part of the Licensed Software itself.
C*  In return for this license, you grant CERFACS a non-exclusive perpetual
C*  paid-up royalty-free license to make, sell, have made, copy, distribute
C*  and make derivative works of any modification or derivative work you
C*  make of the Licensed Software.
C*
C*  4- The licensee shall acknowledge the contribution of the Source Code
C*  in any publication of material dependent upon the use of the Source
C*  Code. The licensee shall use reasonable endeavours to send to CERFACS a
C*  copy of each such publication.
C*
C*  5- CERFACS has no obligation to support the Licensed Software it is
C*  providing under this license.
C*
C*  THE LICENSED SOFTWARE IS PROVIDED " AS IS " AND CERFACS MAKE NO
C*  REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED. BY WAY OF EXAMPLE,
C*  BUT NOT LIMITATION, CERFACS MAKE NO REPRESENTATIONS OR WARRANTIES OF
C*  MERCHANTIBILY OR FITNESS FOR ANY PARTICULAR PURPOSE OR THAT THE USE OF
C*  THE LICENSED SOFTWARE OR DOCUMENTATION WILL NOT INFRINGE ANY THIRD
C*  PARTY PATENTS, COPYRIGHTS, TRADEMARKS OR OTHER RIGHTS. CERFACS WILL NOT
C*  BE LIABLE FOR ANY CONSEQUENTIAL, INCIDENTAL, OR SPECIAL DAMAGES, OR ANY
C*  OTHER RELIEF, OR FOR ANY CLAIM BY ANY THIRD PARTY, ARISING FROM YOUR
C*  USE OF THE LICENSED SOFTWARE.
C*
C*  6- For information regarding a commercial license for the Source Code
C*  and Documentation, please contact Mrs Campassens (campasse@cerfacs.fr)
C*
C*  7- This license is effective until terminated. You may terminate this
C*  license at any time by destroying the Licensed Software.
C*
C*    I agree all the terms and conditions of the above license agreement
C*
***********************************
